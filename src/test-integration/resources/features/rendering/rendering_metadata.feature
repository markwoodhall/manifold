Feature: Rendering metadata

 Scenario: Articles are rendered in citeproc+json

   Given an empty Item Graph database
   And no Items have been rendered yet
   And the following Items in the Item Graph
     | Identifier                       |
     | https://doi.org/10.5555/12345678 |
   When "https://doi.org/10.5555/12345678" is added to "https://id.crossref.org/collections/work" by "https://ror.org/02twcfp32"
   Then the Item "https://doi.org/10.5555/12345678" is rendered as "application/citeproc+json"
   And the Item "https://doi.org/10.5555/12345678" is rendered as "application/vnd.crossref.citeproc.elastic+json"
   Then Items are or aren't rendered
     | Identifier                       | Content Type                                   | Rendered? |
     | https://doi.org/10.5555/12345678 | application/citeproc+json                      | True      |
     | https://doi.org/10.5555/12345678 | application/vnd.crossref.citeproc.elastic+json | True      |
