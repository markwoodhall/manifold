Feature: Rendered items feed

  Scenario: Content type and links should be correct
    When the feed is retrieved from the "/v1/items/feed.xml" endpoint
    Then the Content-Type header should be "application/atom+xml"
    And the feed has a "self" link href prefixed with the feed endpoint "/v1/items/feed.xml"
    And the feed has a "first" link href prefixed with the feed endpoint "/v1/items/feed.xml"

  Scenario Outline: Feeds returns recent items

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type              |
      | https://doi.org/10.5555/12345678 | application/citeproc+json |
      | https://doi.org/10.5555/87654321 | application/citeproc+json |
    When all feed items are retrieved from the "/v1/items/feed.xml" endpoint, following cursors in pages of 2
    And there is an entry with id prefixed with the item endpoint for <Identifier> with <ContentType> and <Version>
    And that entry has updated equal to <Version> for <Identifier> with <ContentType>
    And that entry has title equal to Item <Version> in <ContentType> for <Identifier>
    And that entry has "alternate" link href prefixed with the item endpoint for <Identifier> with type <ContentType>
    And that entry has "related" link href prefixed with the item endpoint for <Identifier> with <ContentType> and <Version>
    And that entry has "enclosure" link href prefixed with the item pointer for <Identifier> with <ContentType> and <Version>

    Examples:
      | Identifier                         | ContentType                 | Version |
      | "https://doi.org/10.5555/87654321" | "application/citeproc+json" | 1       |
      | "https://doi.org/10.5555/12345678" | "application/citeproc+json" | 1       |

  Scenario Outline: Feeds returns recent items past cursor

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type              |
      | https://doi.org/10.5555/12345678 | application/citeproc+json |
      | https://doi.org/10.5555/87654321 | application/citeproc+json |
      | https://doi.org/10.5555/11111111 | application/citeproc+json |
      | https://doi.org/10.5555/22222222 | application/citeproc+json |
    When all feed items are retrieved from the "/v1/items/feed.xml" endpoint, following cursors in pages of 2
    And there is an entry with id prefixed with the item endpoint for <Identifier> with <ContentType> and <Version>
    Examples:
      | Identifier                         | ContentType                 | Version |
      | "https://doi.org/10.5555/87654321" | "application/citeproc+json" | 1       |
      | "https://doi.org/10.5555/12345678" | "application/citeproc+json" | 1       |
      | "https://doi.org/10.5555/22222222" | "application/citeproc+json" | 1       |
      | "https://doi.org/10.5555/11111111" | "application/citeproc+json" | 1       |

  Scenario: An empty feed

    Given the following Items have the following stored rendered representations 
      | Identifier | Content Type |
    When all feed items are retrieved from the "/v1/items/feed.xml" endpoint, following cursors in pages of 2
    Then the feed has no entries

  Scenario Outline: Feeds returns recent items for identifier

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type                                   | Title                   |
      | https://doi.org/10.5555/12345678 | application/citeproc+json                      | Article Title           |
      | https://doi.org/10.5555/12345678 | application/vnd.crossref.citeproc.elastic+json | Article Title           |
      | https://doi.org/10.5555/12345678 | application/citeproc+json                      | Article Title Updated 1 |
      | https://doi.org/10.5555/12345678 | application/vnd.crossref.citeproc.elastic+json | Article Title Updated 1 |
    When all feed items are retrieved from the "/v1/items/feed.xml" endpoint, following cursors in pages of 2
    Then there is an entry with id prefixed with the item endpoint for <Identifier> with <ContentType> and <Version>
    And that entry has id prefixed with the item endpoint for <Identifier> with <ContentType> and <Version>
    And that entry has updated equal to <Version> for <Identifier> with <ContentType>
    And that entry has title equal to Item <Version> in <ContentType> for <Identifier>
    And that entry has "alternate" link href prefixed with the item endpoint for <Identifier> with type <ContentType>
    And that entry has "related" link href prefixed with the item endpoint for <Identifier> with <ContentType> and <Version>
    And that entry has "enclosure" link href prefixed with the item pointer for <Identifier> with <ContentType> and <Version>

    Examples:
      | Identifier                         | ContentType                                      | Version |
      | "https://doi.org/10.5555/12345678" | "application/citeproc+json"                      | 1       |
      | "https://doi.org/10.5555/12345678" | "application/citeproc+json"                      | 2       |
      | "https://doi.org/10.5555/12345678" | "application/vnd.crossref.citeproc.elastic+json" | 1       |
      | "https://doi.org/10.5555/12345678" | "application/vnd.crossref.citeproc.elastic+json" | 2       |

  Scenario Outline: Paging through items in date range

    Given the following Items have the following stored rendered representations 
      | Identifier                       | Content Type              | Updated At          |
      | https://doi.org/10.5555/11111111 | application/citeproc+json | 2023-01-01T01:00:00 |
      | https://doi.org/10.5555/22222222 | application/citeproc+json | 2023-01-01T01:00:00 |
      | https://doi.org/10.5555/33333333 | application/citeproc+json | 2023-01-02T01:00:00 |
      | https://doi.org/10.5555/44444444 | application/citeproc+json | 2023-01-02T01:00:00 |
    When all feed items from <FromDate> to <UntilDate> are retrieved from the "/v1/items/feed.xml" endpoint, following cursors in pages of <Rows>
    Then there are <Count> entries for <Identifier> in <ContentType>
    Examples:
    # Four items, retrieving in pages of 2, should traverse 2 pages.
      | Identifier                         | ContentType                 | Count   | Rows | FromDate     | UntilDate    |
      | "https://doi.org/10.5555/11111111" | "application/citeproc+json" | 1       | 2    | "2023-01-01" | "2023-01-01" |
      | "https://doi.org/10.5555/22222222" | "application/citeproc+json" | 1       | 2    | "2023-01-01" | "2023-01-01" |
      | "https://doi.org/10.5555/33333333" | "application/citeproc+json" | 1       | 2    | "2023-01-02" | "2023-01-02" |
      | "https://doi.org/10.5555/44444444" | "application/citeproc+json" | 1       | 2    | "2023-01-02" | "2023-01-02" |
    # Four items, retrieving in pages of 4, shouldn't need to paginate.
      | "https://doi.org/10.5555/44444444" | "application/citeproc+json" | 1       | 4    | "2023-01-01" | "2023-01-02" |
      | "https://doi.org/10.5555/33333333" | "application/citeproc+json" | 1       | 4    | "2023-01-01" | "2023-01-02" |
      | "https://doi.org/10.5555/22222222" | "application/citeproc+json" | 1       | 4    | "2023-01-01" | "2023-01-02" |
      | "https://doi.org/10.5555/11111111" | "application/citeproc+json" | 1       | 4    | "2023-01-01" | "2023-01-02" |
