Feature: Perspectives
  The Item Graph is made up of Items, which are connected with Relationship Statements and described with Property Statements. Each Statement is made by a party, which is itself an Item. To interpret the Statements in the Item Graph, users need to know:

  1. Who asserts each the statement.
  2. What connection they have to the Items they are describing.
  3. Whom to ultimately consult to know the above.

  For example,

  - The publisher of an article asserts metadata about it.
  - Crossref asserts that the member is the steward.
  - Crossref also asserts, on its own behalf, that Crossref is the registration agency of the DOI.
  - The user retrieving data knows that Crossref is a party they can trust to assert stewardship information.

  This forms a chain of authority that users can follow and interpret. In future this model could be further enriched with information such as authorship, sponsorship, etc.

  This allows for the vision of the Research Nexus to be modelled. But interpreting this model does impose a cognitive load. As there is no single 'truth', it requires each data point to be interpreted on its own merits. The model is also transitive, as one assertion (such as a funder link) can open up paths to data, such as assertions made by the funder.

  In addition to cognitive complexity, there is also technical complexity complexity involved in retrieving information about asserting parties for each assertion.

  The solution to this is PERSPECTIVES, which allow a user to filter out which assertions to retrieve, and which party's assertions they want to retrieve. By shifting the interpretation to pre-filtering statements, the user is not required to look at the provenance of every single assertion. These perspectives include:

  - 'all' - all assertions made by anyone
  - 'registration-agency' - assertions by whoever is the registration agency of the Item, if relevant
  - 'steward' - assertions by the party who is the steward of the item, if relevant
  - 'registration-agency-root' - per 'registration-agency' except we only look at the registration agency of the root item, explained later
  - 'steward-root' - per 'steward', except only take assertions by the steward of the root node, explained later
  - 'partner' - assertions made by a defined list of trusted partner parties
  - 'default' - a combination of 'registration agency' + 'steward' + 'partner'

  If a user doesn't specify a perspective, they get 'default'. This perspective aligns with our historical treatment of metadata.

  The Item Tree Retriever is one of several ways of retrieving data from the Item Graph. When a user requests metadata for a given Item (e.g. an Article), the Item Tree retriever starts with that item as the 'root node' of the tree, and then retrieves the tree to a given depth. Each relationship is a branch in the tree. Property statements are also retrieved. As it recurses down the tree it uses a perspective to decide what to retrieve.

  Some perspectives consult only the root node when they decide whom to include. Some look at each item. They both have their uses.

  - 'registration-agency' consults each Item. This means if we start with a Crossref DOI at the root, we include assertions from Crossref. But if we encounter an ORCID ID, we can ask what ORCID asserted about that ID, such as author name.

  - 'registration-agency-root' in that case would include only Crossref's assertions.

  - 'steward' consults the steward each Item. This means that if Publisher 1 is the steward of an Article, and it cites another article for which the steward is Publisher 2, we can include Publisher 2's assertions (such as its title).

  - 'steward-root' would include only Publisher 1's assertions. This would be useful as it can help spot gaps in the assertions of the steward.

  The authority is a hard-coded list. The strategies look to this list to discover the starting point (e.g. to discover who is the steward). In the initial implementation only Crossref is included, but can be broadened out to multiple trusted parties.

  The 'partners' is a separate hard-coded list of authority roots. This is a list of parties whose assertions are returned as part of the 'partner' (and therefore 'default') perspective. These are useful when there is no named steward, for example Agents that scrape data from the web for which there is no formal concept of stewardship.

  The scenarios here describe each perspective. They then provide illustrations of when each is useful.

  Background:

    Given the following parties are authority roots
      | https://ror.org/02twcfp32 |
      | https://ror.org/04wxnsj81 |

    Given the following parties are authorities
      | https://ror.org/02twcfp32 |
      | https://ror.org/04wxnsj81 |
      | https://ror.org           |
      | https://europepmc.org     |

    Given the following relationship assertions in the item graph
      | Asserted By                               | Subject ID                          | Relationship Type   | Object ID                           |
      | https://ror.org/02twcfp32                 | https://doi.org/10.5555/12345678    | steward             | https://id.crossref.org/organization/1234 |
      | https://ror.org/02twcfp32                 | https://doi.org/10.5555/12345678    | registration-agency | https://ror.org/02twcfp32           |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | cites               | https://id.crossref.org/blank/56789 |
      | https://ror.org/02twcfp32                 | https://id.crossref.org/blank/56789 | object              | https://doi.org/10.6666/987654      |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | cites               | https://id.crossref.org/blank/98765 |
      | https://id.crossref.org/organization/1234 | https://id.crossref.org/blank/98765 | object              | https://doi.org/10.1111/111111      |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | funder              | https://ror.org/01e8hcf19           |
      | https://ror.org                           | https://ror.org/02twcfp32           | registration-agency | https://ror.org                     |
      | https://ror.org/04wxnsj81                 | https://doi.org/10.5555/12345678    | cites               | https://doi.org/10.6666/887766      |
      | https://example.com/random_party          | https://doi.org/10.5555/12345678    | author              | https://orcid.org/98765             |
      | https://europepmc.org                     | https://doi.org/10.5555/12345678    | has-preprint        | https://doi.org/10.1111/23456       |

    And the following property assertions in the item graph
      | Asserted By                               | Subject ID                                | Property     | Value                    |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678          | title        | Psychoceramics: A Study  |
      | https://id.crossref.org/organization/1234 | https://id.crossref.org/blank/98765       | unstructured | Carberry, J. (2008)      |
      | https://ror.org/02twcfp32                 | https://id.crossref.org/organization/1234 | name         | Psychoceramics, Inc      |
      | https://ror.org                           | https://ror.org/01e8hcf19                 | name         | American Ceramic Society |
      | https://ror.org                           | https://ror.org/02twcfp32                 | name         | Crossref                 |
      | https://ror.org/04wxnsj81                 | https://doi.org/10.5555/12345678          | name         | PSYCHOCERAMICS STUDY     |
      | https://example.com/random_party          | https://doi.org/10.5555/12345678          | name         | ABCDEFGHIJKL             |

  Scenario: 'all' retrieves the tree of all connected statements.

    When a user requests an item tree for "https://doi.org/10.5555/12345678" with "all" perspective
    Then the item tree should contain the following relationship statements
      | Asserted By                               | Subject ID                          | Relationship Type   | Object ID                                 |
      | https://ror.org/02twcfp32                 | https://doi.org/10.5555/12345678    | steward             | https://id.crossref.org/organization/1234 |
      | https://ror.org/02twcfp32                 | https://doi.org/10.5555/12345678    | registration-agency | https://ror.org/02twcfp32                 |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | cites               | https://id.crossref.org/blank/56789       |
      | https://ror.org/02twcfp32                 | https://id.crossref.org/blank/56789 | object              | https://doi.org/10.6666/987654            |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | cites               | https://id.crossref.org/blank/98765       |
      | https://id.crossref.org/organization/1234 | https://id.crossref.org/blank/98765 | object              | https://doi.org/10.1111/111111            |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | funder              | https://ror.org/01e8hcf19                 |
      | https://ror.org                           | https://ror.org/02twcfp32           | registration-agency | https://ror.org                           |
      | https://ror.org/04wxnsj81                 | https://doi.org/10.5555/12345678    | cites               | https://doi.org/10.6666/887766            |
      | https://example.com/random_party          | https://doi.org/10.5555/12345678    | author              | https://orcid.org/98765                   |
      | https://europepmc.org                     | https://doi.org/10.5555/12345678    | has-preprint        | https://doi.org/10.1111/23456             |

    And the item tree should contain the following property statements
      | Asserted By                               | Subject ID                                | Property     | Value                    |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678          | title        | Psychoceramics: A Study  |
      | https://id.crossref.org/organization/1234 | https://id.crossref.org/blank/98765       | unstructured | Carberry, J. (2008)      |
      | https://ror.org/02twcfp32                 | https://id.crossref.org/organization/1234 | name         | Psychoceramics, Inc      |
      | https://ror.org                           | https://ror.org/01e8hcf19                 | name         | American Ceramic Society |
      | https://ror.org                           | https://ror.org/02twcfp32                 | name         | Crossref                 |
      | https://ror.org/04wxnsj81                 | https://doi.org/10.5555/12345678          | name         | PSYCHOCERAMICS STUDY     |
      | https://example.com/random_party          | https://doi.org/10.5555/12345678          | name         | ABCDEFGHIJKL             |

  Scenario: 'registration-agency' retrieves the tree of statements asserted by the registration agency of each item as it goes.
  Crossref is the RA of the root node, and uses the ROR ID to identify itself as the RA.
  ROR is also asserted as the RA of that ROR ID, so we retrieve connected assertions by ROR.

    When a user requests an item tree for "https://doi.org/10.5555/12345678" with "default" perspective
    Then the item tree should contain the following relationship statements
      | Asserted By               | Subject ID                       | Relationship Type   | Object ID                                 |
      | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | steward             | https://id.crossref.org/organization/1234 |
      | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | registration-agency | https://ror.org/02twcfp32                 |
    And the item tree should contain the following property statements
      | Asserted By               | Subject ID                                | Property | Value               |
      | https://ror.org/02twcfp32 | https://id.crossref.org/organization/1234 | name     | Psychoceramics, Inc |
      | https://ror.org           | https://ror.org/02twcfp32                 | name     | Crossref            |

  Scenario: 'steward' retrieves the tree of statements asserted by the steward of the item

  Crossref asserts that the member is the steward, so follow all links and retrieve properties asserted by the steward.
  Retrieve the reference link asserted by the member, but not the match asserted by Crossref.

    When a user requests an item tree for "https://doi.org/10.5555/12345678" with "steward" perspective
    Then the item tree should contain the following relationship statements
      | Asserted By                               | Subject ID                          | Relationship Type | Object ID                           |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | cites             | https://id.crossref.org/blank/56789 |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | cites             | https://id.crossref.org/blank/98765 |
      | https://id.crossref.org/organization/1234 | https://id.crossref.org/blank/98765 | object            | https://doi.org/10.1111/111111      |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | funder            | https://ror.org/01e8hcf19           |

    And the item tree should contain the following property statements
      | Asserted By                               | Subject ID                          | Property     | Value                   |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | title        | Psychoceramics: A Study |
      | https://id.crossref.org/organization/1234 | https://id.crossref.org/blank/98765 | unstructured | Carberry, J. (2008)     |

  Scenario: 'registration-agency-root' perspective retrieves the tree of statements asserted by the registration agency of only the root item.

  Look for the RA of the root item (i.e. Crossref) and retrieve assertions only by that party.

    When a user requests an item tree for "https://doi.org/10.5555/12345678" with "registration-agency-root" perspective
    Then the item tree should contain the following relationship statements
      | Asserted By               | Subject ID                       | Relationship Type   | Object ID                                 |
      | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | steward             | https://id.crossref.org/organization/1234 |
      | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | registration-agency | https://ror.org/02twcfp32                 |

    And the item tree should contain the following property statements
      | Asserted By               | Subject ID                                | Property | Value               |
      | https://ror.org/02twcfp32 | https://id.crossref.org/organization/1234 | name     | Psychoceramics, Inc |


  Scenario: 'steward-root' retrieves the tree of statements asserted by the steward of the root node

    When a user requests an item tree for "https://doi.org/10.5555/12345678" with "steward-root" perspective
    Then the item tree should contain the following relationship statements
      | Asserted By                               | Subject ID                          | Relationship Type | Object ID                           |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | cites             | https://id.crossref.org/blank/56789 |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | cites             | https://id.crossref.org/blank/98765 |
      | https://id.crossref.org/organization/1234 | https://id.crossref.org/blank/98765 | object            | https://doi.org/10.1111/111111      |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | funder            | https://ror.org/01e8hcf19           |

    And the item tree should contain the following property statements
      | Asserted By                               | Subject ID                          | Property     | Value                   |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | title        | Psychoceramics: A Study |
      | https://id.crossref.org/organization/1234 | https://id.crossref.org/blank/98765 | unstructured | Carberry, J. (2008)     |

  Scenario: 'partner' retrieves all statements made by known partners

  Partners include DataCite (https://ror.org/04wxnsj81), Crossref (https://ror.org/02twcfp32) and ROR itself (ror.org)

    When a user requests an item tree for "https://doi.org/10.5555/12345678" with "partner" perspective
    Then the item tree should contain the following relationship statements
      | Asserted By               | Subject ID                       | Relationship Type   | Object ID                           |
      | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | steward             | https://id.crossref.org/organization/1234 |
      | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | registration-agency | https://ror.org/02twcfp32           |
      | https://ror.org           | https://ror.org/02twcfp32        | registration-agency | https://ror.org                     |
      | https://ror.org/04wxnsj81 | https://doi.org/10.5555/12345678 | cites               | https://doi.org/10.6666/887766      |
      | https://europepmc.org     | https://doi.org/10.5555/12345678 | has-preprint        | https://doi.org/10.1111/23456       |


    And the item tree should contain the following property statements
      | Asserted By               | Subject ID                                | Property | Value                |
      | https://ror.org/02twcfp32 | https://id.crossref.org/organization/1234 | name     | Psychoceramics, Inc  |
      | https://ror.org           | https://ror.org/02twcfp32                 | name     | Crossref             |
      | https://ror.org/04wxnsj81 | https://doi.org/10.5555/12345678          | name     | PSYCHOCERAMICS STUDY |


  Scenario: default retrieves the combination of 'registration agency' + 'steward' + 'partner'

    When a user requests an item tree for "https://doi.org/10.5555/12345678" with default perspective
    Then the item tree should contain the following relationship statements
      | Asserted By                               | Subject ID                          | Relationship Type   | Object ID                           |
      | https://ror.org/02twcfp32                 | https://doi.org/10.5555/12345678    | steward             | https://id.crossref.org/organization/1234 |
      | https://ror.org/02twcfp32                 | https://doi.org/10.5555/12345678    | registration-agency | https://ror.org/02twcfp32           |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | cites               | https://id.crossref.org/blank/56789 |
      | https://ror.org/02twcfp32                 | https://id.crossref.org/blank/56789 | object              | https://doi.org/10.6666/987654      |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | cites               | https://id.crossref.org/blank/98765 |
      | https://id.crossref.org/organization/1234 | https://id.crossref.org/blank/98765 | object              | https://doi.org/10.1111/111111      |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678    | funder              | https://ror.org/01e8hcf19           |
      | https://ror.org                           | https://ror.org/02twcfp32           | registration-agency | https://ror.org                     |
      | https://ror.org/04wxnsj81                 | https://doi.org/10.5555/12345678    | cites               | https://doi.org/10.6666/887766      |
      | https://europepmc.org                     | https://doi.org/10.5555/12345678    | has-preprint        | https://doi.org/10.1111/23456       |

    And the item tree should contain the following property statements
      | Asserted By                               | Subject ID                                | Property     | Value                    |
      | https://id.crossref.org/organization/1234 | https://doi.org/10.5555/12345678          | title        | Psychoceramics: A Study  |
      | https://id.crossref.org/organization/1234 | https://id.crossref.org/blank/98765       | unstructured | Carberry, J. (2008)      |
      | https://ror.org/02twcfp32                 | https://id.crossref.org/organization/1234 | name         | Psychoceramics, Inc      |
      | https://ror.org                           | https://ror.org/01e8hcf19                 | name         | American Ceramic Society |
      | https://ror.org                           | https://ror.org/02twcfp32                 | name         | Crossref                 |
      | https://ror.org/04wxnsj81                 | https://doi.org/10.5555/12345678          | name         | PSYCHOCERAMICS STUDY     |


  Scenario: DOI without prefix is redirected

    When a user requests an item tree for non-prefixed "10.5555/12345678" with default perspective
    Then resource "10.5555/12345678" is moved permanently
