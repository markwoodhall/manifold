Feature: Organization information is retrieved from Sugar and put into the item graph

  Scenario: Organization information is accurately copied from Sugar to the item graph.
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Sugar Internal Id | Name                                | Account Type          | Crossref Member Id | Sponsor Id | Active |
      | 1                 | Federal Reserve Bank of Dallas      | Sponsored Publisher   | 10241              |            | true   |
      | 2                 | Federal Reserve Bank of Kansas City | Sponsoring Publishers | 7902               |            | true   |
    When the Sugar ingestion is run
    Then the Item Graph contains members
      | Name                                | Identifier                                 |
      | Federal Reserve Bank of Dallas      | https://id.crossref.org/organization/10241 |
      | Federal Reserve Bank of Kansas City | https://id.crossref.org/organization/7902  |

  Scenario: Organizations are accurately marked as Crossref members, or not
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Name                                           | Crossref Member Id | Account Type          | Sugar Internal Id                    | Active |
      | Universidad Pedagogica Experimental Libertador | 35135              | Member Publisher      | 0a557dcc-ca34-11ec-b609-02c180ed988e | true   |
      | Sekolah Tinggi Ilmu Kesehatan Abdurahman       | 33704              | REPRESENTED_MEMBER    | 4c126d4a-6ef6-11ec-a143-06b3e72edb9c | true   |
      | Médecine et Hygiène                            | 17667              | Sponsoring Publishers | 8640a360-b40f-11e8-b397-02ef9e9f3eb9 | true   |
      | Relawan Jurnal Indonesia                       | 15117              | Sponsoring Entities   | d979586a-c487-11e7-84a5-06e14280fdab | true   |
      | Federal Reserve Bank of Dallas                 | 10241              | Sponsored Publisher   | 1ee92fee-f92f-11e6-bb87-0230eed5e4fb | true   |
      | Sapiens publications SDN BHD                   | 22574              | RA_AFFILIATE          | 8276234c-e783-11e9-a3d3-06b3e72edb9c | true   |
      | Janeway                                        | 38171              | AGENT                 | ba7c8ce0-b6ce-11ed-b784-06a99076beaf | true   |
      | ResearchFilter                                 | 21926              | Affiliates            | 435dc456-b6b1-11e9-948d-06b3e72edb9c | true   |
    When the Sugar ingestion is run
    Then the Item Graph contains members
      | Name                                           | Identifier                                 |
      | Universidad Pedagogica Experimental Libertador | https://id.crossref.org/organization/35135 |
      | Sekolah Tinggi Ilmu Kesehatan Abdurahman       | https://id.crossref.org/organization/33704 |
      | Médecine et Hygiène                            | https://id.crossref.org/organization/17667 |
    Then the following relationship statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID                                 | Relationship Type | Object ID                 |
      | 1     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/organization/35135 | member-of         | https://ror.org/02twcfp32 |
      | 1     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/organization/33704 | member-of         | https://ror.org/02twcfp32 |
      | 1     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/organization/17667 | member-of         | https://ror.org/02twcfp32 |
      | 0     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/organization/15117 | member-of         | https://ror.org/02twcfp32 |
      | 0     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/organization/10241 | member-of         | https://ror.org/02twcfp32 |
      | 0     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/organization/22574 | member-of         | https://ror.org/02twcfp32 |
      | 0     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/organization/38171 | member-of         | https://ror.org/02twcfp32 |
      | 0     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/organization/21926 | member-of         | https://ror.org/02twcfp32 |

  Scenario: Sponsoring Entities - REPRESENTED_MEMBER
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Sugar Internal Id                     | Name                                     | Crossref Member Id | Account Type        | Sponsor Id                           | Active |
      | 4c126d4a-6ef6-11ec-a143-06b3e72edb9c  | Sekolah Tinggi Ilmu Kesehatan Abdurahman | 33704              | REPRESENTED_MEMBER  | d979586a-c487-11e7-84a5-06e14280fdab | true   |
      | d979586a-c487-11e7-84a5-06e14280fdab  | Relawan Jurnal Indonesia                 | 15117              | Sponsoring Entities |                                      | true   |
    When the Sugar ingestion is run
    Then the following relationship statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID                                 | Relationship Type | Object ID                                  |
      | 1     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/organization/33704 | sponsor           | https://id.crossref.org/organization/15117 |

  Scenario: Sponsoring Publisher - Sponsored Publisher
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Sugar Internal Id                    | Name                                | Crossref Member Id | Account Type          | Sponsor Id                           | Active |
      | 1ee92fee-f92f-11e6-bb87-0230eed5e4fb | Federal Reserve Bank of Dallas      | 10241              | Sponsored Publisher   | ec7958d8-5299-150d-a27e-55ad26cd8d98 | true   |
      | ec7958d8-5299-150d-a27e-55ad26cd8d98 | Federal Reserve Bank of Kansas City | 7902               | Sponsoring Publishers |                                      | true   |
    When the Sugar ingestion is run
    Then the following relationship statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID                                 | Relationship Type | Object ID                                 |
      | 1     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/organization/10241 | sponsor           | https://id.crossref.org/organization/7902 |

  Scenario: Organization import can cope with empty Crossref Member Id
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Sugar Internal Id | Name                                | Account Type          | Crossref Member Id | Sponsor Id | Active |
      | 1                 | Federal Reserve Bank of Dallas      | Sponsored Publisher   | 10241              |            | true   |
      | 2                 | Federal Reserve Bank of Kansas City | Sponsoring Publishers |                    |            | true   |
    When the Sugar ingestion is run
    Then the Item Graph contains members
      | Name                           | Identifier                                 |
      | Federal Reserve Bank of Dallas | https://id.crossref.org/organization/10241 |

  Scenario: Duplicate Organizations are resolved so that the latest is present
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Sugar Internal Id | Name                           | Account Type        | Crossref Member Id | Sponsor Id | Active |
      | 1                 | Federal Reserve Bank of Dllas  | Sponsored Publisher | 10241              |            | true   |
      | 2                 | Federal Reserve Bank of Dallas | Sponsored Publisher | 10241              |            | true   |
    When the Sugar ingestion is run
    Then the Item Graph contains members
      | Name                                | Identifier                                 |
      | Federal Reserve Bank of Dallas      | https://id.crossref.org/organization/10241 |

  Scenario: Duplicate Sugar IDs are resolved so that no ingestion occurs
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Name                                           | Crossref Member Id | Account Type          | Sugar Internal Id                    | Active |
      | Universidad Pedagogica Experimental Libertador | 35135              | Member Publisher      | 0a557dcc-ca34-11ec-b609-02c180ed988e | true   | 
      | Sekolah Tinggi Ilmu Kesehatan Abdurahman       | 33704              | REPRESENTED_MEMBER    | 0a557dcc-ca34-11ec-b609-02c180ed988e | true   |
    When the Sugar ingestion is run
    Then the Item Graph does not contain members
      | Name                                           | Identifier                                 |
      | Universidad Pedagogica Experimental Libertador | https://id.crossref.org/organization/35135 |
      | Sekolah Tinggi Ilmu Kesehatan Abdurahman       | https://id.crossref.org/organization/33704 |

  Scenario: Active and Inactive Organizations are accurately marked as Crossref members, or not
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Name                                           | Crossref Member Id | Account Type          | Sugar Internal Id                    | Active |
      | Universidad Pedagogica Experimental Libertador | 35135              | Member Publisher      | 0a557dcc-ca34-11ec-b609-02c180ed988e | true   |
      | Sekolah Tinggi Ilmu Kesehatan Abdurahman       | 33704              | REPRESENTED_MEMBER    | 4c126d4a-6ef6-11ec-a143-06b3e72edb9c | false  |
    When the Sugar ingestion is run
    Then the Item Graph contains members
      | Name                                           | Identifier                                 |
      | Universidad Pedagogica Experimental Libertador | https://id.crossref.org/organization/35135 |
      | Sekolah Tinggi Ilmu Kesehatan Abdurahman       | https://id.crossref.org/organization/33704 |
    Then the following relationship statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID                                 | Relationship Type | Object ID                 |
      | 1     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/organization/35135 | member-of         | https://ror.org/02twcfp32 |
      | 0     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/organization/33704 | member-of         | https://ror.org/02twcfp32 |

  Scenario: Organizations that become inactive are not marked as Crossref members
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Name                                           | Crossref Member Id | Account Type     | Sugar Internal Id                    |
      | Universidad Pedagogica Experimental Libertador | 35135              | Member Publisher | 0a557dcc-ca34-11ec-b609-02c180ed988e | 
    And the Sugar ingestion has run
    And the following organizations become inactive in Sugar
      | Name                                           | Crossref Member Id | Account Type     | Sugar Internal Id                    |
      | Universidad Pedagogica Experimental Libertador | 35135              | Member Publisher | 0a557dcc-ca34-11ec-b609-02c180ed988e | 
    When the Sugar ingestion is run
    Then the following relationship statements are found in the item graph
      | Count | State | Current | Asserted By               | Subject ID                                 | Relationship Type | Object ID                 |
      | 1     | true  | false   | https://ror.org/02twcfp32 | https://id.crossref.org/organization/35135 | member-of         | https://ror.org/02twcfp32 |
      | 1     | false | true    | https://ror.org/02twcfp32 | https://id.crossref.org/organization/35135 | member-of         | https://ror.org/02twcfp32 |

  Scenario: Organizations are rendered in org JSON
    Given an empty Item Graph database
    And no Items have been rendered yet
    And the following organizations in Sugar
      | Sugar Internal Id | Name                                | Account Type          | Crossref Member Id | Sponsor Id | Active |
      | 1                 | Federal Reserve Bank of Dallas      | Sponsored Publisher   | 10241              |            | true   |
      | 2                 | Federal Reserve Bank of Kansas City | Sponsoring Publishers | 7902               |            | true   |
    When the Sugar ingestion is run
    Then the Item "https://id.crossref.org/organization/10241" is rendered as "application/vnd.crossref.org+json"
    And the Item "https://id.crossref.org/organization/7902" is rendered as "application/vnd.crossref.org+json"

  Scenario: Member Organizations are rendered in member JSON
    Given an empty Item Graph database
    And no Items have been rendered yet
    And the following organizations in Sugar
      | Sugar Internal Id | Name                                | Account Type          | Crossref Member Id | Sponsor Id | Active |
      | 1                 | Federal Reserve Bank of Kansas City | Sponsoring Publishers | 7902               |            | true   |
    When the Sugar ingestion is run
    Then the Item "https://id.crossref.org/organization/7902" is rendered as "application/vnd.crossref.member+json"

  Scenario: Organizational name change is reflected in citeproc+json publisher
    Given an empty Item Graph database
    And UniXSD file "test-publisher-name-change-10.5555-12345678.xml" registers Journal DOI "10.5555/1234567890"
    And UniXSD file "test-publisher-name-change-10.5555-12345678.xml" has crm-item with Member ID "1000"
    And UniXSD file "test-publisher-name-change-10.5555-12345678.xml" has crm-item with Publisher Name "Old Name"
    And UniXSD file "test-publisher-name-change-10.5555-12345678.xml" has been ingested
    And the following organizations in Sugar
      | Name     | Crossref Member Id | Account Type     | Sugar Internal Id                    |
      | New Name | 1000               | Member Publisher | 0a557dcc-ca34-11ec-b609-02c180ed988e | 
    When the Sugar ingestion is run
    Then "https://doi.org/10.5555/12345678" in "application/citeproc+json" should have member name "New Name"

  Scenario Outline: Organizations are ingested from ROR
    Given an empty Item Graph database
    And "ror-data.json" contains Organization with ID "<Identifier>", Name "<Name>" and Status "<Status>"
    When ROR file "ror-data.json" is ingested
    Then the Item Graph contains members
      | Name   | Identifier   |
      | <Name> | <Identifier> |

    Examples:
      | Identifier                | Name                           | Status |
      | https://ror.org/019wvm592 | Australian National University | active |
      | https://ror.org/02bfwt286 | Monash University              | active |
      | https://ror.org/00rqy9422 | University of Queensland       | active |

  Scenario Outline: Withdrawn Organizations are NOT ingested from ROR
    Given an empty Item Graph database
    And "ror-data.json" contains Organization with ID "<Identifier>", Name "<Name>" and Status "<Status>"
    When ROR file "ror-data.json" is ingested
    Then the Item Graph does not contain members
      | Name   | Identifier   |
      | <Name> | <Identifier> |

    Examples:
      | Identifier                | Name                 | Status     |
      | https://ror.org/01sf06y89 | Macquarie University | Widthdrawn |
      | https://ror.org/01kj2bm70 | Newcastle University | Widthdrawn |

  Scenario Outline: ROR ingestion creates same-as reltionships for any and preferred FundRef External IDs
    Given an empty Item Graph database
    And "ror-data.json" contains Organization with ID "<Identifier>", Name "<Name>" and Status "<Status>"
    And "ror-data.json" contains Organization preferred FundRef External ID "<PreferredFundRefId>"
    And "ror-data.json" contains Organization all FundRef External ID "<PreferredFundRefId>"
    And "ror-data.json" contains Organization all FundRef External ID "<FundRefId2>"
    And "ror-data.json" contains Organization all FundRef External ID "<FundRefId3>"
    When ROR file "ror-data.json" is ingested
    Then the Item Graph contains members
      | Name   | Identifier   |
      | <Name> | <Identifier> |
    And the following relationship statements are found in the item graph
      | Count | State | Current | Asserted By               | Subject ID   | Relationship Type | Object ID                                     |
      | 1     | true  | true    | https://ror.org/02twcfp32 | <Identifier> | same-as           | https://doi.org/10.13039/<PreferredFundRefId> |
      | 1     | true  | true    | https://ror.org/02twcfp32 | <Identifier> | same-as           | https://doi.org/10.13039/<FundRefId2>         |
      | 1     | true  | true    | https://ror.org/02twcfp32 | <Identifier> | same-as           | https://doi.org/10.13039/<FundRefId3>         |

    Examples:
      | Identifier                | Name                           | Status | PreferredFundRefId | FundRefId2   | FundRefId3 |
      | https://ror.org/019wvm592 | Australian National University | active | 501100000995       | 501100001151 | 100009020  |

