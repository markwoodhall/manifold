Feature: UniXML Bulk Ingestion

  UniXML is the data format that Crossref Members use to send us metadata, and the format we expose in our "XML APIs". It is currently our main source of truth as asserted by our members. It is also converted by Cayenne parser into Item Trees for ingestion into the /works endpoint of the REST API.

  At the start of the lifetime of this service we want to ingest the whole corpus of UniXML files in bulk. See the "Envelope Batch Bulk Ingestion" feature for details of the trade-offs about bulk ingestion.

  This feature covers the conversion from an XML snapshot file into an Envelope Archive file prior to ingestion.

  Scenario: Bulk file

    Given an empty Item Graph database
    And UniXSD file "silly-string.xml" registers Journal DOI "10.5555/1234567890"
    And UniXSD file "silly-string.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "silly-string.xml" has crm-item with Member ID "7822"
    And an XML Snapshot file named "xml.tar.gz" containing XML file "silly-string.xml"
    When XML Snapshot file "xml.tar.gz" is converted to a bulk file "bulk.tar.gz" and the bulk file is ingested
    Then the following relationship statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID                         | Relationship Type | Object ID                                 |
      | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/1234567890 | steward           | https://id.crossref.org/organization/7822 |
      | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678   | steward           | https://id.crossref.org/organization/7822 |
