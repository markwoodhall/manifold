Feature: Item Graph Ingestion strategies

  The Item Graph is made up of statements about Items, asserted by individuals and organizations.

  Rule: All strategies perform simple ingestion when there are no propr statements for the subject by the asserting party.

    Scenario Outline: Every strategy asserts the given relationships and properties simply when the graph is empty.

      Given an empty Item Graph database
      When "https://ror.org/02twcfp32" asserts relationship "https://example.com" "discusses" "https://doi.org/10.5555/12345678" in the Item Graph with <strategy> merge
      Then the following relationship statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
        | 1     | true    | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |
        | 0     | false   | https://ror.org/02twcfp32 | https://example.com | discusess         | https://doi.org/10.5555/12345678 |

      Given an empty Item Graph database
      When "https://ror.org/02twcfp32" asserts property "https://doi.org/10.5555/12345678" "title": "Psychoceramics" in the Item Graph with <strategy> merge
      Then the following property statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID                       | Property | Value          |
        | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |
        | 0     | false   | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |

      Examples:
        | strategy       |
        | "naive"        |
        | "union-closed" |
        | "closed"       |


  Rule: NAIVE strategy simply asserts statements without consulting the prior state

    Scenario: Re-asserting relationships with naive strategy.

    Using the naive merge strategy, asserting a relationship twice means it's re-asserted.

      Given the following relationship assertions in the item graph
        | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
        | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |
      When "https://ror.org/02twcfp32" asserts relationship "https://example.com" "discusses" "https://doi.org/10.5555/12345678" in the Item Graph with "naive" merge
      Then the following relationship statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
        | 1     | true    | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |
        | 1     | false   | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |

    Scenario: Replace properties with naive strategy.

    Asserting with a naive strategy replaces all previous properties for that item.

      Given the following property assertions in the item graph
        | Asserted By               | Subject ID                       | Property | Value |
        | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | colour   | green |
      When "https://ror.org/02twcfp32" asserts property "https://doi.org/10.5555/12345678" "title": "Psychoceramics" in the Item Graph with "naive" merge
      Then the following property statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID                       | Property | Value          |
        | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |
        | 0     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | colour   | green          |
        | 1     | false   | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | colour   | green          |

    Scenario: Re-asserting property with naive strategy

    Using the naive merge strategy, asserting a property twice means it's re-asserted.

      Given the following property assertions in the item graph
        | Asserted By               | Subject ID                       | Property | Value          |
        | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |
      When "https://ror.org/02twcfp32" asserts property "https://doi.org/10.5555/12345678" "title": "Psychoceramics" in the Item Graph with "naive" merge
      Then the following property statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID                       | Property | Value          |
        | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |
        | 1     | false   | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |

  Rule: UNION_CLOSED asserts all new statements without removing any prior statements.

    Scenario: Merging relationships with union-closed strategy

    Using union-closed merge strategy, asserting a relationship twice means it's not re-asserted.

      Given the following relationship assertions in the item graph
        | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
        | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |
      When "https://ror.org/02twcfp32" asserts relationship "https://example.com" "discusses" "https://doi.org/10.5555/12345678" in the Item Graph with "union-closed" merge
      Then the following relationship statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
        | 1     | true    | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |
        | 0     | false   | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |

    Scenario: Simple property re-assertions with union-closed strategy

    Asserting with a union-closed strategy asserts the property.

      Given the following property assertions in the item graph
        | Asserted By               | Subject ID                       | Property | Value |
        | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | OLD   |
      When "https://ror.org/02twcfp32" asserts property "https://doi.org/10.5555/12345678" "title": "Psychoceramics" in the Item Graph with "union-closed" merge
      Then the following property statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID                       | Property | Value          |
        | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |
        | 1     | false   | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | OLD            |

    Scenario: Properties merged with union-closed

    Asserting with a union-closed strategy merges with previous properties. It may or may not leave a historical statement, but the semantics will be the same.

      Given the following property assertions in the item graph
        | Asserted By               | Subject ID                       | Property | Value |
        | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | colour   | green |
      When "https://ror.org/02twcfp32" asserts property "https://doi.org/10.5555/12345678" "title": "Psychoceramics" in the Item Graph with "union-closed" merge
      Then the following property statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID                       | Property | Value          |
        | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |
        | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | colour   | green          |

    Scenario: Properties merged with union-closed

    Asserting properties with a union-closed strategy merges the supplied properties with previous current properties. Supplied keys are replaced with new ones. Previously asserted properties that are not re-supplied are re-asserted.

      Given the following multi-valued property assertion in the item graph
        | Asserted By               | Subject ID                       | Property | Value     |
        | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | colour   | green     |
        | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | OLD TITLE |

      When "https://ror.org/02twcfp32" asserts property "https://doi.org/10.5555/12345678" "title": "Psychoceramics" in the Item Graph with "union-closed" merge
      Then the following property statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID                       | Property | Value          |
        | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |
        | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | colour   | green          |
        | 1     | false   | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | colour   | green          |
        | 1     | false   | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | OLD TITLE      |

    Scenario: Re-asserting property with union-closed strategy

    Union-closed always results in the properties being re-asserted, even if they didn't change. This could be optimised in future.

      Given the following property assertions in the item graph
        | Asserted By               | Subject ID                       | Property | Value          |
        | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |
      When "https://ror.org/02twcfp32" asserts property "https://doi.org/10.5555/12345678" "title": "Psychoceramics" in the Item Graph with "union-closed" merge
      Then the following property statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID                       | Property | Value          |
        | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |
        | 1     | false   | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |

    Scenario: Two parties assert

    Different parties live in different universes. Their assertions work independently and don't affect each other.
    Assertions from Crossref and DataCite.

      Given the following relationship assertions in the item graph
        | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
        | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |
      When "https://ror.org/04wxnsj81" asserts relationship "https://example.com" "discusses" "https://doi.org/10.5555/12345678" in the Item Graph with "union-closed" merge
      Then the following relationship statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
        | 1     | true    | https://ror.org/04wxnsj81 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |
        | 1     | true    | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |

  Rule: CLOSED allows the asserting party to specify everything they know about an Item, and retract anything that they don't include in the batch.

    Scenario: Re-asserting relationships with closed strategy.

    Repeated assertions of the same relationship result in no re-assertion, as the statements are still current.

      Given the following relationship assertions in the item graph
        | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
        | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |
      When "https://ror.org/02twcfp32" asserts relationship "https://example.com" "discusses" "https://doi.org/10.5555/12345678" in the Item Graph with "closed" merge
      Then the following relationship statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
        | 1     | true    | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |
        | 0     | false   | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |

    Scenario: Re-asserting properties with closed strategy.

    Repeated assertions of the same properties may in re-assertion, but the statements are still current.

      Given the following property assertions in the item graph
        | Asserted By               | Subject ID  | Property | Value |
        | https://ror.org/02twcfp32 | https://example.com | colour   | red   |
      When "https://ror.org/02twcfp32" asserts property "https://example.com" "colour": "red" in the Item Graph with "closed" merge
      Then the following property statements are found in the item graph
        | 1 | true | https://ror.org/02twcfp32 | https://example.com | colour | red |

    Scenario: Re-asserting relationships with CLOSED will remove relationships that are not in the assertion batch.

      Given the following relationship assertions in the item graph
        | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
        | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |
        | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.6666/99999999 |
      When "https://ror.org/02twcfp32" asserts relationship "https://example.com" "discusses" "https://doi.org/10.5555/12345678" in the Item Graph with "closed" merge
      Then the following relationship statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
        | 1     | true    | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.5555/12345678 |
        | 0     | true    | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.6666/99999999 |
        | 1     | false   | https://ror.org/02twcfp32 | https://example.com | discusses         | https://doi.org/10.6666/99999999 |

    Scenario: Re-asserting properties with CLOSED will remove relationships that are not in the assertion batch.

      Given the following multi-valued property assertion in the item graph
        | Asserted By               | Subject ID  | Property | Value |
        | https://ror.org/02twcfp32 | https://example.com | size     | big   |
        | https://ror.org/02twcfp32 | https://example.com | sound    | noisy |
      When "https://ror.org/02twcfp32" asserts property "https://example.com" "size": "medium" in the Item Graph with "closed" merge
      Then the following property statements are found in the item graph
        | Count | Current | Asserted By               | Subject ID  | Property | Value  |
        | 1     | true    | https://ror.org/02twcfp32 | https://example.com | size     | medium |
        | 0     | true    | https://ror.org/02twcfp32 | https://example.com | sound    | noisy  |
        | 1     | false   | https://ror.org/02twcfp32 | https://example.com | sound    | noisy  |
