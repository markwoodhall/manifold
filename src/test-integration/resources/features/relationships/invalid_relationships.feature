Feature: Handle invalid relationships by ignoring them

  Scenario Outline: Ignore invalid relationships

    Given the following relationship assertions in the item graph
      | Subject ID                                    | Subject Blank | Relationship Type | Object ID                                                                                     | Object Blank | Asserted At          | Asserted By                         |
      #Relationships to items that are blank nodes
      | https://doi.org/10.7358/ling-2020-002-pett    | false         | has-manifestation | https://id.crossref.org/blank/4                                                               | true         | 2022-03-30T23:25:10Z | https://id.crossref.org/organization/5994 |
      | https://doi.org/10.1021/acsomega.2c04734      | false         | funding-source    | https://id.crossref.org/blank/5                                                               | true         | 2022-10-05T19:20:11Z | https://id.crossref.org/organization/301  |
      #Relationships from blank nodes where the relationship type isn't object
      | https://doi.org/10.1098/rsbl.2012.0298        | false         | references        | https://id.crossref.org/blank/6                                                               | true         | 2022-09-30T23:57:55Z | https://ror.org/02twcfp32           |
      | https://id.crossref.org/blank/6               | true          | source            | https://ca.wikipedia.org/api/rest_v1/page/html/Sopa_de_pl%C3%A0stic_del_Pac%C3%ADfic/30711606 | false        | 2022-09-30T23:57:55Z | https://ror.org/02twcfp32           |
      | https://doi.org/10.1080/01630563.2022.2132510 | false         | funding-source    | https://id.crossref.org/blank/7                                                               | true         | 2022-10-26T05:15:31Z | https://id.crossref.org/organization/301  |
      | https://id.crossref.org/blank/7               | true          | funder            | https://doi.org/10.13039/100007224                                                            | false        | 2022-10-26T05:15:31Z | https://id.crossref.org/organization/301  |

    When relationships for item <Identifier> are requested

    Then 0 relationships are returned from the item graph

    Examples:
      | Identifier                                      |
      | "https://doi.org/10.7358/ling-2020-002-pett"    |
      | "https://doi.org/10.1021/acsomega.2c04734"      |
      | "https://doi.org/10.1098/rsbl.2012.0298"        |
      | "https://id.crossref.org/blank/6"               |
      | "https://doi.org/10.1080/01630563.2022.2132510" |
      | "https://id.crossref.org/blank/7"               |
