Feature: Add headers and a few 'standard' fields.

  Our APIs typically include a header and a few fields that users expect to see. This includes the count of total results and HTTP status.

  Background:

    Given the following relationship assertions in the item graph
      | Subject ID                                   | Relationship Type | Object ID                                     | Asserted At          | Asserted By                              |
      | https://doi.org/10.5555/12345678             | author            | https://orcid.org/120398476                   | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3                 |
      | https://doi.org/10.5555/12345678             | references        | https://doi.org/10.5555/abcdef                | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3                 |
      | https://doi.org/10.3390/atoms1010001         | references        | https://doi.org/10.5555/12345678              | 2021-11-13T12:00:00Z | https://id.crossref.org/100              |
      | https://doi.org/10.1364/josab.476006         | author            | https://orcid.org/0000-0003-1527-4533         | 2022-12-19T20:34:03Z | https://id.crossref.org/organization/285 |
      | https://orcid.org/0000-0003-1527-4533        | affiliation       | https://ror.org/04ct4d772                     | 2022-12-19T20:34:03Z | https://id.crossref.org/organization/285 |
      | https://hypothes.is/a/FMqYpoo2Ee2H3yuF69ckYg | discusses         | https://doi.org/10.1016/j.celrep.2022.111934  | 2023-01-02T11:17:03Z | https://ror.org/02twcfp3                 |
      | https://doi.org/10.3390/separations8110217   | references        | https://doi.org/10.1016/j.indcrop.2016.06.017 | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3                 |
      | https://doi.org/10.3390/app12052751          | references        | https://doi.org/10.3390/separations8110217    | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3                 |

  Scenario: Users can see the HTTP status in the response.

    When all relationships are requested

    Then HTTP status "OK" is returned

  Scenario: Users can see the total number of results for a query.

    When all relationships are requested

    Then total row count 8 is returned

  Scenario: Users can see the message type in the response.

    When all relationships are requested

    Then message type "relationship-list" is returned
