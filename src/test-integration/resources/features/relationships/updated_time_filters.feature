Feature: Update time filters

  Scenario: one complete relationship through a reference and object assertion

    Given the following relationship assertions in the item graph
      | Subject ID                             | Relationship Type | Object ID                              | Ingested At          | Asserted By                 |
      | https://doi.org/10.1098/rsif.2021.0871 | references        | https://id.crossref.org/blank/001      | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/001      | object            | https://doi.org/10.1098/rsif.2021.0068 | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |

    When relationships created after "2023-01-01T00:00:00Z" are requested

    Then the following relationships are returned from the item graph
      | Subject ID                             | Relationship Type | Object ID                              | Created Time         | Updated Time         |
      | https://doi.org/10.1098/rsif.2021.0871 | references        | https://doi.org/10.1098/rsif.2021.0068 | 2023-02-01T00:00:00Z | 2023-02-01T00:00:00Z |

  Scenario Outline: one relationship, updated object assertion

    Given the following relationship assertions in the item graph
      | Subject ID                             | Relationship Type | Object ID                              | Ingested At          | Asserted By                 |
      | https://doi.org/10.1098/rsif.2021.0871 | references        | https://id.crossref.org/blank/001      | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/001      | object            | https://doi.org/10.1098/rsif.2021.0068 | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/001      | object            | https://doi.org/10.1098/rsif.2021.0068 | 2023-03-05T17:17:17Z | https://id.crossref.org/100 |

    When relationships updated after <Time> are requested

    Then the following relationships are returned from the item graph
      | Subject ID                             | Relationship Type | Object ID                              | Created Time         | Updated Time         |
      | https://doi.org/10.1098/rsif.2021.0871 | references        | https://doi.org/10.1098/rsif.2021.0068 | 2023-02-01T00:00:00Z | 2023-03-05T17:17:17Z |

    Examples:
      | Time                   |
      | "2023-01-01T00:00:00Z" |
      | "2023-03-01T00:00:00Z" |

  Scenario: one relationship, updated object assertion, out of range

    Given the following relationship assertions in the item graph
      | Subject ID                             | Relationship Type | Object ID                              | Ingested At          | Asserted By                 |
      | https://doi.org/10.1098/rsif.2021.0871 | references        | https://id.crossref.org/blank/001      | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/001      | object            | https://doi.org/10.1098/rsif.2021.0068 | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/001      | object            | https://doi.org/10.1098/rsif.2021.0068 | 2023-03-05T17:17:17Z | https://id.crossref.org/100 |

    When relationships updated after "2023-03-10T00:00:00Z" are requested

    Then 0 relationships are returned from the item graph

  Scenario Outline: one relationship, reference part reasserted

    Given the following relationship assertions in the item graph
      | Subject ID                             | Relationship Type | Object ID                              | Ingested At          | Asserted By                 |
      | https://doi.org/10.1098/rsif.2021.0871 | references        | https://id.crossref.org/blank/001      | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/001      | object            | https://doi.org/10.1098/rsif.2021.0068 | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://doi.org/10.1098/rsif.2021.0871 | references        | https://id.crossref.org/blank/001      | 2023-03-21T21:21:21Z | https://id.crossref.org/100 |

    When relationships updated after <Time> are requested

    Then the following relationships are returned from the item graph
      | Subject ID                             | Relationship Type | Object ID                              | Created Time         | Updated Time         |
      | https://doi.org/10.1098/rsif.2021.0871 | references        | https://doi.org/10.1098/rsif.2021.0068 | 2023-02-01T00:00:00Z | 2023-03-21T21:21:21Z |

    Examples:

      | Time                   |
      | "2023-01-01T00:00:00Z" |
      | "2023-03-01T00:00:00Z" |

  Scenario: one relationship, reference part reasserted, out of range

    Given the following relationship assertions in the item graph
      | Subject ID                             | Relationship Type | Object ID                              | Ingested At          | Asserted By                 |
      | https://doi.org/10.1098/rsif.2021.0871 | references        | https://id.crossref.org/blank/001      | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/001      | object            | https://doi.org/10.1098/rsif.2021.0068 | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://doi.org/10.1098/rsif.2021.0871 | references        | https://id.crossref.org/blank/001      | 2023-03-21T21:21:21Z | https://id.crossref.org/100 |

    When relationships updated after "2023-03-25T00:00:00Z" are requested

    Then 0 relationships are returned from the item graph

  Scenario Outline: one relationship, updated object assertion, updated before

    Given the following relationship assertions in the item graph
      | Subject ID                             | Relationship Type | Object ID                              | Ingested At          | Asserted By                 |
      | https://doi.org/10.1098/rsif.2021.0871 | references        | https://id.crossref.org/blank/001      | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/001      | object            | https://doi.org/10.1098/rsif.2021.0068 | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/001      | object            | https://doi.org/10.1098/rsif.2021.0068 | 2023-03-05T17:17:17Z | https://id.crossref.org/100 |

    When relationships updated before <Time> are requested

    Then the following relationships are returned from the item graph
      | Subject ID                             | Relationship Type | Object ID                              | Created Time         | Updated Time         |
      | https://doi.org/10.1098/rsif.2021.0871 | references        | https://doi.org/10.1098/rsif.2021.0068 | 2023-02-01T00:00:00Z | 2023-03-05T17:17:17Z |

    Examples:
      | Time                   |
      | "2023-03-05T17:17:17Z" |
      | "2023-03-06T00:00:00Z" |

  Scenario: one relationship, updated object assertion, updated before, out of range

    Given the following relationship assertions in the item graph
      | Subject ID                             | Relationship Type | Object ID                              | Ingested At          | Asserted By                 |
      | https://doi.org/10.1098/rsif.2021.0871 | references        | https://id.crossref.org/blank/001      | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/001      | object            | https://doi.org/10.1098/rsif.2021.0068 | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/001      | object            | https://doi.org/10.1098/rsif.2021.0068 | 2023-03-05T17:17:17Z | https://id.crossref.org/100 |

    When relationships updated before "2023-03-05T00:00:00Z" are requested

    Then 0 relationships are returned from the item graph
