Feature: Created filter parameters on the relationships endpoint

  Background:

    Given the following relationship assertions in the item graph
      | Subject ID                                 | Relationship Type | Object ID                                  | Ingested At          | Asserted By                 |
      | https://doi.org/10.1098/rsif.2021.0871     | references        | https://id.crossref.org/blank/001          | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/001          | object            | https://doi.org/10.1098/rsif.2021.0068     | 2023-02-01T00:00:00Z | https://id.crossref.org/100 |
      | https://doi.org/10.1109/lcsys.2023.3241137 | references        | https://id.crossref.org/blank/002          | 2023-02-15T00:08:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/002          | object            | https://doi.org/10.1109/lcsys.2022.3184820 | 2023-02-15T00:09:27Z | https://id.crossref.org/100 |
      | https://doi.org/10.34133/2021/9843140      | references        | https://id.crossref.org/blank/003          | 2023-03-01T00:00:00Z | https://id.crossref.org/100 |
      | https://id.crossref.org/blank/003          | object            | https://doi.org/10.34133/2021/9843140      | 2023-03-07T00:09:42Z | https://id.crossref.org/100 |

  Scenario: Default behaviour

    When all relationships are requested

    Then the following relationships are returned from the item graph
      | Subject ID                                 | Relationship Type | Object ID                                  | Created Time         | Updated Time         |
      | https://doi.org/10.1098/rsif.2021.0871     | references        | https://doi.org/10.1098/rsif.2021.0068     | 2023-02-01T00:00:00Z | 2023-02-01T00:00:00Z |
      | https://doi.org/10.1109/lcsys.2023.3241137 | references        | https://doi.org/10.1109/lcsys.2022.3184820 | 2023-02-15T00:09:27Z | 2023-02-15T00:09:27Z |
      | https://doi.org/10.34133/2021/9843140      | references        | https://doi.org/10.34133/2021/9843140      | 2023-03-07T00:09:42Z | 2023-03-07T00:09:42Z |

  Scenario Outline: from created time filter

    When relationships created after <Time> are requested

    Then the following relationships are returned from the item graph
      | Subject ID                                 | Relationship Type | Object ID                                  | Created Time         | Updated Time         |
      | https://doi.org/10.1109/lcsys.2023.3241137 | references        | https://doi.org/10.1109/lcsys.2022.3184820 | 2023-02-15T00:09:27Z | 2023-02-15T00:09:27Z |
      | https://doi.org/10.34133/2021/9843140      | references        | https://doi.org/10.34133/2021/9843140      | 2023-03-07T00:09:42Z | 2023-03-07T00:09:42Z |

    Examples:
      | Time                   |
      | "2023-02-10"           |
      | "2023-02-10T00:00:00Z" |
      | "2023-02-15T00:09:27"  |

  Scenario Outline: until created time filter

    When relationships created before <Time> are requested

    Then the following relationships are returned from the item graph
      | Subject ID                                 | Relationship Type | Object ID                                  | Created Time         | Updated Time         |
      | https://doi.org/10.1098/rsif.2021.0871     | references        | https://doi.org/10.1098/rsif.2021.0068     | 2023-02-01T00:00:00Z | 2023-02-01T00:00:00Z |
      | https://doi.org/10.1109/lcsys.2023.3241137 | references        | https://doi.org/10.1109/lcsys.2022.3184820 | 2023-02-15T00:09:27Z | 2023-02-15T00:09:27Z |

    Examples:
      | Time                   |
      | "2023-02-20"           |
      | "2023-02-20T00:00:00Z" |
      | "2023-02-15T00:09:27"  |
