Feature: Rows and cursors

  Results can be limited by a number of rows and cursors used to scroll through results.

  Background:

    Given the following relationship assertions in the item graph
      | Subject ID                                  | Relationship Type | Object ID                                     | Asserted At          | Asserted By                              |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://doi.org/10.21203/rs.3.rs-2279577/v1   | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3                 |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://doi.org/10.1016/j.patcog.2004.02.003  | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3                 |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://orcid.org/0000-0001-7648-6447         | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3                 |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://ror.org/04ct4d772                     | 2022-12-19T20:34:03Z | https://id.crossref.org/organization/285 |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://doi.org/10.1016/j.celrep.2022.111934  | 2023-01-02T11:17:03Z | https://ror.org/02twcfp3                 |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://doi.org/10.1016/j.indcrop.2016.06.017 | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3                 |

  Scenario Outline: requested number of rows is adhered to within lower and upper bounds.

    When <requested> relationships for item "https://doi.org/10.21203/rs.3.rs-2279577/v2" are requested

    Then <returned> relationships are returned from the item graph

    Examples:
      | requested | returned |
      | -1        | 0        |
      | 0         | 0        |
      | 1         | 1        |
      | 2         | 2        |
      | 3         | 3        |
      | 4         | 4        |
      | 5         | 5        |
      | 6         | 5        |

  Scenario: Results can be scrolled through using cursors.

    When the first 3 relationships for item "https://doi.org/10.21203/rs.3.rs-2279577/v2" are requested

    Then the following relationships are returned from the item graph
      | Subject ID                                  | Relationship Type | Object ID                                    |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://doi.org/10.21203/rs.3.rs-2279577/v1  |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://doi.org/10.1016/j.patcog.2004.02.003 |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://orcid.org/0000-0001-7648-6447        |

    When the next 3 relationships for item "https://doi.org/10.21203/rs.3.rs-2279577/v2" are requested

    Then the following relationships are returned from the item graph
      | Subject ID                                  | Relationship Type | Object ID                                     |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://doi.org/10.1016/j.celrep.2022.111934  |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://doi.org/10.1016/j.indcrop.2016.06.017 |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://ror.org/04ct4d772                     |
