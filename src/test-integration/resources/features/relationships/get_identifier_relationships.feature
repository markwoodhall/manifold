Feature: Get Identifier Relationships

  Query the relationships API for identifiers of varying types and return all relationships where those identifiers are the subject or object.

  Background:

    Given the following relationship assertions in the item graph
      | Subject ID                                   | Relationship Type | Object ID                                     | Asserted At          | Asserted By                              |
      | https://doi.org/10.5555/12345678             | author            | https://orcid.org/120398476                   | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3                 |
      | https://doi.org/10.5555/12345678             | references        | https://doi.org/10.5555/abcdef                | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3                 |
      | https://doi.org/10.5555/12345678             | references        | https://doi.org/10.5555/abcdef                | 2021-11-11T12:00:00Z | https://id.crossref.org/100              |
      | https://doi.org/10.3390/atoms1010001         | references        | https://doi.org/10.5555/12345678              | 2021-11-13T12:00:00Z | https://id.crossref.org/100              |
      | https://doi.org/10.3390/atoms1010001         | references        | https://doi.org/10.5555/12345678              | 2021-11-15T12:00:00Z | https://id.crossref.org/100              |
      | https://doi.org/10.1364/josab.476006         | author            | https://orcid.org/0000-0003-1527-4533         | 2022-12-19T20:34:03Z | https://id.crossref.org/organization/285 |
      | https://orcid.org/0000-0003-1527-4533        | affiliation       | https://ror.org/04ct4d772                     | 2022-12-19T20:34:03Z | https://id.crossref.org/organization/285 |
      | https://hypothes.is/a/FMqYpoo2Ee2H3yuF69ckYg | discusses         | https://doi.org/10.1016/j.celrep.2022.111934  | 2023-01-02T11:17:03Z | https://ror.org/02twcfp3                 |
      | https://doi.org/10.3390/separations8110217   | references        | https://doi.org/10.1016/j.indcrop.2016.06.017 | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3                 |
      | https://doi.org/10.3390/app12052751          | references        | https://doi.org/10.3390/separations8110217    | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3                 |

  Scenario: Query for DOI returns current versions of assertions where DOI is subject or object

    When relationships for item "https://doi.org/10.5555/12345678" are requested

    Then the following relationships are returned from the item graph
      | Subject ID                           | Relationship Type | Object ID                        |
      | https://doi.org/10.5555/12345678     | author            | https://orcid.org/120398476      |
      | https://doi.org/10.5555/12345678     | references        | https://doi.org/10.5555/abcdef   |
      | https://doi.org/10.3390/atoms1010001 | references        | https://doi.org/10.5555/12345678 |

  Scenario: Report relationship assertions for an ORCID

    When relationships for item "https://orcid.org/0000-0003-1527-4533" are requested

    Then the following relationships are returned from the item graph
      | Subject ID                            | Relationship Type | Object ID                             |
      | https://doi.org/10.1364/josab.476006  | author            | https://orcid.org/0000-0003-1527-4533 |
      | https://orcid.org/0000-0003-1527-4533 | affiliation       | https://ror.org/04ct4d772             |

  Scenario: Report relationship assertions for a ROR ID

    When relationships for item "https://ror.org/04ct4d772" are requested

    Then the following relationships are returned from the item graph
      | Subject ID                            | Relationship Type | Object ID                 |
      | https://orcid.org/0000-0003-1527-4533 | affiliation       | https://ror.org/04ct4d772 |

  Scenario: Report relationships for a URL

    When relationships for item "https://hypothes.is/a/FMqYpoo2Ee2H3yuF69ckYg" are requested

    Then the following relationships are returned from the item graph
      | Subject ID                           | Relationship Type | Object ID                                    |
      | https://hypothes.is/a/FMqYpoo2Ee2H3yuF69ckYg | discusses         | https://doi.org/10.1016/j.celrep.2022.111934 |

  Scenario: Filter by subject.

    When relationships for subject "https://doi.org/10.3390/separations8110217" are requested

    Then the following relationships are returned from the item graph
      | Subject ID                                 | Relationship Type | Object ID                                     |
      | https://doi.org/10.3390/separations8110217 | references        | https://doi.org/10.1016/j.indcrop.2016.06.017 |

  Scenario: Filter by object.

    When relationships for object "https://doi.org/10.3390/separations8110217" are requested

    Then the following relationships are returned from the item graph
      | Subject ID                          | Relationship Type | Object ID                                  |
      | https://doi.org/10.3390/app12052751 | references        | https://doi.org/10.3390/separations8110217 |
