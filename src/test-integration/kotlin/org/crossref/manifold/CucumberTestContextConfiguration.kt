package org.crossref.manifold

import com.ninjasquad.springmockk.MockkBean
import io.cucumber.spring.CucumberContextConfiguration
import org.crossref.manifold.itemgraph.Configuration.MAX_RETURNED_ROWS
import org.crossref.manifold.modules.unixml.Module.Companion.UNIXML
import org.crossref.manifold.relationship.Constants.API
import org.crossref.manifold.relationship.Constants.RELATIONSHIPS
import org.crossref.manifold.rendering.Configuration.RENDER_QUEUE_PROCESSOR_ENABLED
import org.crossref.manifold.rendering.Configuration.ITEMS_API
import org.crossref.manifold.rendering.Configuration.RENDERER_ENABLED
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.Configuration.RENDER_S3_BUCKET
import org.crossref.manifold.rendering.Configuration.WORKS_API
import org.crossref.manifold.util.Constants.FIXED_DELAY
import org.crossref.manifold.util.Constants.INITIAL_DELAY
import org.crossref.messaging.aws.autoconfig.S3AutoConfig.Companion.S3_ENABLED
import org.crossref.messaging.aws.autoconfig.S3AutoConfig.Companion.S3_NOTIFICATION_QUEUE
import org.crossref.messaging.aws.autoconfig.SqsAutoConfig.Companion.SQS_ENABLED
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestPropertySource
import java.time.InstantSource

@CucumberContextConfiguration
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(
    properties = [
        "$MAX_RETURNED_ROWS=5",
        "$UNIXML.$S3_NOTIFICATION_QUEUE=manifold-mainq",
        "$RENDERING.$RENDER_QUEUE_PROCESSOR_ENABLED=true",
        // Give us a chance to truncate the tables before the stale flags are picked up.
        "$RENDERING.$INITIAL_DELAY=5",
        // Stale loop should run faster than production as we've got smaller volumes of data, and we wait for it.
        // In [reset] we wait for this to complete.
        "$RENDERING.$FIXED_DELAY=5",
        "$RENDERING.$RENDERER_ENABLED=true",
        "$RENDERING.$RENDER_S3_BUCKET=render-bucket",
        "$S3_ENABLED=true",
        "$SQS_ENABLED=true",
        "$RELATIONSHIPS.$API=true",
        "$RENDERING.$WORKS_API=true",
        "$RENDERING.$ITEMS_API=true",
    ]
)
class CucumberTestContextConfiguration {
    // Mock the InstantSource bean so we can inject preset ingestion times.
    @MockkBean
    private lateinit var instantSource: InstantSource
}
