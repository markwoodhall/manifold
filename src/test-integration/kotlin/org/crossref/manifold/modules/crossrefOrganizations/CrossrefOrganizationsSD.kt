package org.crossref.manifold.modules.crossreforganizations

import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import io.mockk.every
import io.mockk.mockk
import org.apache.commons.io.IOUtils
import org.crossref.manifold.common.ItemGraphSetup
import org.crossref.manifold.db.BufferBatchDao
import org.crossref.manifold.db.BufferType
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.ror.RORIngestion
import org.crossref.manifold.rendering.RenderedItemDao
import org.crossref.manifold.rendering.RenderedItemStorageDao
import org.crossref.manifold.rendering.RenderingSD
import org.json.JSONArray
import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class CrossrefOrganizationsSD(
    val itemGraph: ItemGraph,
    val itemGraphSetup: ItemGraphSetup,
    val renderSd: RenderingSD,
    val renderedItemDao: RenderedItemDao,
    val renderedItemStorageDao: RenderedItemStorageDao,
    val bufferBatchDao: BufferBatchDao,
    val rorIngestion: RORIngestion,
) {
    val logger: Logger = LoggerFactory.getLogger(this::class.java)
    private val sugarCRMServiceMock: SugarService = mockk()

    fun resource(filename: String) = Thread.currentThread().contextClassLoader
        .getResourceAsStream("ror/$filename")!!

    @Before
    fun beforeEach() {
        itemGraphSetup.truncateItemGraphTables()
        every {
            sugarCRMServiceMock.authenticate(any<String>(), any<String>())
        } answers {
            "TOKEN"
        }
    }

    @When("the following organizations in Sugar")
    fun the_following_organizations(orgs: List<SugarOrganization>) {
        every {
            sugarCRMServiceMock.getOrganizations(any<String>(), any<Int>())
        } answers {
            PagedSugarOrganizations(
                -1,
                orgs.toTypedArray(),
            )
        }
    }

    @When("the following organizations become inactive in Sugar")
    fun the_following_organizations_become_inactive_in_sugar(orgs: List<SugarOrganization>) {
        every {
            sugarCRMServiceMock.getOrganizations(any<String>(), any<Int>())
        } answers {
            PagedSugarOrganizations(
                -1,
                orgs.map { it.copy(active = false) }.toTypedArray(),
            )
        }
    }

    @Given("the Sugar ingestion has run")
    fun the_sugar_ingestion_has_run() {
        val importer = SugarIngestion(sugarCRMServiceMock, itemGraph)
        importer.ingest("username", "password")
    }

    @When("the Sugar ingestion is run")
    fun the_sugar_ingestion_is_run() {
        val importer = SugarIngestion(sugarCRMServiceMock, itemGraph)
        importer.ingest("username", "password")

        bufferBatchDao.waitForBuffer(BufferType.RELATIONSHIP_ASSERTION)
        bufferBatchDao.waitForBuffer(BufferType.PROPERTY_ASSERTION)
        renderSd.waitForQueues()
    }

    @Then("{string} in {string} should have member name {string}")
    fun item_in_content_type_should_have_member_name(identifier: String, contentType: String, memberName: String) {
        val resolvedContentType = renderSd.resolveAndCheckContentType(contentType)
        val resolvedItem = renderSd.resolveAndCheckItem(identifier)
        val renderedItem = renderedItemStorageDao.fetchContent(
            renderedItemDao.getRenderedItem(resolvedItem.pk!!, resolvedContentType)!!,
        )

        Assertions.assertNotNull(renderedItem.content, "Expected renderedItem to have content")

        val citeproc = JSONObject(renderedItem.content)
        Assertions.assertEquals(memberName, citeproc.getString("publisher"))
    }

    @Given("{string} contains Organization with ID {string}, Name {string} and Status {string}")
    fun file_contains_organization_with_id_and_name(file: String, id: String, name: String, status: String) {
        resource(file).use {
            val json = JSONArray(IOUtils.toString(it, "UTF-8"))
            var foundOrg: Boolean = false
            for (i in 0 until json.length()) {
                val org = json.getJSONObject(i)
                foundOrg = (org.getString("id") == id && org.getString("name") == name && org.getString("status") == status)
                if (foundOrg) break
            }
            Assertions.assertTrue(foundOrg, "Expected to find Organization with ID $id, Name $name and Status $status")
        }
    }

    @Given("{string} contains Organization preferred FundRef External ID {string}")
    fun file_contains_organization_with_prefered_fund_ref_id(file: String, id: String) {
        resource(file).use {
            val json = JSONArray(IOUtils.toString(it, "UTF-8"))
            var foundOrg: Boolean = false
            for (i in 0 until json.length()) {
                val org = json.getJSONObject(i)
                val externalIds = org.getJSONObject("external_ids")
                val fundRef = externalIds.getJSONObject("FundRef")
                foundOrg = fundRef.getString("preferred") == id
                if (foundOrg) break
            }
            Assertions.assertTrue(foundOrg, "Expected to find Organization with preferred FundRef External ID $id")
        }
    }

    @Given("{string} contains Organization all FundRef External ID {string}")
    fun file_contains_organization_with_all_fund_ref_id(file: String, id: String) {
        resource(file).use {
            val json = JSONArray(IOUtils.toString(it, "UTF-8"))
            var foundOrg: Boolean = false
            for (i in 0 until json.length()) {
                val org = json.getJSONObject(i)
                val externalIds = org.getJSONObject("external_ids")
                val fundRef = externalIds.getJSONObject("FundRef")
                val all = fundRef.getJSONArray("all")
                for (j in 0 until all.length()) {
                    foundOrg = all[j].toString() == id
                    if (foundOrg) break
                }
                if (foundOrg) break
            }
            Assertions.assertTrue(foundOrg, "Expected to find Organization with any FundRef External ID $id")
        }
    }

    @When("ROR file {string} is ingested")
    fun ror_file_is_ingested(file: String) {
        resource(file).use {
            rorIngestion.ingest(IOUtils.toString(it, "UTF-8"))
        }

        bufferBatchDao.waitForBuffer(BufferType.RELATIONSHIP_ASSERTION)
        bufferBatchDao.waitForBuffer(BufferType.PROPERTY_ASSERTION)
        renderSd.waitForQueues()
    }
}
