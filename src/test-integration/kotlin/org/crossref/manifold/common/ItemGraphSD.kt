package org.crossref.manifold.common

import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemgraph.PropertyDao
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.junit.Before
import org.junit.jupiter.api.Assertions.*
import java.net.URI
import java.time.OffsetDateTime

class ItemGraphSD(
    private val itemGraph: ItemGraph,
    private val itemGraphSetup: ItemGraphSetup,
    private val resolver: Resolver,
    private val propertyDao: PropertyDao,
) {
    @Before
    fun truncateItemGraphTables() {
        itemGraphSetup.truncateItemGraphTables()
    }

    @When("{string} asserts relationship {string} {string} {string} in the Item Graph with {string} merge")
    fun asserts_relationship(assertedBy: String, subj: String, relationship: String, obj: String, merge: String) {
        val mergeStrategy = getMergeStrategy(merge)

        assertNotNull(mergeStrategy, "Not recognised merge strategy")

        val envelope = Envelope(
            listOf(
                Item().withIdentifier(Identifier.new(subj))
                    .withRelationship(
                        Relationship(
                            relationship, Item()
                                .withIdentifier(Identifier.new(obj))
                        )
                    )
            ),
            ItemTreeAssertion(
                OffsetDateTime.now(), mergeStrategy,
                Item().withIdentifier(Identifier.new(assertedBy))
            )
        )

        val envelopeBatch = EnvelopeBatch(
            listOf(envelope),
            EnvelopeBatchProvenance("Crossref Test", "1234"),
        )

        itemGraphSetup.waitForIngestions(listOf(itemGraph.ingest(listOf(envelopeBatch))))
    }

    @When("{string} asserts property {string} {string}: {string} in the Item Graph with {string} merge")
    fun asserts_property(assertedBy: String, subj: String, property: String, value: String, merge: String) {
        val mergeStrategy = getMergeStrategy(merge)

        val envelope = Envelope(
            listOf(
                Item().withIdentifier(Identifier.new(subj))
                    .withPropertiesFromMap(mapOf(property to value))
            ),
            ItemTreeAssertion(
                OffsetDateTime.now(),
                mergeStrategy,
                Item().withIdentifier(Identifier.new(assertedBy))
            )
        )

        val envelopeBatch = EnvelopeBatch(
            listOf(envelope),
            EnvelopeBatchProvenance("Crossref Test", "1234"),
        )

        itemGraphSetup.waitForIngestions(listOf(itemGraph.ingest(listOf(envelopeBatch))))
    }

    private fun getMergeStrategy(merge: String): MergeStrategy {
        val mergeStrategy = when (merge) {
            "naive" -> MergeStrategy.NAIVE
            "none" -> MergeStrategy.NONE
            "union-closed" -> MergeStrategy.UNION_CLOSED
            "closed" -> MergeStrategy.CLOSED
            else -> null
        }
        assertNotNull(mergeStrategy, "Not recognised merge strategy")
        return mergeStrategy!!
    }

    @Then("the Item Graph contains Item {string}")
    fun the_item_graph_contains_item(identifier: String) {
        val result = resolver.resolveRO(Identifier.new(identifier))

        assertNotNull(result.pk, "Expected to  find Item $identifier in the Item Graph")
    }

    @Then("the Item Graph contains members")
    fun the_item_graph_contains_members(members: List<Statements.Member>) {
        members.forEach { m ->
            val result = resolver.resolveRO(Identifier.new(m.identifier))
            assertNotNull(result.pk, "Expected to find Item ${m.identifier} in the Item Graph")
            val properties = propertyDao.getCurrentSubjPropertyStatements(setOf(result.pk!!))
            assertEquals(
                m.name,
                properties.first().values.get("name").textValue(),
                "Expected to find name property with value ${m.name} for ${m.identifier}",
            )
        }
    }

    @Then("the Item Graph does not contain members")
    fun the_item_graph_does_not_contain_members(members: List<Statements.Member>) {
        members.forEach { m ->
            val result = resolver.resolveRO(Identifier.new(m.identifier))
            assertNull(result.pk, "Expected not to find Item ${m.identifier} in the Item Graph")
        }
    }
}
