package org.crossref.manifold.common

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.crossref.manifold.rendering.Configuration
import org.crossref.manifold.rendering.RenderStatusDao
import org.junit.jupiter.api.Assertions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import software.amazon.awssdk.services.sqs.SqsAsyncClient
import software.amazon.awssdk.services.sqs.model.GetQueueAttributesRequest
import software.amazon.awssdk.services.sqs.model.GetQueueUrlRequest
import software.amazon.awssdk.services.sqs.model.QueueAttributeName

@Component
class RenderingUtil(
    @Autowired
    val renderStatusDao: RenderStatusDao,
    ) {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    fun waitForQueueToEmpty() {
        var queueLength: Long?
        var retries = 0
        do {
            queueLength = renderStatusDao.queueLength()
            logger.info("Waiting for render queue to emtpy. Size: $queueLength")
            if (queueLength != 0L) {
                runBlocking {
                    delay(500L)
                }
            }
        } while (queueLength != 0L && ++retries < 30)
        Assertions.assertEquals(0, queueLength, "Timed out waiting for queue length to be 0. Was: $queueLength")
    }

}