package org.crossref.manifold.common

import io.cucumber.java.en.Given
import io.cucumber.java.en.When
import org.apache.commons.io.IOUtils
import org.crossref.manifold.bulk.BulkIngestionService
import org.crossref.manifold.ingestion.TempFile
import org.crossref.manifold.modules.unixml.UniXMLSnapshotConverter
import org.crossref.manifold.modules.unixml.XmlSingleIngester
import org.crossref.manifold.util.TarGzWriter
import org.junit.jupiter.api.Assertions
import org.springframework.beans.factory.annotation.Autowired
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory

fun resource(filename: String) = Thread.currentThread().contextClassLoader
    .getResourceAsStream("xml/$filename")!!

/** Does the given XML contain the XPath?)
 */
fun hasXpath(filename: String, xPath: String): Boolean {
    resource(filename).use {
        val factory = DocumentBuilderFactory.newInstance()
        val builder = factory.newDocumentBuilder()
        val document = builder.parse(it)
        val xpath: XPath = XPathFactory.newInstance().newXPath()
        val result = xpath.evaluate(xPath, document, XPathConstants.NODE)
        return result != null
    }
}

/**
 * The Given step definitions here refer to an existing file in the test resources directory, which we expect to be
 * ready-made for the test. They don't construct the XML file, but they do verify the content to be sure that it says
 * what we expected it to say.
 */
class UniXML(
    private val itemGraphSetup: ItemGraphSetup,
    @Autowired
    val ingester: XmlSingleIngester,

    @Autowired
    var archiveIngester: BulkIngestionService,

    @Autowired
    var renderingUtil: RenderingUtil,

    private val tempFile: TempFile,
) {
    /**
     * DOI is expressed without resolver (e.g. 10.5555/12345678) in the XML.
     */
    @Given("UniXSD file {string} registers Journal DOI {string}")
    fun xml_file_contains_journal(filename: String, doi: String) {
        Assertions.assertTrue(
            hasXpath(filename, "//doi_record/crossref/journal/journal_metadata/doi_data/doi[text()='$doi']"),
            "Expected Journal DOI $doi"
        )
    }

    /**
     * DOI is expressed without resolver (e.g. 10.5555/12345678) in the XML.
     */
    @Given("UniXSD file {string} registers Book DOI {string}")
    fun xml_file_contains_book(filename: String, doi: String) {
        Assertions.assertTrue(
            hasXpath(filename, "//doi_record/crossref/book/book_metadata/doi_data/doi[text()='$doi']"),
            "Expected Book DOI $doi"
        )
    }

    /**
     * DOI is expressed without resolver (e.g. 10.5555/12345678) in the XML.
     */
    @Given("UniXSD file {string} registers Book Chapter DOI {string}")
    fun xml_file_contains_book_chapter(filename: String, doi: String) {
        Assertions.assertTrue(
            hasXpath(filename, "//doi_record/crossref/book/content_item/doi_data/doi[text()='$doi']"),
            "Expected Book Chapter DOI $doi"
        )
    }

    /**
     * DOI is expressed without resolver (e.g. 10.5555/12345678) in the XML.
     */
    @Given("UniXSD file {string} registers Conference Paper DOI {string}")
    fun xml_file_contains_conference_paper(filename: String, doi: String) {
        Assertions.assertTrue(
            hasXpath(filename, "//doi_record/crossref/conference/conference_paper/doi_data/doi[text()='$doi']"),
            "Expected Conference Paper DOI $doi"
        )
    }

    /**
     * DOI is expressed without resolver (e.g. 10.5555/12345678) in the XML.
     */
    @Given("UniXSD file {string} registers Standalone Component DOI {string}")
    fun xml_file_contains_sa_component(filename: String, doi: String) {
        Assertions.assertTrue(
            hasXpath(
                filename,
                "//doi_record/crossref/sa_component/component_list/component/doi_data/doi[text()='$doi']"
            ),
            "Expected Component DOI $doi"
        )
    }

    /**
     * DOI is expressed without resolver (e.g. 10.5555/12345678) in the XML.
     */
    @Given("UniXSD file {string} registers Article DOI {string}")
    fun xml_file_contains_article_doi(filename: String, doi: String) {
        Assertions.assertTrue(
            hasXpath(filename, "//doi_record/crossref/journal/journal_article/doi_data/doi[text()='$doi']"),
            "Expected Article DOI $doi"
        )
    }

    @Given("UniXSD file {string} has crm-item with Member ID {string}")
    fun xml_file_has_crm_item_with_member_id(filename: String, memberId: String) {
        Assertions.assertTrue(
            hasXpath(filename, "//crm-item[@name='member-id'][text()='$memberId']"),
            "Expected to find member id $memberId"
        )
    }

    @Given("UniXSD file {string} has crm-item with Publisher Name {string}")
    fun xml_file_has_crm_item_with_publisher_name(filename: String, publisherName: String) {
        Assertions.assertTrue(
            hasXpath(filename, "//crm-item[@name='publisher-name'][text()='$publisherName']"),
            "Expected to find publisher-name id $publisherName",
        )
    }

    @When("UniXSD file {string} is ingested")
    fun the_xml_file_is_processed_by_the_uni_xml_snapshot_parser(filename: String) {
        itemGraphSetup.waitForIngestions(listOf(ingester.ingest(filename, resource(filename).reader().use { it.readText() })))
    }

    @Given("UniXSD file {string} has been ingested")
    fun the_xml_file_has_been_processed_by_the_uni_xml_snapshot_parser(filename: String) {
        itemGraphSetup.waitForIngestions(listOf(ingester.ingest(filename, resource(filename).reader().use { it.readText() })))
    }

    @Given("an XML Snapshot file named {string} containing XML file {string}")
    fun an_xml_snapshot_file_named_containing(snapshotFilename: String, xmlFilename: String) {
        // Just create a tar file with a single entry.
        TarGzWriter(tempFile[snapshotFilename]).use {
            it.add(xmlFilename) { outputStream ->
                IOUtils.copy(resource(xmlFilename), outputStream)
            }
        }
    }

    @When("XML Snapshot file {string} is converted to a bulk file {string} and the bulk file is ingested")
    fun the_xml_snapshot_file_is_converted_to_a_bulk_file_and_the_bulk_file_is_ingested(
        snapshotFilename: String,
        bulkFilename: String,
    ) {
        UniXMLSnapshotConverter.run(tempFile[snapshotFilename], tempFile[bulkFilename])
        itemGraphSetup.waitForIngestions(archiveIngester.ingestFiles(listOf(tempFile[bulkFilename])))
        renderingUtil.waitForQueueToEmpty()
    }
}
