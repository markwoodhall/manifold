package org.crossref.manifold.common

import io.cucumber.java.After
import io.cucumber.java.Before
import io.cucumber.java.Scenario
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.mockk.clearMocks
import io.mockk.every
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.crossref.manifold.db.BufferBatchDao
import org.crossref.manifold.db.BufferType
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.junit.Assert
import org.junit.jupiter.api.Assertions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import java.time.InstantSource
import java.util.*

class ItemGraphSetup(
    private val itemGraph: ItemGraph,
    private val jdbcTemplate: JdbcTemplate,
    private val resolver: Resolver,
    private val instantSource: InstantSource,
    private val bufferBatchDao: BufferBatchDao,
) {
    companion object {
        private val PROVENANCE = EnvelopeBatchProvenance("Crossref Test", "1234")
    }

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Before
    fun logStart(scenario: Scenario) {
        logger.info("Start scenario: ${scenario.name} ... ")
    }

    @After
    fun logEnd(scenario: Scenario) {
        // It's possible that a test ran that didn't need to wait for these buffers to empty,
        // so didn't already wait for these.
        bufferBatchDao.waitForBuffer(BufferType.PROPERTY_ASSERTION)
        bufferBatchDao.waitForBuffer(BufferType.RELATIONSHIP_ASSERTION)
        logger.info("Finish scenario: ${scenario.name}")
    }

    // By convention, scenarios start with an empty item graph.
    @Before
    fun truncateItemGraphTables() {
        jdbcTemplate.execute("TRUNCATE envelope_batch CASCADE")
        jdbcTemplate.execute("TRUNCATE item CASCADE")
        jdbcTemplate.execute("TRUNCATE item_tree_reachability CASCADE")
        jdbcTemplate.execute("TRUNCATE open_cursor CASCADE")
        jdbcTemplate.execute("UPDATE buffer_count SET row_count = 0, processor_running = FALSE")
    }

    @Before
    fun initInstantSource() {
        clearMocks(instantSource)
        // We can't spy on instantSource directly since SystemInstantSource is non-public.
        // Instead we use a pure mock and replicate the default behaviour.
        every { instantSource.instant() } returns InstantSource.system().instant()
    }

    @Given("the following relationship assertions in the item graph")
    fun the_following_relationship_assertions_in_the_item_graph(statements: List<Statements.RelationshipAssertionStatement>) {
        if (statements.any { it.ingestedAt != null }) {
            clearMocks(instantSource)
            every { instantSource.instant() } returnsMany statements.map {
                it.ingestedAt?.toInstant() ?: InstantSource.system().instant()
            }
        }

        waitForIngestion(
            itemGraph.ingest(
                listOf(
                    EnvelopeBatch(
                        statements.map { statement ->
                            val item = Item()
                                .withIdentifier(Identifier.new(statement.subjectId))
                                .withBlank(statement.subjectBlank)
                                .withRelationship(
                                    Relationship(
                                        statement.relationshipType,
                                        Item().withIdentifier(Identifier.new(statement.objectId)).withBlank(statement.objectBlank)
                                    )
                                )

                            val assertion = ItemTreeAssertion(
                                statement.assertedAt,
                                MergeStrategy.NAIVE,
                                Item().withIdentifier(Identifier.new(statement.assertedBy))
                            )

                            Envelope(listOf(item), assertion)
                        }, PROVENANCE
                    )
                )
            )
        )
    }


    @Given("the following property assertions in the item graph")
    fun the_following_property_assertions_in_the_item_graph(statements: List<Statements.PropertyAssertionStatement>) {

        val envelopes = statements.map { statement ->
            val item = Item()
                .withIdentifier(Identifier.new(statement.subjectId))
                .withPropertiesFromMap(mapOf(statement.property to statement.value))

            val assertion = ItemTreeAssertion(
                statement.assertedAt,
                MergeStrategy.NAIVE,
                Item().withIdentifier(Identifier.new(statement.assertedBy))
            )

            Envelope(listOf(item), assertion)
        }

        waitForIngestions(
            listOf(
            itemGraph.ingest(
                listOf(
                    EnvelopeBatch(
                        envelopes, PROVENANCE
                    )
                )
            )
            )
        )
    }

    /**
     * This one specifically asserts all the property values in one assertion.
     */
    @Given("the following multi-valued property assertion in the item graph")
    fun the_following_multi_property_assertions_in_the_item_graph(statements: List<Statements.PropertyAssertionStatement>) {

        Assert.assertTrue(
            "All statements must be asserted by the same party",
            statements.map { it.assertedBy }.toSet().count() == 1
        )

        Assert.assertTrue(
            "All subject ids should be the same",
            statements.map { it.subjectId }.toSet().count() == 1
        )

        val assertedBy = statements.first().assertedBy
        val subjectId = statements.first().subjectId
        val assertedAt = statements.first().assertedAt

        val allProperties = statements.associate { it.property to it.value }
        val item = Item()
            .withIdentifier(Identifier.new(subjectId))
            .withPropertiesFromMap(allProperties)

        val assertion =
            ItemTreeAssertion(assertedAt, MergeStrategy.NAIVE, Item().withIdentifier(Identifier.new(assertedBy)))

        waitForIngestion(
            itemGraph.ingest(
                listOf(
                    EnvelopeBatch(
                        listOf(Envelope(listOf(item), assertion)), PROVENANCE
                    )
                )
            )
        )
    }


    /**
     * This Given doesn't do anything other than serve as a placeholder to make tests clear when there is no prior data
     * in the item graph.
     * The clearing is done by [truncateItemGraphTables].
     */
    @Given("an empty Item Graph database")
    fun empty_item_graph() = Unit


    @Then("the following relationship statements are found in the item graph")
    fun the_item_graph_contains_current_relationship_statements(
        expectedStatements: List<Statements.RelationshipAssertionStatement>,
    ) {
        expectedStatements.forEach {
            val foundStatements = itemGraph.getRelationshipStatements(
                Identifier.new(it.subjectId),
                it.relationshipType,
                Identifier.new(it.objectId),
                Identifier.new(it.assertedBy),
                current = it.current,
                state = it.state,
            )

            Assertions.assertEquals(it.count, foundStatements.count())
        }
    }

    @Then("the following property statements are found in the item graph")
    fun the_item_graph_contains_current_property_statements(
        expectedStatements: List<Statements.PropertyAssertionStatement>,
    ) {
        expectedStatements.forEach { statement ->
            val foundStatements = itemGraph.getPropertyStatements(
                Identifier.new(statement.subjectId),
                Identifier.new(statement.assertedBy),
                statement.current
            )
            val matching = foundStatements.filter {
                it.values.get(statement.property)?.asText() == statement.value
            }

            Assertions.assertEquals(statement.count, matching.count())
        }
    }

    @Given("the following Items in the Item Graph")
    fun the_following_items_are_in_the_item_graph(items: List<Statements.Item>) {
        items.forEach { item ->
            val resolvedItem = resolver.resolveRO(Identifier.new(item.identifier))
            Assertions.assertNotNull(resolvedItem, "Expected to resolved item $item")
            val createdItem = resolver.resolveSynchronousRW(listOf(resolvedItem)).firstOrNull()
            Assertions.assertNotNull(createdItem, "Expected to find item with identifier $item")
        }
    }

    fun waitForIngestion(ingestionId: UUID) = waitForIngestions(listOf(ingestionId))
    fun waitForIngestions(ingestionIds: Collection<UUID>) {
        var retries = 0
        var runningIngestionIds = ingestionIds.filter { itemGraph.isIngestionRunning(it) }
        while (runningIngestionIds.isNotEmpty() && ++retries < 60) {
            runBlocking {
                delay(1000L)
            }
            runningIngestionIds = ingestionIds.filter { itemGraph.isIngestionRunning(it) }
        }
        Assertions.assertTrue(
            runningIngestionIds.isEmpty(),
            "Expected ingestions to be finished but the following IDs were still active: $runningIngestionIds"
        )
    }
}
