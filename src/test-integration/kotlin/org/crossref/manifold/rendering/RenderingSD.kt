package org.crossref.manifold.rendering

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import io.cucumber.java.After
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.crossref.manifold.api.Entry
import org.crossref.manifold.api.Feed
import org.crossref.manifold.common.ApiResponseContext
import org.crossref.manifold.common.ItemGraphSetup
import org.crossref.manifold.common.RenderingUtil
import org.crossref.manifold.common.Statements
import org.crossref.manifold.db.BufferBatchDao
import org.crossref.manifold.db.BufferType
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.*
import org.crossref.manifold.itemtree.Properties
import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.net.URI
import java.net.URL
import java.net.URLDecoder
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

class RenderingSD(
    @Autowired
    val renderStatusDao: RenderStatusDao,

    @Autowired
    val renderedItemDao: RenderedItemDao,

    @Autowired
    val renderedItemStorageDao: RenderedItemStorageDao,

    @Autowired
    val resolver: Resolver,

    @Autowired
    val jdbcTemplate: JdbcTemplate,

    @Autowired
    val itemGraph: ItemGraph,

    @Autowired
    val renderingUtil: RenderingUtil,

    @Autowired
    val itemTreeRenderer: ItemTreeRenderer,

    val itemGraphSetup: ItemGraphSetup,

    val bufferBatchDao: BufferBatchDao,

    private val apiResponseContext: ApiResponseContext,
) {


    val logger: Logger = LoggerFactory.getLogger(this::class.java)
    val mapper = XmlMapper()
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    // Store feed results between steps.
    private var feedPages = mutableListOf<Feed>()
    private var foundEntry: Entry? = null

    @Before
    fun beforeEach() {
         resetRendering()
    }


    /**
     * Wait for render-related activity to stop before ending the test.
     *
     */
    @After
    fun waitForQueues() {
        bufferBatchDao.waitForBuffer(BufferType.ITEM_IDENTIFIER)
        renderingUtil.waitForQueueToEmpty()
    }

    /**
     * Reset the rendering system.
     * No items will be marked stale, pending render, or rendered.
     * The item tree reachability index isn't touched.
     */
    private fun resetRendering() {
        // Wait for queue to report empty.
        // Otherwise, the TRUNCATE on rendered_item and item_render_status will deadlock.
        renderingUtil.waitForQueueToEmpty()

        // Remove all render flags, including anything stale.
        // This will avoid any extra items being added to the queue.
        jdbcTemplate.update("TRUNCATE item_render_status_queue RESTART IDENTITY CASCADE")

        // Remove all rendered items.
        jdbcTemplate.update("TRUNCATE rendered_item RESTART IDENTITY CASCADE")
    }



    /**
     * Tries to resolve an item by its identifier or primary key, and
     * asserts that the item has been resolved
     * @return the resolved [Item]
     */
    fun resolveAndCheckItem(identifier: String): Item {
        val resolvedItem: Item = identifier.toLongOrNull()?.let {
            resolver.resolveRO(Item(pk = identifier.toLong()))
        } ?: resolver.resolveRO(Identifier.new(identifier))
        Assertions.assertNotNull(resolvedItem, "Expected to find item with identifier $identifier")
        Assertions.assertNotNull(resolvedItem.pk, "Expected to find pk for $identifier")
        return resolvedItem
    }

    /**
     * Tries to resolve a string representation of [ContentType], and
     * asserts that the the [contentType] has been resolved
     * @return the resolved Item
     */
    fun resolveAndCheckContentType(contentType: String): ContentType {
        val resolvedContentType = ContentType.fromMimeType(contentType)
        Assertions.assertNotNull(
            resolvedContentType!!,
            "Expected to be able to resolve $contentType value to ContentType enum"
        )
        return resolvedContentType
    }

    /**
     * Try to find a RenderedItem for the @param[item] and @param[contentType].
     * This should run after all rendering has completed, so it expects to find it immediately.
     */
    private fun checkForRenderedItems(item: Item, contentType: ContentType): Collection<RenderedItem> {
        var renderedItems = renderedItemDao.getHistoricalRenderedItems(item.pk!!, contentType.mimeType)

        Assertions.assertFalse(renderedItems.isEmpty(), "Expected to find rendered items for $item and $contentType")
        for (renderedItem in renderedItems) {
            Assertions.assertNotEquals(
                "",
                renderedItem.pointer,
                "Expected to find rendered item pointer for $item and $contentType"
            )
            Assertions.assertNotEquals(
                "",
                renderedItem.hash,
                "Expected to find rendered item hash for $item and $contentType"
            )

        }

        return renderedItems
    }

    fun addItemToCollection(identifier: String, collection: String, assertedBy: String) {
        val envelope = Envelope(
            listOf(
                Item().withIdentifier(Identifier.new(identifier))
                    .withRelationship(
                        Relationship(
                            "collection",
                            Item()
                                .withIdentifier(Identifier.new(collection)),
                        ),
                    ),
            ),
            ItemTreeAssertion(
                OffsetDateTime.now(),
                MergeStrategy.NAIVE,
                Item().withIdentifier(Identifier.new(assertedBy)),
            ),
        )

        val envelopeBatch = EnvelopeBatch(
            listOf(envelope),
            EnvelopeBatchProvenance("Crossref Test", "1234"),
        )

        itemGraphSetup.waitForIngestions(listOf(itemGraph.ingest(listOf(envelopeBatch))))
    }

    fun checkEtagForVersion(version: Int, contentType: String) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.log())
            .andExpect(status().isOk)
            .andReturn()

        val eTag = result.response.getHeader("ETag")
        val identifier = result.request.requestURI?.split("/v1/items/")?.last()

        Assertions.assertNotNull(identifier, "Expected to find identifier from request")

        val resolvedItem = resolveAndCheckItem(identifier!!)
        val resolvedContentType = resolveAndCheckContentType(contentType)

        val renderedItem = renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType)
            .drop(version)
            .firstOrNull()
        val hash = renderedItem?.hash

        Assertions.assertEquals("\"" + hash + "\"", eTag, "Expected api $eTag to match the stored hash $hash")
    }

    fun checkDataForVersion(version: Int, contentType: String) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.log())
            .andExpect(status().isOk)
            .andReturn()

        val identifier = result.request.requestURI?.split("/v1/items/")?.last()
        val content = result.response.contentAsString

        Assertions.assertNotNull(identifier, "Expected to find identifier from request")

        val resolvedContentType = resolveAndCheckContentType(contentType)
        val resolvedItem = resolveAndCheckItem(identifier!!)
        val renderedItem = renderedItemStorageDao.fetchContent(
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version).first(),
        )

        Assertions.assertEquals(
            renderedItem.content,
            content,
            "Expected api $content to match the stored blob ${renderedItem.content}"
        )
    }

    fun checkLastModifiedForVersion(version: Int, contentType: String) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.log())
            .andExpect(status().isOk)
            .andReturn()

        val identifier = result.request.requestURI?.split("/v1/items/")?.last()
        val apiLastModified = result.response.getHeader("Last-Modified")

        val resolvedContentType = resolveAndCheckContentType(contentType)

        Assertions.assertNotNull(identifier, "Expected to find identifier from request")

        val resolvedItem = resolveAndCheckItem(identifier!!)
        val renderedItem =
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version).firstOrNull()
        val storeLastModified = renderedItem?.updatedAt
        val formatter =
            DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US).withZone(ZoneId.of("GMT"))
        val lastModified = formatter.format(storeLastModified)

        Assertions.assertEquals(
            lastModified,
            apiLastModified,
            "Expected api Last-Modified $apiLastModified to match the stored last modified $lastModified"
        )
    }

    fun getApiResponseAsFeed(): Feed {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.log())
            .andExpect(status().isOk)
            .andReturn()

        return mapper.readValue(result.response.contentAsString, Feed::class.java)
    }

    @Given("all items have been allowed to render and are no longer stale")
    fun all_items_no_longer_stale() {
        renderingUtil.waitForQueueToEmpty()
    }

    /**
     * The Item Tree reachability table is out of scope here. We want to keep that intact during a scenario (it's
     * cleared at the start of each scenario).
     */
    @Given("no Items have been rendered yet")
    fun no_items_have_been_rendered_yet() {
        logger.info("Truncating rendered_item")
        resetRendering()
    }

    @Given("the following Items have the following stored rendered representations")
    fun the_following_Items_have_the_following_stored_renderer_representations(items: List<Statements.Item>) {
        no_items_have_been_rendered_yet()
        items.forEach { item ->
            val identifier = item.identifier
            val contentType = item.contentType
            Assertions.assertNotNull(
                contentType,
                "Expected Content Type to be non null, it should be a mimiType resolvable by ContentType"
            )

            val resolvedItem = resolver.resolveRO(Identifier.new(identifier))
            Assertions.assertNotNull(resolvedItem, "Expected to resolved item $item")

            var createdItem = resolver.resolveSynchronousRW(listOf(resolvedItem)).firstOrNull()
            Assertions.assertNotNull(createdItem, "Expected to find item with identifier $identifier")
            Assertions.assertNotNull(createdItem!!.pk, "Expected to find pk for $createdItem")

            createdItem = createdItem.withProperties(
                listOf(
                    Properties(
                        jsonObjectFromMap(mapOf("title-long" to listOf(mapOf("value" to item.title)))),
                    ),
                ),
            )

            addItemToCollection(identifier, "https://id.crossref.org/collections/work", "https://ror.org/02twcfp32")
            itemTreeRenderer.renderContentType(createdItem, contentType!!)

            // Alter the updated_at of the rendered item if one is supplied
            if (item.updatedAt != null) {
                npTemplate.update(
                    "UPDATE rendered_item SET updated_at = :updated_at WHERE item_pk = :item_pk",
                    mapOf(
                        "updated_at" to item.updatedAt,
                        "item_pk" to createdItem.pk,
                    ),
                )
            }
        }
        renderingUtil.waitForQueueToEmpty()
    }

    /**
     * Check the rendered status of a list of items using the 'Identifier' and 'Rendered?' fields.
     * This can check for the presence or absence of rendered items.
     * This waits for the render queue / loop to run, so it can wait for a row to appear for True.
     * For False it's checking for the absence, so that's less rigorous (e.g. a delay in rendering might be missed).
     * Therefore, put any 'false' ones at the end of the scenario after any delays have run.
     */
    @Then("Items are or aren't rendered")
    fun the_following_items_are_rendered(items: List<Statements.Item>) {

        // Then wait for all the stale to have been processed, which means the renderer has seen them all.
        renderingUtil.waitForQueueToEmpty()

        items.forEach { item ->
            val resolvedItem = resolveAndCheckItem(item.identifier)

            when (item.rendered) {
                true -> {
                    // True, wait for the row to appear.
                    val renders = checkForRenderedItems(resolvedItem, item.contentType!!)

                    if (item.numRenders != null) {
                        Assertions.assertEquals(item.numRenders, renders.count(), "Expected number of renders")
                    }
                }

                false -> {
                    // False, don't wait. This is less rigorous.
                    val result = renderedItemDao.getRenderedItems(resolvedItem.pk!!)
                    Assertions.assertTrue(
                        result.isEmpty(),
                        "Expected to find no renders for ${item.identifier}. Found $result."
                    )
                }

                else -> {
                    Assertions.fail("Expected Render status to be true or false.")
                }
            }
        }
    }

    /**
     * Checking to see if an item is rendered relies on StaleItemSqsWriter and
     * StaleItemSqsReader working in the background to publish/process items to/from
     * the rendering queue.
     */
    @Then("the Item {string} is rendered as {string}")
    fun the_item_is_rendered_as(identifier: String, contentType: String) {
        val resolvedContentType = resolveAndCheckContentType(contentType)
        val resolvedItem = resolveAndCheckItem(identifier)
        checkForRenderedItems(resolvedItem, resolvedContentType)
    }

    @Then("the ETag header should match the most recent hash in storage for {string}")
    fun the_etag_header_should_match_the_most_recent_hash_in_storage(contentType: String) {
        checkEtagForVersion(0, contentType)
    }

    @Then("the ETag header should match {int} versions old hash in storage for {string}")
    fun the_etag_header_should_match_the_version_hash_in_storage(version: Int, contentType: String) {
        checkEtagForVersion(version, contentType)
    }

    @Then("the status code should be 4XX client error")
    fun the_status_should_be_client_error() {
        apiResponseContext.andDo(MockMvcResultHandlers.log())
            .andExpect(status().is4xxClientError)
            .andReturn()
    }

    @Then("the Content-Type header should be {string}")
    fun the_content_type_header_should_be(contentType: String) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.log())
            .andExpect(status().isOk)
            .andReturn()

        val apiContentType = result.response.getHeader("Content-Type")
        Assertions.assertEquals(
            contentType,
            apiContentType,
            "Expected api Content-Type $apiContentType to match $contentType"
        )
    }

    @Then("the Link header should be {string}")
    fun the_link_header_should_be(link: String) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.log())
            .andExpect(status().isOk)
            .andReturn()

        val apiFeedLink = URLDecoder.decode(result.response.getHeader("Link"), StandardCharsets.UTF_8)
        Assertions.assertEquals(link, apiFeedLink, "Expected api Feed Link $apiFeedLink to match $link")
    }

    @Then("the Last-Modified header should match the most recent one in storage for {string}")
    fun the_last_modified_header_should_match_the_most_recent_one_in_storage(contentType: String) {
        checkLastModifiedForVersion(0, contentType)
    }

    @Then("the Last-Modified header should match {int} versions old in storage for {string}")
    fun the_last_modified_header_should_match_the_most_recent_one_in_storage(version: Int, contentType: String) {
        checkLastModifiedForVersion(version, contentType)
    }

    @Then("the returned data should match the most recent one in storage for {string}")
    fun the_returned_data_should_match_the_most_recent_one_in_storage(contentType: String) {
        checkDataForVersion(0, contentType)
    }

    @Then("the returned data should match {int} versions old data in storage for {string}")
    fun the_returned_data_should_match_version_in_storage(version: Int, contentType: String) {
        checkDataForVersion(version, contentType)
    }

    @Then("the title of the returned Item should be {string}")
    fun the_title_of_the_returned_item_should_be(title: String) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.log())
            .andExpect(status().isOk)
            .andReturn()

        val identifier = result.request.requestURI?.split("/v1/items/")?.last()
        val content = result.response.contentAsString
        val jsonObject = JSONObject(content)
        val apiTitle = jsonObject.getJSONArray("title").getString(0)

        Assertions.assertNotNull(identifier, "Expected to find identifier from request")

        Assertions.assertEquals(apiTitle, title, "Expected api title $apiTitle to match $title")
    }

    @Then("the feed has no {string} link")
    fun the_feed_has_no_link(rel: String) {
        val feed = getApiResponseAsFeed()
        Assertions.assertTrue(feed.links.none { l -> l.rel == rel })
    }

    @Then("the feed has a {string} link href prefixed with the feed endpoint {string}")
    fun the_feed_has_a_link_href_prefixed_with_the_feed_endpoint(rel: String, endpoint: String) {
        val feed = getApiResponseAsFeed()
        val expected = "http://localhost:1234$endpoint"
        Assertions.assertEquals(expected, feed.links.first { l -> l.rel == rel }.href)
    }

    @Then("the feed has a {string} link href prefixed with the feed endpoint {string} and {int} for {int}")
    fun the_feed_has_a_link_href_prefixed_with_the_feed_endpoint_and_cursor(
        rel: String,
        endpoint: String,
        nextCursor: Int,
        rows: Int,
    ) {
        val feed = getApiResponseAsFeed()
        val expected = "http://localhost:1234$endpoint?cursor=$nextCursor&rows=$rows"
        Assertions.assertEquals(expected, feed.links.first { l -> l.rel == rel }.href)
    }

    @Then("the feed has a {string} link href prefixed with the feed endpoint {string} and {int} for {int} between {string} and {string}")
    fun the_feed_has_a_link_href_prefixed_with_the_feed_endpoint_and_cursor_between(
        rel: String,
        endpoint: String,
        nextCursor: Int,
        rows: Int,
        fromDate: String,
        untilDate: String,
    ) {
        val feed = getApiResponseAsFeed()
        val expected = "http://localhost:1234$endpoint?cursor=$nextCursor&rows=$rows&fromDate=$fromDate&untilDate=$untilDate"
        Assertions.assertEquals(expected, feed.links.first { l -> l.rel == rel }.href)
    }

    @Then("the feed has no entries")
    fun the_feed_has_no_entries() {
        Assertions.assertTrue(this.feedPages.isEmpty())
    }

    @Then("the feed has a {string} link href prefixed with the feed endpoint {string} for {string} and {string}")
    fun the_feed_has_a_self_href_prefixed_with_the_feed_endpoint_for(
        rel: String,
        endpoint: String,
        identifier: String,
        contentType: String,
    ) {
        val feed = getApiResponseAsFeed()
        val encodedContentType = URLEncoder.encode(contentType, StandardCharsets.UTF_8)
        val expected = "http://localhost:1234$endpoint?content-type=$encodedContentType&item=$identifier"
        Assertions.assertEquals(expected, feed.links.first { l -> l.rel == rel }.href)
    }

    @Then("that entry has id prefixed with the item endpoint for {string} with {string} and {int}")
    fun entry_is_prefixed_with_the_item_endpoint_for_identifier(
        identifier: String,
        contentType: String,
        version: Int,
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)
        val renderedItem =
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version - 1).first()

        Assertions.assertNotNull(renderedItem, "Expected to find a rendered item for ${resolvedItem.pk}")

        val prefix = "http://localhost:1234/v1/items/${resolvedItem.pk}"
        val mimeType = URLEncoder.encode(resolvedContentType.mimeType, StandardCharsets.UTF_8)
        val versionString = renderedItem.version
        val expected = "$prefix?content-type=$mimeType&version=$versionString"
        Assertions.assertEquals(
            expected,
            this.foundEntry?.id,
            "Expected the entry ID to be based on URL"
        )
    }

    /**
     * This one looks at the retrieved list.
     */
    @Then("there is an entry with id prefixed with the item endpoint for {string} with {string} and {int}")
    fun an_entry_id_is_prefixed_with_the_item_endpoint_for_identifier(
        identifier: String,
        contentType: String,
        version: Int,
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)
        val renderedItem =
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version - 1).first()

        Assertions.assertNotNull(renderedItem, "Expected to find a rendered item for ${resolvedItem.pk}")

        val prefix = "http://localhost:1234/v1/items/${resolvedItem.pk}"
        val mimeType = URLEncoder.encode(resolvedContentType.mimeType, StandardCharsets.UTF_8)
        val versionString = renderedItem.version
        val expected = "$prefix?content-type=$mimeType&version=$versionString"

        val found = this.feedPages.flatMap { it.entries }.firstOrNull() {
            it.id == expected
        }

        Assertions.assertNotNull(
            found,
            "Expected to find an entry with ID $expected in ${this.feedPages}"
        )

        this.foundEntry = found
    }

    @Then("there are {int} entries for {string} in {string}")
    fun there_are_n_entries_for_identifier_in_format(countExpected: Int, identifier: String, contentType: String) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)

        val mimeType = URLEncoder.encode(resolvedContentType.mimeType, StandardCharsets.UTF_8)

        val expected = "http://localhost:1234/v1/items/${resolvedItem.pk}?content-type=$mimeType"

        val countFound = this.feedPages.flatMap { it.entries }.filter {


            // Don't look for the version, just the item pk and type.

            it.id.startsWith(expected)
        }
        Assertions.assertEquals(countExpected, countFound, "Expected to find $countExpected entries for $identifier in content type $contentType")
    }

    @Then("that entry has {string} link href prefixed with the item endpoint for {string} with {string} and {int}")
    fun that_entry_related_link_href_is_prefixed_with_the_item_endpoint_for_identifer(
        rel: String,
        identifier: String,
        contentType: String,
        version: Int,
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)
        val renderedItem =
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version - 1).first()

        Assertions.assertNotNull(renderedItem, "Expected to find a rendered item for ${resolvedItem.pk}")

        val prefix = "http://localhost:1234/v1/items/${resolvedItem.pk}"
        val mimeType = resolvedContentType.mimeType
        val encodedMimeType = URLEncoder.encode(resolvedContentType.mimeType, StandardCharsets.UTF_8)
        val versionString = renderedItem.version
        val expected = "$prefix?content-type=$encodedMimeType&version=$versionString"

        val related = this.foundEntry?.links?.firstOrNull { l -> l.rel == "related" }

        Assertions.assertEquals(expected, related?.href, "Expected link to match for related")
        Assertions.assertEquals(rel, related?.rel, "Expected rel type to match for related")
        Assertions.assertEquals(mimeType, related?.type, "Expected mime type to match for related")
    }

    @Then("that entry has {string} link href prefixed with the item pointer for {string} with {string} and {int}")
    fun that_entry_enclosure_link_href_is_prefixed_with_the_item_pointer_for_identifer(
        rel: String,
        identifier: String,
        contentType: String,
        version: Int,
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)
        val renderedItem =
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version - 1).first()

        Assertions.assertNotNull(renderedItem, "Expected to find a rendered item for ${resolvedItem.pk}")

        val mimeType = resolvedContentType.mimeType
        val prefix = "s3://render-bucket/${renderedItem.pointer}"
        val enclosure = this.foundEntry?.links?.firstOrNull { l -> l.rel == "enclosure" }

        Assertions.assertEquals(prefix, enclosure?.href, "Expected link to match for enclosure")
        Assertions.assertEquals(rel, enclosure?.rel, "Expected rel to match for enclosure")
        Assertions.assertEquals(mimeType, enclosure?.type, "Expected mime type to match for enclosure")
    }

    @Then("that entry has {string} link href prefixed with the item endpoint for {string} with type {string}")
    fun that_entry_alternate_link_href_is_prefixed_with_the_item_endpoint_for_identifer(
        rel: String,
        identifier: String,
        contentType: String,
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)

        val prefix = "http://localhost:1234/v1/items/${resolvedItem.pk}"
        val mimeType = resolvedContentType.mimeType

        val alternate = foundEntry?.links?.firstOrNull { l -> l.rel == "alternate" }

        Assertions.assertEquals(prefix, alternate?.href, "Expected link to match for alternate")
        Assertions.assertEquals(rel, alternate?.rel, "Expected rel to match for alternate")
        Assertions.assertEquals(mimeType, alternate?.type, "Expected mime type to match for alternate")
    }

    @Then("that entry has updated equal to {int} for {string} with {string}")
    fun that_entry_has_update_equal_to_version_for_identifier_with_content_type(
        version: Int,
        identifier: String,
        contentType: String,
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)

        val renderedItem =
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version - 1).first()

        Assertions.assertNotNull(renderedItem, "Expected to find a rendered item for ${resolvedItem.pk}")

        val updatedAt = renderedItem.updatedAt.toString()
        Assertions.assertEquals(updatedAt, foundEntry?.updated)
    }

    @Then("that entry has title equal to Item {int} in {string} for {string}")
    fun entry_id_has_title_equal_to_version_for_identifier_with_content_type(
        version: Int,
        contentType: String,
        identifier: String,
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)

        val renderedItem =
            renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).drop(version - 1).first()

        Assertions.assertNotNull(renderedItem, "Expected to find a rendered item for ${resolvedItem.pk}")

        val titleString = "Item ${resolvedItem.pk} in ${resolvedContentType.mimeType}"
        Assertions.assertEquals(titleString, foundEntry?.title, "Expected title to match")
    }

    @When("the feed is retrieved from the {string} endpoint")
    fun the_feed_is_retrieved_from_the_endpoint(endpoint: String) {
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint")))
    }

    @When("all feed items are retrieved from the {string} endpoint, following cursors in pages of {int}")
    fun the_feed_for_item_and_content_type_is_retrieved_from_the_endpoint(
        endpoint: String,
        pageSize: Int,
    ) = retrieveAll("http://localhost:1234$endpoint?rows=$pageSize")

    @When("all feed items from {string} to {string} are retrieved from the {string} endpoint, following cursors in pages of {int}")
    fun the_feed_from_date_until_date_for_item_and_content_type_is_retrieved_from_the_endpoint(
        fromDate: String,
        untilDate: String,
        endpoint: String,
        pageSize: Int,
    ) = retrieveAll("http://localhost:1234$endpoint?rows=$pageSize&fromDate=$fromDate&untilDate=$untilDate")

    /**
     * Iterate all results, store result in this.feedPages.
     */
    private fun retrieveAll(url: String) {
        apiResponseContext.perform(get(URI(url)))

        this.feedPages = mutableListOf<Feed>()

        // Iterate whole feed.
        var feed = getApiResponseAsFeed()
        while (feed.entries.isNotEmpty()) {
            val next = feed.links.firstOrNull { l -> l.rel == "next" }
            feedPages.add(feed)

            apiResponseContext.perform(get(URI(next?.href)))
            feed = getApiResponseAsFeed()
        }
    }

    @When("the feed between {string} and {string} is retrieved from the {string} endpoint")
    fun the_feed_between_is_retrieved_from_the_endpoint(fromDate: String, untilDate: String, endpoint: String) {
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint?fromDate=$fromDate&untilDate=$untilDate")))
    }

    @When("{int} feed items after {int} between {string} and {string} are retrieved from the {string} endpoint")
    fun feed_items_after_cursor_and_between_dates_are_retrieved_from_the_endpoint(rows: Int, cursor: Int, fromDate: String, untilDate: String, endpoint: String) {
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint?rows=$rows&cursor=$cursor&fromDate=$fromDate&untilDate=$untilDate")))
    }

    @When("the feed for {string} and {string} is retrieved from the {string} endpoint")
    fun the_feed_for_item_and_content_type_is_retrieved_from_the_endpoint(
        identifier: String,
        contentType: String,
        endpoint: String,
    ) {
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint?item=$identifier&content-type=$contentType")))
    }

    @When("{string} is retrieved from the {string} endpoint")
    fun is_retrieved_from_the_endpoint(identifier: String, endpoint: String) {
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint$identifier")))
    }

    @When("{string} is retrieved from the {string} endpoint by its PK")
    fun is_retrieved_from_the_endpoint_by_its_pk(identifier: String, endpoint: String) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val pk = resolvedItem.pk
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint$pk")))
    }

    @When("retrieving a version of {string} from the {string} endpoint without a content type")
    fun retrieving_a_version_without_a_content_type(identifier: String, endpoint: String) {
        resolveAndCheckItem(identifier)
        val version = OffsetDateTime.now()
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint$identifier?version=$version")))
    }

    @When("retrieving a version of {string} from the {string} endpoint as {string} without a version")
    fun retrieving_a_version_without_a_content_type(identifier: String, endpoint: String, contentType: String) {
        resolveAndCheckItem(identifier)
        resolveAndCheckContentType(contentType)
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint$identifier?content-type=$contentType")))
    }

    @When("retrieving {string} at {int} versions old from the {string} endpoint with content type {string}")
    fun retrieving_at_versions_old_from_the_endpoint_with_content_type(
        identifier: String,
        version: Int,
        endpoint: String,
        contentType: String,
    ) {
        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)
        val pk = resolvedItem.pk!!

        val versions = renderedItemDao.getRenderedItemHistory(pk, resolvedContentType)
        val renderedItem = versions.drop(version).firstOrNull()

        Assertions.assertNotNull(
            renderedItem,
            "Expected to find renderedItem $version older than the current with content type $contentType"
        )

        val renderedVersion = renderedItem?.version
        apiResponseContext.perform(get(URI("http://localhost:1234$endpoint$pk?version=$renderedVersion&content-type=$contentType")))
    }

    @When("{string} is added to {string} by {string}")
    fun item_is_added_to_collection_and_is_stale(item: String, collection: String, assertedBy: String) {
        addItemToCollection(item, collection, assertedBy)
    }
}
