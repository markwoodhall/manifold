package org.crossref.manifold.relationship

import com.fasterxml.jackson.databind.ObjectMapper
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.crossref.manifold.api.relationship.RelationshipsResponse
import org.crossref.manifold.common.ApiResponseContext
import org.crossref.manifold.common.Statements
import org.crossref.manifold.itemtree.Identifier
import org.junit.jupiter.api.Assertions
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class RelationshipsSD(private val apiResponseContext: ApiResponseContext, private val mapper: ObjectMapper) {
    @When("all relationships are requested")
    fun all_relationships_are_requested() {
        apiResponseContext.perform(get("/v1/relationships/"))
    }

    @When("relationships for item {string} are requested")
    fun relationships_for_item_are_requested(itemId: String) {
        relationshipsAreRequestedFor("item", itemId)
    }

    @When("relationships for subject {string} are requested")
    fun relationships_for_subject_are_requested(subjectId: String) {
        relationshipsAreRequestedFor("subject", subjectId)
    }

    @When("relationships for object {string} are requested")
    fun relationships_for_object_are_requested(objectId: String) {
        relationshipsAreRequestedFor("object", objectId)
    }

    @When("{int} relationships for item {string} are requested")
    fun relationships_for_item_are_requested(rows: Int, itemId: String) {
        apiResponseContext.perform(get("/v1/relationships/").param("item-id", itemId).param("rows", rows.toString()))
    }

    @When("the first {int} relationships for item {string} are requested")
    fun the_first_relationships_for_item_are_requested(rows: Int, itemId: String) {
        apiResponseContext.perform(
            get("/v1/relationships/").param("item-id", itemId).param("rows", rows.toString())
                .param("cursor", "*")
        )
    }

    @When("the next {int} relationships for item {string} are requested")
    fun the_next_relationships_for_item_are_requested(rows: Int, itemId: String) {
        val nextCursor = readRelationshipsResponse().message.nextCursor
        apiResponseContext.perform(
            get("/v1/relationships/").param("item-id", itemId).param("rows", rows.toString())
                .param("cursor", nextCursor)
        )
    }

    @When("relationships for relationship type {string} are requested")
    fun relationships_for_relationship_type_are_requested(relationshipType: String) {
        apiResponseContext.perform(get("/v1/relationships/").param("relationship-type", relationshipType))
    }

    @When("relationships created after {string} are requested")
    fun relationships_created_after_are_requested(fromCreatedTime: String) {
        apiResponseContext.perform(get("/v1/relationships/").param("from-created-time", fromCreatedTime))
    }

    @When("relationships created before {string} are requested")
    fun relationships_created_before_are_requested(untilCreatedTime: String) {
        apiResponseContext.perform(get("/v1/relationships/").param("until-created-time", untilCreatedTime))
    }

    @When("relationships updated after {string} are requested")
    fun relationships_updated_after_are_requested(fromUpdatedTime: String) {
        apiResponseContext.perform(get("/v1/relationships/").param("from-updated-time", fromUpdatedTime))
    }

    @When("relationships updated before {string} are requested")
    fun relationships_updated_before_are_requested(untilUpdatedTime: String) {
        apiResponseContext.perform(get("/v1/relationships/").param("until-updated-time", untilUpdatedTime))
    }

    @Then("the following relationships are returned from the item graph")
    fun the_following_relationships_are_returned_from_the_item_graph(statements: List<Statements.RelationshipStatement>) {
        apiResponseContext.andDo(MockMvcResultHandlers.log()).andExpect(status().isOk).andExpect(
            content().contentType(MediaType.APPLICATION_JSON)
        )
        val relationshipsResponse = readRelationshipsResponse()
        Assertions.assertEquals(statements.size, relationshipsResponse.message.relationships.size)
        statements.forEach {
            Assertions.assertTrue(
                relationshipsResponseContainsStatementOnce(
                    relationshipsResponse,
                    it
                ),
                "Could not find $it in the response exactly once, looking in ${relationshipsResponse.message.relationships}."
            )
        }
    }

    @Then("{int} relationships are returned from the item graph")
    fun relationships_are_returned_from_the_item_graph(rows: Int) {
        apiResponseContext.andDo(MockMvcResultHandlers.log()).andExpect(status().isOk).andExpect(
            content().contentType(MediaType.APPLICATION_JSON)
        )
        val relationshipsResponse = readRelationshipsResponse()
        Assertions.assertTrue(relationshipsResponse.message.relationships.size == rows)
    }

    @Then("HTTP status {string} is returned")
    fun http_status_is_returned(httpStatus: String) {
        apiResponseContext.andDo(MockMvcResultHandlers.log()).andExpect(status().isOk).andExpect(
            content().contentType(MediaType.APPLICATION_JSON)
        )
        val relationshipsResponse = readRelationshipsResponse()
        Assertions.assertTrue(relationshipsResponse.status.name == httpStatus)
    }

    @Then("total row count {int} is returned")
    fun total_row_count_is_returned(totalCount: Int) {
        apiResponseContext.andDo(MockMvcResultHandlers.log()).andExpect(status().isOk).andExpect(
            content().contentType(MediaType.APPLICATION_JSON)
        )
        val relationshipsResponse = readRelationshipsResponse()
        Assertions.assertTrue(relationshipsResponse.message.totalResults == totalCount)
    }

    @Then("message type {string} is returned")
    fun message_type_is_returned(messageType: String) {
        apiResponseContext.andDo(MockMvcResultHandlers.log()).andExpect(status().isOk).andExpect(
            content().contentType(MediaType.APPLICATION_JSON)
        )
        val relationshipsResponse = readRelationshipsResponse()
        Assertions.assertTrue(relationshipsResponse.messageType.getLabel() == messageType)
    }

    private fun readRelationshipsResponse(): RelationshipsResponse =
        mapper.readValue(apiResponseContext.andReturn().response.contentAsString, RelationshipsResponse::class.java)

    private fun relationshipsResponseContainsStatementOnce(
        relationshipsResponse: RelationshipsResponse,
        relationshipStatement: Statements.RelationshipStatement,
    ): Boolean =
        relationshipsResponse.message.relationships.count {
            it.subject.identifiers.map { itemIdentifierView -> itemIdentifierView.identifier }
                .contains(relationshipStatement.subjectId)
                    && it.`object`.identifiers.map { itemIdentifierView -> itemIdentifierView.identifier }
                .contains(relationshipStatement.objectId)
                    && it.relationshipType == relationshipStatement.relationshipType
                    && (relationshipStatement.createdTime == null || relationshipStatement.createdTime == it.createdTime)
                    && (relationshipStatement.updatedTime == null || relationshipStatement.updatedTime == it.updatedTime)
        } == 1

    private fun relationshipsAreRequestedFor(identifierType: String, identifierId: String) {
        apiResponseContext.perform(get("/v1/relationships/").param("$identifierType-id", identifierId))
    }

}
