package org.crossref.manifold.modules.eventdata.ingester

import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.modules.eventdata.inputEventGeneric
import org.crossref.manifold.modules.eventdata.inputEventTwitter
import org.crossref.manifold.modules.eventdata.support.Author
import org.crossref.manifold.modules.eventdata.support.ingester.toItem
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory


internal class EventsConversionTest {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Test
    @Deprecated("Evaluate this test's usefulness after refactoring events conversion")
    fun testToItemGeneric() {
        val expected = Item(
            identifiers = listOf(
               Identifier.new(
                    "https://doi.org/10.1039/d0qi01407a",
                )
            ), rels = listOf(
                Relationship(
                    relTyp = "references", Item(
                        identifiers = listOf(
                           Identifier.new(
                                "https://doi.org/10.1021/ja203695h",
                            )
                        ), rels = emptyList()
                    )
                )
            )
        )

        assertEquals(expected, inputEventGeneric.toItem())
    }

    @Test
    @Deprecated("Evaluate this test's usefulness after refactoring events conversion")
    fun testToItemTwitter() {
        val expected = Item(
            identifiers = listOf(
               Identifier.new(
                    "twitter://status?id=xxxxxxxxx",
                )
            ), rels = listOf(
                Relationship(
                    relTyp = "discusses", Item(
                        identifiers = listOf(
                           Identifier.new(
                                 "https://doi.org/10.1111/(issn)1467-8748",
                            )
                        ), rels = emptyList()
                    )
                )
            )
        ).withPropertiesFromMap(
            mapOf(
                "title" to "Tweet xxxxxxxxx",
                "author" to Author("twitter://user?screen_name=xxxxxxxx"),
                "original-tweet-url" to "twitter://status?id=xxxxxxxxxxx",
                "original-tweet-author" to "twitter://user?screen_name=xxxxxxxxxxx",
                "alternative-id" to "0123456789"
            )
        )

        assertEquals(expected, inputEventTwitter.toItem())
    }
}