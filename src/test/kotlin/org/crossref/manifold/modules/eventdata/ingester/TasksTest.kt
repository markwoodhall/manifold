package org.crossref.manifold.modules.eventdata.ingester

import io.mockk.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.runBlocking
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.eventdata.EventSnapshotIngester
import org.crossref.manifold.modules.eventdata.envelopeBatchGeneric
import org.crossref.manifold.modules.eventdata.inputEventGeneric
import org.crossref.manifold.modules.eventdata.support.Event
import org.crossref.manifold.modules.eventdata.support.ingester.PARSE_PARALLELISM
import org.crossref.manifold.modules.eventdata.support.ingester.ingestEventFromChannel
import org.crossref.manifold.modules.eventdata.support.ingester.parseEventToEnvelopeBatch
import org.crossref.manifold.modules.eventdata.support.ingester.scanDirectory
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.concurrent.atomic.AtomicInteger

internal class TasksTest {

    @Test
    fun testSnapshotJsonFile() {
        val snapshotsDirectory = "abc"
        val itemGraph = mockk<ItemGraph>()
        mockkStatic(::scanDirectory)

        EventSnapshotIngester.ingestJsonSnapshot(null, itemGraph, snapshotsDirectory, emptySet())

        verify { scanDirectory("abc", itemGraph, emptySet(), null) }

        unmockkStatic(::scanDirectory)
    }

    @Test
    fun testSnapshotJsonFileException() {
        val snapshotsDirectory = "abc"
        val itemGraph = mockk<ItemGraph>()
        mockkStatic(::scanDirectory)

        every { scanDirectory(any(), any(), any(), any()) } throws Exception()

        assertThrows<Exception> {
            EventSnapshotIngester.ingestJsonSnapshot(null, itemGraph, snapshotsDirectory, emptySet())
        }

        unmockkStatic(::scanDirectory)
    }


    @Test
    fun testIngestFromChannel() {

        val eventChannel = Channel<Event>(PARSE_PARALLELISM)
        mockkStatic(::parseEventToEnvelopeBatch)

        val itemGraph = mockk<ItemGraph>()

        every { parseEventToEnvelopeBatch(any()) } returns envelopeBatchGeneric

        runBlocking {

            eventChannel.send(inputEventGeneric)
            eventChannel.close()

            ingestEventFromChannel(
                0,
                eventChannel,
                itemGraph,
                AtomicInteger(0),
                AtomicInteger(0),
                null
            )
        }

        verify(exactly = 1) { itemGraph.ingest(listOf(envelopeBatchGeneric)) }

        // Important
        unmockkStatic(::parseEventToEnvelopeBatch)

    }

    @Test
    fun `test that ingester exceptions are handled`() {
        val eventChannel = Channel<Event>(PARSE_PARALLELISM)

        val itemGraph = mockk<ItemGraph>()
        mockkStatic(::parseEventToEnvelopeBatch)

        every { parseEventToEnvelopeBatch(any()) } returns envelopeBatchGeneric
        every { itemGraph.ingest(any()) } throws Exception()

        runBlocking {

            eventChannel.send(inputEventGeneric)
            eventChannel.send(inputEventGeneric)
            eventChannel.close()

            ingestEventFromChannel(
                0,
                eventChannel,
                itemGraph,
                AtomicInteger(0),
                AtomicInteger(0),
                null
            )
        }

        verify(exactly = 2) { itemGraph.ingest(any()) }

        // Important
        unmockkStatic(::parseEventToEnvelopeBatch)
    }

    @Test
    fun `test that parsing events to envelopeBatch exceptions are handled`() {
        val eventChannel = Channel<Event>(PARSE_PARALLELISM)
        val itemGraph = mockk<ItemGraph>()
        mockkStatic(::parseEventToEnvelopeBatch)

        every { parseEventToEnvelopeBatch(any()) } throws Exception()

        runBlocking {

            eventChannel.send(inputEventGeneric)
            eventChannel.send(inputEventGeneric)
            eventChannel.close()

            ingestEventFromChannel(
                0,
                eventChannel,
                itemGraph,
                AtomicInteger(0),
                AtomicInteger(0),
                null
            )
        }
        verify(exactly = 2) { parseEventToEnvelopeBatch(inputEventGeneric) }

        // Important
        unmockkStatic(::parseEventToEnvelopeBatch)
    }
}