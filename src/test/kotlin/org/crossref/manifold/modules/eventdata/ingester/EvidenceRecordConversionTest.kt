package org.crossref.manifold.modules.eventdata.ingester

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.networknt.schema.JsonSchema
import com.networknt.schema.JsonSchemaFactory
import com.networknt.schema.SpecVersion
import com.networknt.schema.ValidationMessage
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.modules.eventdata.*
import org.crossref.manifold.modules.eventdata.support.EvidenceRecord
import org.crossref.manifold.modules.eventdata.support.ingester.*
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.time.OffsetDateTime

/**
 * To test the Evidence Record to Item Tree mappings we have created a set of curated
 * Envelope Batch instances in JSON format. There are three files per Event Data agent, an evidence record,
 * an agent EnvelopeBatch and a percolator EnvelopeBatch.
 *
 * For the tests we read all evidence record files, convert them into item trees in memory and serialise them to JSON
 * to compare against the curated examples.
 *
 * The agents covered in these tests are:
 * - twitter
 * - wikipedia
 * - reddit
 * - reddit-links
 * - stackexchange
 * - newsfeed
 * - wordpressdotcom
 * - hypothesis
 *
 */
internal class EvidenceRecordConversionTest {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)


    // We will be using a Json Schema validator to validate item types
    private val factory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V7)

    private val itemTypesSchema: JsonSchema = factory.getSchema(
        EventsConversionTest::class.java.getResourceAsStream("/schema/item-types.schema.json")
    )

    // Fetch all evidence records in resources
    private val evidenceRecordFiles = File(object {}.javaClass.getResource("/modules/eventdata/").file)
        .walk().filter { it.name.endsWith("-evidence-record.json") }
        .map { it.name.substringBefore("-evidence-record.json") to it.name }

    // The mapper is configured to ignore empty fields and
    // to convert timestamps to ISO 8601 string format when serialising.
    private val mapper: ObjectMapper =
        ObjectMapper().registerModule(JavaTimeModule()).disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .setDateFormat(StdDateFormat().withColonInTimeZone(true))
            .registerModule(KotlinModule())

    /**
     * Items contain properties that can be validated against the item types' schema.
     * Gather all items properties, validate them and assert no errors.
     */
    private fun EnvelopeBatch.validateItemTypes(itemTypesSchema: JsonSchema) =
        envelopes.forEach { envelope ->
            (envelope.itemTrees + envelope.assertion.assertingParty)
                .forEach { item ->
                    item.properties.forEach { properties ->

                        val errors: Set<ValidationMessage> = itemTypesSchema.validate(properties.values)

                        if (errors.isNotEmpty()) {
                            println(errors)
                        }

                        assert(errors.isEmpty())
                    }
                }
        }

    /**
     * Fetches all curated envelope batches for a certain agent from the resources.
     * These are both agent and percolator envelope batches.
     *
     * @receiver String - event data agent id (twitter, wikipedia, newsfeed etc).
     *
     * @return A pair
     */
    private fun String.getResourceEnvelopeBatches(): Pair<JsonNode, JsonNode> {

        val expectedAgent = mapper.readTree(
            EventsConversionTest::class.java
                .getResourceAsStream(
                    "/modules/eventdata/$this-agent.json"
                )
        )

        val expectedPercolator = mapper.readTree(
            EventsConversionTest::class.java
                .getResourceAsStream(
                    "/modules/eventdata/$this-percolator.json"
                )
        )

        return expectedAgent to expectedPercolator
    }

    private fun String.getEvidenceRecord(): EvidenceRecord = mapper.readValue(
        EventsConversionTest::class.java
            .getResourceAsStream(
                "/modules/eventdata/$this"
            ),
        EvidenceRecord::class.java
    )

    /**
     * Convert an evidence record to agent and percolator envelope batches
     * and return them as a pair of agentEnvelopeBatch to percolatorEnvelopeBatch.
     */
    private fun getEnvelopeBatches(
        filename: String,
        evidenceRecord: EvidenceRecord
    ): Pair<EnvelopeBatch?, EnvelopeBatch?> {
        val envelopeBatches = convertEvidenceRecordToEnvelopeBatch(
            filename,
            evidenceRecord
        )

        val agentBatch = envelopeBatches
            .firstOrNull { it.provenance.userAgent.contains("agent") }

        val percolatorBatch = envelopeBatches
            .firstOrNull { it.provenance.userAgent.contains("percolator") }

        return agentBatch to percolatorBatch
    }

    /**
     * This test is meant to ensure that the evidence record to item tree mapping is producing the expected output.
     * The expected output has been decided and recorded into JSON envelope batch examples for each agent.
     *
     * Convert evidence records to envelope batches, serialise the envelope batches to JSON and compare
     * them to their corresponding curated examples.
     */
    @Test
    fun `Evidence record to item tree mapping matches expected records`() {

        evidenceRecordFiles.forEach { pair ->

            val agent = pair.first
            val filename = pair.second

            logger.info("Testing $agent records")

            val evidenceRecord = filename.getEvidenceRecord()

            val (expectedAgent, expectedPercolator) = agent.getResourceEnvelopeBatches()

            val (agentBatch, percolatorBatch) = getEnvelopeBatches(filename, evidenceRecord)

            // Assert produced item trees match expected
            Assertions.assertEquals(expectedAgent, mapper.valueToTree(agentBatch))
            Assertions.assertEquals(expectedPercolator, mapper.valueToTree(percolatorBatch))

        }

    }

    /**
     * Each item will have a type, described in its properties.
     * Item types are recorded in the item-types schema.
     *
     * Collect and validate each item's properties against the item-types schema.
     *
     * @see [validateItemTypes]
     */
    @Test
    fun `Validate item properties against Item Types schema`() {

        evidenceRecordFiles.forEach { pair ->

            val agent = pair.first
            val filename = pair.second

            logger.info("Testing $agent records")

            val evidenceRecord = filename.getEvidenceRecord()

            val (agentBatch, percolatorBatch) = getEnvelopeBatches(filename, evidenceRecord)

            // Validate properties item types
            agentBatch?.validateItemTypes(itemTypesSchema)
            percolatorBatch?.validateItemTypes(itemTypesSchema)

        }

    }

    /**
     * Not all actions produce records. Actually there are many observations that do not match a DOI.
     * Ensure that we filter out Envelope batches that do not contain any item trees.
     */
    @Test
    fun `Empty envelope batches are discarded`() {
        val filename = "newsfeed-evidence-record-empty.json"

        val evidenceRecord = filename.getEvidenceRecord()

        val (agentBatch, percolatorBatch) = getEnvelopeBatches(filename, evidenceRecord)

        assert(agentBatch == null)
        assert(percolatorBatch == null)

    }

    /**
     * The agent envelope assertions are expected to carry their action's individual occurredAt timestamp.
     * On the other hand the percolator envelope assertions are expected to all match the evidence record's timestamp.
     */
    @Test
    fun `Validate timestamps`() {

        evidenceRecordFiles.forEach { pair ->

            val agent = pair.first
            val filename = pair.second

            logger.info("Testing $agent records")

            val evidenceRecord = filename.getEvidenceRecord()

            val (agentBatch, percolatorBatch) = getEnvelopeBatches(
                filename,
                evidenceRecord
            )


            // Create a map of subjects to timestamps
            val urlToTimestampPairs = evidenceRecord.pages.flatMap { page ->
                page.actions.map { action ->
                    action.url to action.occurredAt
                }
            }.toMap()

            // Agent batch timestamps come from actions
            agentBatch?.envelopes?.forEach { envelope ->
                envelope.itemTrees.forEach { item ->
                    item.identifiers
                        .filter { identifier -> urlToTimestampPairs.containsKey(identifier.uri.toString()) }
                        .forEach { identifier ->
                            val expected = OffsetDateTime.parse(urlToTimestampPairs[identifier.uri.toString()])
                            val actual = envelope.assertion.assertedAt
                            Assertions.assertEquals(expected, actual)
                        }
                }

            }

            // Percolator batch timestamps come from the evidence record
            percolatorBatch?.envelopes?.forEach { envelope ->
                Assertions.assertEquals(evidenceRecord.timestamp, envelope.assertion.assertedAt.toString())
            }

        }

    }

    /**
     * Given an author and tweetId the resulting identifier should be structured as:
     * "twitter://status?author=$author&tweet-id=$tweetId"
     */
    @Test
    fun testTwitterIdentifierFrom() {
        val expected = "twitter://status?author=xyz&tweet-id=123"
        val actual = twitterIdentifierFrom("xyz", "123")

        Assertions.assertEquals(expected, actual)
    }

    /**
     * Ensure that [getTwitterSubjectIdentifierComponents] obtains the appropriate author and tweetId
     * and using [twitterIdentifierFrom] returns the expected structured composite identifier
     * for the subject.
     */
    @Test
    fun testGetTwitterSubjectIdentifier() {
        val expected = Triple("YeisciminE", "1559694486116204545","twitter://status?author=YeisciminE&tweet-id=1559694486116204545")
        val actual = twitterAction.getTwitterSubjectIdentifierComponents()

        Assertions.assertEquals(expected, actual)
    }

    /**
     * Should never happen, but ensure that an exception is thrown when for any reason we cannot
     * construct a subject identifier.
     */
    @Test
    fun testGetTwitterSubjectIdentifierMalformed() {
        assertThrows<TwitterIdRegexException> { twitterActionMalformedId.getTwitterSubjectIdentifierComponents() }
        assertThrows<TwitterAuthorRegexException> { twitterActionMalformedAuthor.getTwitterSubjectIdentifierComponents() }
        assertThrows<NullAuthorException> { twitterActionNullAuthor.getTwitterSubjectIdentifierComponents() }
    }

    /**
     * Ensure that [getTwitterSubjectIdentifierComponents] obtains the appropriate author and tweetId
     * and using [twitterIdentifierFrom] returns the expected structured composite identifier
     * for the object.
     */
    @Test
    fun testGetTwitterObjectIdentifier() {
        val expected = "twitter://status?author=SomeNone&tweet-id=1559694486116204555"
        val actual = twitterAction.getTwitterObjectIdentifier()

        Assertions.assertEquals(expected, actual)
    }

    /**
     * Should never happen, but ensure that an exception is thrown when for any reason we cannot
     * construct an object identifier.
     */
    @Test
    fun testGetTwitterObjectIdentifierMalformed() {
        assertThrows<TwitterIdRegexException> { twitterActionMalformedId.getTwitterObjectIdentifier() }
        assertThrows<TwitterAuthorRegexException> { twitterActionMalformedAuthor.getTwitterObjectIdentifier() }
        assertThrows<NullAuthorException> { twitterActionNullAuthor.getTwitterObjectIdentifier() }
    }

    @Test
    fun testIsRetweet() {
        Assertions.assertTrue(twitterAction.isReTweet())
        Assertions.assertFalse(twitterActionOriginalTweet.isReTweet())
    }

}