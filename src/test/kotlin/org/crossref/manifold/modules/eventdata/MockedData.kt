package org.crossref.manifold.modules.eventdata

import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.modules.eventdata.support.*
import org.crossref.manifold.modules.eventdata.support.ingester.toItem
import java.time.OffsetDateTime

val secrets = setOf("test", "test2")
const val token =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MzgzNTEzMTAsInN1YiI6ImNyb3NzcmVmIn0.g8dNKApyNu_FEUYFi1SaOBaImcqPwmFd-r8qsfeN4A0"
const val invalidToken =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MzgzNTgzOTMsInN1YiI6ImNyb3NzcmVmIn0.kkZ8JLCz13vkaDRsRRX3u0iylePpSsrhQG9BimO_Ttc"

val inputEventGeneric = Event(
    id = "71823282-06c6-4a8f-8df3-e1c1b6b43faa",
    sourceToken = "36c35e23-8757-4a9d-aacf-345e9b7eb50d",
    occurredAt = "2021-09-22T01:26:39.000Z",
    subj = EventSubj(
        pid = "https://doi.org/10.1039/d0qi01407a",
        url = "https://doi.org/10.1039/d0qi01407a",
        null,null,null,null,null,null,null
    ),
    obj = Obj(pid = "https://doi.org/10.1021/ja203695h","", "", ""),
    relationTypeId = "references",
    sourceId = "crossref",
    license = "",
    objId = "",
    subjId = "",
    evidenceRecord = "",
    action = ""
)

val inputEventTwitter = Event(
    id = "71823282-06c6-4a8f-8df3-e1c1b6b43faa",
    sourceToken = "36c35e23-8757-4a9d-aacf-345e9b7eb50d",
    occurredAt = "2021-09-22T01:26:39.000Z",
    subj = EventSubj(
        pid = "twitter://status?id=xxxxxxxxx",
        url = "twitter://status?id=xxxxxxxxx",
        title = "Tweet xxxxxxxxx",
        author = Author("twitter://user?screen_name=xxxxxxxx"),
        originalTweetAuthor = "twitter://user?screen_name=xxxxxxxxxxx",
        alternativeId = "0123456789",
        originalTweetUrl = "twitter://status?id=xxxxxxxxxxx",
        type = null,
        issued = null
    ),
    obj = Obj(pid = "https://doi.org/10.1111/(issn)1467-8748", "", "", ""),
    relationTypeId = "discusses",
    sourceId = "twitter",
    license = "",
    objId = "",
    subjId = "",
    evidenceRecord = "",
    action = ""
)

val envelopeBatchGeneric = EnvelopeBatch(
    listOf(
        Envelope(
            listOf(inputEventGeneric.toItem()!!),
            ItemTreeAssertion(
                OffsetDateTime.parse("2021-09-22T01:26:39.000Z"), MergeStrategy.NAIVE, Item().withIdentifier(
                   Identifier.new(
                        Items.CROSSREF_AUTHORITY
                    )
                )
            )
        )
    ), EnvelopeBatchProvenance(
        "Crossref 36c35e23-8757-4a9d-aacf-345e9b7eb50d",
        "71823282-06c6-4a8f-8df3-e1c1b6b43faa"
    )
)

val match = Match(
    "doi-url",
    "10.5555/12345680.",
    "10.5555/12345680",
    "method",
    "verification")


val action = Action(
    "123",
    "2021-09-22T01:26:39.000Z",
    listOf(match),
    "https://hr.wikipedia.org/w/index.php?title=Kera_Tamara&oldid=6236673",
    ActionSubject(
        title = "A test",
        issued = "2021-09-22T01:26:39.000Z"),
    "discusses"
)

/**
 * This twitter action is modeled as a retweet.
 */
val twitterAction = Action(
    id= "123",
    url = "twitter://status?id=1559694486116204545",
    occurredAt = "2021-09-22T01:26:39.000Z",
    matches = listOf(match),
    subj = ActionSubject(
        author = Author("twitter://user?screen_name=YeisciminE"),
        issued = "2021-09-22T01:26:39.000Z",
        originalTweetAuthor = "twitter://user?screen_name=SomeNone",
        originalTweetUrl = "twitter://status?id=1559694486116204555"
    ),
        relationTypeId = "discusses"
)

/**
 * This twitter action is modeled as an original tweet.
 */
val twitterActionOriginalTweet = Action(
    id= "123",
    url = "twitter://status?id=1559694486116204545",
    occurredAt = "2021-09-22T01:26:39.000Z",
    matches = listOf(match),
    subj = ActionSubject(
        author = Author("twitter://user?screen_name=YeisciminE"),
        issued = "2021-09-22T01:26:39.000Z"
    ),
    relationTypeId = "discusses"
)

val twitterActionMalformedId = Action(
    id= "123",
    url = "twitter://status?1559694486116204545",
    occurredAt = "2021-09-22T01:26:39.000Z",
    matches = listOf(match),
    subj = ActionSubject(
        author = Author("twitter://user?screen_name=YeisciminE"),
        issued = "2021-09-22T01:26:39.000Z",
        originalTweetAuthor = "twitter://user?screen_name=SomeNone",
        originalTweetUrl = "twitter://status?1559694486116204555"),
    relationTypeId = "discusses"
)

val twitterActionMalformedAuthor = Action(
    id= "123",
    url = "twitter://status?1559694486116204545",
    occurredAt = "2021-09-22T01:26:39.000Z",
    matches = listOf(match),
    subj = ActionSubject(
        author = Author("twitter://user?YeisciminE"),
        issued = "2021-09-22T01:26:39.000Z",
        originalTweetAuthor = "twitter://user?SomeNone",
        originalTweetUrl = "twitter://status?id=1559694486116204555"),
    relationTypeId = "discusses"
)

val twitterActionNullAuthor = Action(
    id= "123",
    url = "twitter://status?1559694486116204545",
    occurredAt = "2021-09-22T01:26:39.000Z",
    matches = listOf(match),
    subj = ActionSubject(
        issued = "2021-09-22T01:26:39.000Z",
        originalTweetUrl = "twitter://status?id=1559694486116204555"),
    relationTypeId = "discusses"
)

val hypothesisAnnotatesAction = Action(
    "123",
    "2021-09-22T01:26:39.000Z",
    listOf(match),
    "url here",
    ActionSubject(
        issued = "2021-09-22T01:26:39.000Z"),
    "annotates"
)

val hypothesisDiscussesAction = Action(
    "123",
    "2021-09-22T01:26:39.000Z",
    listOf(match),
    "url here",
    ActionSubject(
        issued = "2021-09-22T01:26:39.000Z"),
    "discusses"
)