package org.crossref.manifold.modules.eventdata.ingester

import org.crossref.manifold.modules.eventdata.*
import org.crossref.manifold.modules.eventdata.support.ingester.*
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class EvidenceRecordConversionPropertiesTest {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Test
    fun testGetSubjectProperties() {

        setOf(
            NEWSFEED, REDDIT, REDDIT_LINKS, WIKIPEDIA,
            STACK_EXCHANGE, WORDPRESS_DOT_COM
        ).forEach { agent ->

            logger.info("Testing subject properties for $agent agent...")

            val expectedMap = mapOf(
                "type" to "web-resource",
                "subtype" to "web-content",
                "issued" to "2021-09-22T01:26:39.000Z",
                "title" to "A test"
            )

            val actualMap = action.getSubjectProperties(agent)

            Assertions.assertEquals(expectedMap, actualMap)

        }
    }

    /**
     * For twitter properties we ensure to skip the title.
     */
    @Test
    fun testGetTwitterSubjectProperties() {
        val expectedMap = mapOf(
            "type" to "web-resource",
            "subtype" to "web-content",
            "issued" to "2021-09-22T01:26:39.000Z"
        )

        val actualMap = twitterAction.getSubjectProperties(TWITTER)

        Assertions.assertEquals(expectedMap, actualMap)
    }

    /**
     * The subtype for a hypothesis action that "annotates" a work
     * should be annotation. Also, no title.
     */
    @Test
    fun testGetHypothesisAnnotationSubjectProperties() {
        val expectedMap = mapOf(
            "type" to "web-resource",
            "subtype" to "annotation",
            "issued" to "2021-09-22T01:26:39.000Z"
        )

        val actualMap = hypothesisAnnotatesAction.getSubjectProperties(HYPOTHESIS)

        Assertions.assertEquals(expectedMap, actualMap)
    }


    /**
     * The subtype for a hypothesis action that "discusses" about something
     * should be web-content. Also, no title.
     */
    @Test
    fun testGetHypothesisDiscussionSubjectProperties() {
        val expectedMap = mapOf(
            "type" to "web-resource",
            "subtype" to "web-content",
            "issued" to "2021-09-22T01:26:39.000Z"
        )

        val actualMap = hypothesisDiscussesAction.getSubjectProperties(HYPOTHESIS)

        Assertions.assertEquals(expectedMap, actualMap)
    }

    /**
     * All agents follow the same observation properties structure:
     * - type: reference
     * - subtype: work
     * - text: observation_in_plain_text
     */
    @Test
    fun testGetObservationProperties() {
        setOf(
            NEWSFEED, REDDIT, REDDIT_LINKS, TWITTER, HYPOTHESIS,
            STACK_EXCHANGE, WORDPRESS_DOT_COM, WIKIPEDIA
        ).forEach { agent ->

            logger.info("Testing observation properties for $agent agent...")

            val expectedMap = mapOf(
                "type" to "reference",
                "subtype" to "work",
                "text" to "10.5555/12345680.",
            )

            val actualMap = match.getObservationProperties()

            Assertions.assertEquals(expectedMap, actualMap)
        }
    }

}