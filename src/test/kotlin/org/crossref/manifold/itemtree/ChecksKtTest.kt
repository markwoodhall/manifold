package org.crossref.manifold.itemtree

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class ChecksKtTest {

    @Test
    fun `allItems should call at all levels of the tree`() {

        // Tree with two branches in the middle to test that we recurse down and across.
        val bottomItem = Item().withIdentifier(Identifier.new("bottom"))
        val middleItem1 = Item().withIdentifier(Identifier.new("middle1"))
        val middleItem2 =
            Item().withIdentifier(Identifier.new("middle2")).withRelationships(listOf(Relationship("cites", bottomItem)))
        val topItem = Item().withIdentifier(Identifier.new("top")).withRelationships(
            listOf(
                Relationship("mentions", middleItem1),
                Relationship("mentions", middleItem2)
            )
        )

        // A selection of functions to try which trigger at different levels of the tree.
        assertTrue(allItems(topItem) { _ -> true }, "allItems returns true if all checks return true")

        assertFalse(allItems(topItem) { _ -> false }, "allItems returns false if all checks return false")

        assertFalse(allItems(topItem) { it != middleItem1 }, "allItems returns false if middle level returns false")

        assertFalse(allItems(topItem) { it != bottomItem }, "allItems returns false if bottom level returns false")
    }

    @Test
    fun `allItemsResolved fails if any Items are unresolved`() {
        val all = Item().withPk(1).withIdentifier(
           Identifier.new("http://example.com/abc")
        ).withRelationships(
            listOf(
                Relationship(
                    "mentions", Item().withPk(2).withIdentifier(
                       Identifier.new("http://example.com/def")
                    )
                ),
                Relationship(
                    "mentions", Item().withPk(3).withIdentifier(
                       Identifier.new("http://example.com/hij")
                    )
                        .withRelationships(
                            listOf(
                                Relationship(
                                    "mentions",
                                    // No id here!
                                    Item().withIdentifier(
                                       Identifier.new("http://example.com/klm")
                                    )
                                )
                            )
                        )
                )
            )
        )

        assertFalse(allItemsResolved(all), "allIdentifiersResolved returns false if any Item has no PK.")
    }

    @Test
    fun `allItemsResolved succeeds if all Items are resolved`() {
        val all = Item().withPk(1).withIdentifier(
           Identifier.new("http://example.com/abc")
        ).withRelationships(
            listOf(
                Relationship(
                    "mentions", Item().withPk(2).withIdentifier(
                       Identifier.new("http://example.com/def")
                    )
                ),
                Relationship(
                    "mentions", Item().withPk(3).withIdentifier(
                       Identifier.new("http://example.com/hij")
                    )
                        .withRelationships(
                            listOf(
                                Relationship(
                                    "mentions",
                                    Item().withPk(5)
                                        .withIdentifier(
                                           Identifier.new("http://example.com/klm")
                                        )
                                )
                            )
                        )
                )
            )
        )

        assertTrue(allItemsResolved(all), "allIdentifiersResolved returns true if all Items have no PK.")
    }

    @Test
    fun `allItemIdentifiersResolved fails if any ItemIdentifiers are unresolved`() {
        val all = Item().withPk(1).withIdentifier(
           Identifier.new("http://example.com/abc")
        ).withRelationships(
            listOf(
                Relationship(
                    "mentions", Item().withPk(2).withIdentifier(
                       Identifier.new("http://example.com/def")
                    )
                ),
                Relationship(
                    "mentions", Item().withPk(3).withIdentifier(
                       Identifier.new("http://example.com/hij")
                    )
                        .withRelationships(
                            listOf(
                                Relationship(
                                    "mentions",
                                    // No PK on the identifier! Though there is one on the item, that's irrelevant.
                                    Item().withPk(4).withIdentifier(
                                       Identifier.new("http://example.com/klm")
                                    )
                                )
                            )
                        )
                )
            )
        )

        assertFalse(
            allItemIdentifiersResolved(all),
            "allIdentifiersResolved returns false if any Item has no PK."
        )
    }

    @Test
    fun `allItemIdentifiersResolved succeeds if all ItemIdentifiers are resolved`() {
        val all = Item().withPk(1).withIdentifier(
           Identifier.new("http://example.com/abc").withPk(1001)
        ).withRelationships(
            listOf(
                Relationship(
                    "mentions", Item().withPk(2).withIdentifier(
                       Identifier.new("http://example.com/def").withPk(1002)
                    )
                ),
                Relationship(
                    "mentions",
                    Item().withPk(3).withIdentifier(
                       Identifier.new("http://example.com/hij").withPk(1003)
                    )
                        .withRelationships(
                            listOf(
                                Relationship(
                                    "mentions",
                                    Item().withPk(5)
                                        .withIdentifier(
                                           Identifier.new("http://example.com/klm").withPk(1005)
                                        )
                                )
                            )
                        )
                )
            )
        )

        assertTrue(
            allItemIdentifiersResolved(all),
            "allIdentifiersResolved returns true if all Items have no PK."
        )
    }
}