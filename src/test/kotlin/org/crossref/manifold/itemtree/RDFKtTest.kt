package org.crossref.manifold.retrieval.view

import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

/**
 * Recursively make assertions about all nodes in the tree.
 */
fun assertAllItems(item: Item, f: (item: Item) -> Unit) {
    f(item)
    for (rel in item.rels) {
        assertAllItems(rel.obj, f)
    }
}

internal class RDFKtTest {
    private val allBlanks = Item().withRelationships(
        listOf(
            Relationship(
                "citation",
                Item().withRelationships(
                    listOf(
                        Relationship(
                            "UNKNOWN_ABC",
                            Item().withRelationships(listOf(Relationship("citation", Item())))
                        )
                    )
                )
            ), Relationship(
                "citation",
                Item()
            ), Relationship(
                "funder",
                Item()
            ), Relationship(
                // This relationship should be returned as it's unresolved.
                "UNKNOWN_XYZ",
                Item().withRelationships(
                    listOf(
                        Relationship(
                            "citation", Item()
                        )
                    )
                )
            )
        )
    )

    private val noBlanks =
        Item().withIdentifiers(listOf(Identifier.new("http://dx.doi.org/10.5555/12345678"))).withRelationships(
            listOf(
                Relationship(
                    "citation",
                    Item().withIdentifiers(listOf(Identifier.new("http://dx.doi.org/10.6666/8764321"))).withRelationships(
                        listOf(
                            Relationship(
                                "UNKNOWN_ABC",
                                Item().withIdentifiers(listOf(Identifier.new("http://dx.doi.org/10.0000/0000")))
                            )
                        )
                    )

                ), Relationship(
                    "citation",
                    Item().withIdentifiers(listOf(Identifier.new("http://dx.doi.org/10.7777/24242424")))
                ), Relationship(
                    "funder",
                    Item().withIdentifiers(listOf(Identifier.new("http://dx.doi.org/10.8888/98989898")))
                ), Relationship(
                    // This relationship should be returned as it's unresolved.
                    "UNKNOWN_XYZ",
                    Item().withIdentifiers(listOf(Identifier.new("http://dx.doi.org/10.9999/9999")))

                )
            )
        )

    @Test
    fun `markBlankNodes should make no changes with fully identified tree`() {
        val result = markBlankNodes(noBlanks, "http://example.com/")
        assertEquals(result, noBlanks, "With all identifiers supplied, tree should be unchanged")
    }

    @Test
    fun `markBlankNodes should add Identifiers to every Item`() {
        assertAllItems(allBlanks) {
            assertTrue(it.identifiers.isEmpty(), "Pre: All nodes have no Identifiers")
        }

        val prefix = "http://example.com/"
        val result = markBlankNodes(allBlanks, prefix)

        val seen = mutableSetOf<String>()
        assertAllItems(result) {
            assertTrue(it.identifiers.size == 1, "All nodes have exactly one ItemIdentifier")
            val thisBlankNode = it.identifiers.first().uri.toString()

            assertTrue(thisBlankNode.startsWith(prefix), "All blank nodes start with the assigned prefix")
            assertFalse(seen.contains(thisBlankNode), "Shouldn't see the same blank node twice")
            seen.add(thisBlankNode)
            assertTrue(seen.contains(thisBlankNode), "Pre for next time: we added it for comparison")
        }
    }

    @Test
    fun `markBlankNodes should be stable for same input`() {
        assertAllItems(allBlanks) {
            assertTrue(it.identifiers.isEmpty(), "Pre: All nodes have no Identifiers")
        }

        val prefix = "http://example.com/"
        val result1 = markBlankNodes(allBlanks, prefix)
        val result2 = markBlankNodes(allBlanks, prefix)

        assertEquals(result1, result2, "markBlankNodes should give the same result when called twice with same input")
    }

    @Test
    fun `markBlankNodes should ensure that identical siblings have different Blank Node identifiers as long as they have different relationships`() {
        // Identical sibling Blank Nodes, but they are related via different rel types.
        val identicalSiblings = Item().withIdentifier(Identifier.new("https://doi.org/10.5555/12345678")).withRelationships(
            listOf(
                Relationship(
                    "citation",
                    Item().withPropertiesFromMap(mapOf("colour" to "red"))
                ),
                Relationship(
                    "component",
                    Item().withPropertiesFromMap(mapOf("colour" to "red"))
                )
            )
        )

        val result = markBlankNodes(identicalSiblings, "https://example.com/")

        val id1 = result.rels[0].obj.identifiers
        val id2 = result.rels[1].obj.identifiers

        assertNotEquals(
            id1,
            id2,
            "IDs for siblings that are identical, but are related to parent via different relationships, should be different."
        )
    }

    @Test
    fun `identical siblings, with an identical relationship to the parent, are allowed to have the same Blank Node Identifier`() {
        // Identical sibling Blank Nodes, also related via identical rel types.
        val identicalSiblings = Item().withIdentifier(Identifier.new("https://doi.org/10.5555/12345678")).withRelationships(
            listOf(
                Relationship(
                    "citation",
                    Item().withPropertiesFromMap(mapOf("colour" to "red"))
                ),
                Relationship(
                    "citation",
                    Item().withPropertiesFromMap(mapOf("colour" to "red"))
                )
            )
        )

        val result = markBlankNodes(identicalSiblings, "https://example.com/")

        val id1 = result.rels[0].obj.identifiers
        val id2 = result.rels[1].obj.identifiers

        assertEquals(
            id1,
            id2,
            "IDs for siblings that are identical, but are related to parent via different relationships, should be different."
        )
    }
}