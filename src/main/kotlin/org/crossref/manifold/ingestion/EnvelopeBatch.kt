package org.crossref.manifold.ingestion

import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item


/**
 * A batch of [Envelope]s which corresponds to one chunk of information arriving from outside, such as a metadata
 * deposit, social media event, reference match etc. These are batched together for traceability, because one piece of
 * information in the outside world may map to more than one [Envelope], i.e. represent assertions with multiple
 * parties. By keeping these together we can track them all the work that we did in response.
 */
data class EnvelopeBatch(
    /**
     * List of [Envelope]s, each of which asserts an Item Tree.
     */
    val envelopes: List<Envelope>,

    /** Which service inserted this Envelope.
     */
    val provenance: EnvelopeBatchProvenance,

    /** Primary Key in the database
     */
    val pk: Long? = null
) {
    /**
     * Add and replace Envelopes.
     */
    fun withEnvelopes(newEnvelopes: List<Envelope>) = EnvelopeBatch(newEnvelopes, provenance)

    /** Add and replace the PK.
     */
    fun withPk(newPk: Long) = EnvelopeBatch(envelopes, provenance, newPk)
    fun countItems() : Int =
        this.envelopes.sumOf { it.countItems() }

}

/**
 *  Is there any use of ambiguous identifiers in this EnvelopeBatch?
 */
fun hasAmbiguousIdentifiers(batch: EnvelopeBatch) = batch.envelopes.any { env ->
    env.itemTrees.any { item ->
        org.crossref.manifold.itemtree.hasAmbiguousIdentifiers(item)
    }
}

/** From an EnvelopeBatch retrieve all Identifiers that are used in an unambiguous way.
 * From the Content and the Envelope itself.
 */
fun getUnambiguousUnresolvedIdentifiers(batch: EnvelopeBatch): Set<Item> =
    batch.envelopes.flatMap { envelope ->
        envelope.itemTrees.flatMap { org.crossref.manifold.itemtree.getUnambiguousUnresolvedItems(it) } +
                org.crossref.manifold.itemtree.getUnambiguousUnresolvedItems(
                    envelope.assertion.assertingParty
                )
    }.toSet()

/**
 * Remove ambiguously identified Items form the [EnvelopeBatch] and return.
 */
fun removeAmbiguousItems(batch: EnvelopeBatch): EnvelopeBatch =
    batch.withEnvelopes(batch.envelopes.map(::removeAmbiguousItems))
