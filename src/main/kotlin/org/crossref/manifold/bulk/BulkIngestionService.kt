package org.crossref.manifold.bulk

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.scanTarGz
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.bulk.Module
import org.crossref.manifold.util.Constants.BATCH_SIZE
import org.crossref.manifold.util.logDuration
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.File
import java.util.*

/**
 * Bulk ingester can read EnvelopeBatch Batch files. This is optimised for offline ingestion of hundreds of millions of
 * Envelope Batches, so it needs a lock on the database.
 */
@Service
class BulkIngestionService(
    private val itemGraph: ItemGraph,
    @Value("\${${Module.BULK_INGESTER}.$BATCH_SIZE:10000}") private val batchSize: Int,
) {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    fun ingest(archiveFiles: List<File>) {
        logDuration(logger, "ingest assertions from archive batch files") {
            val ingestionIds = ingestFiles(archiveFiles)
            while (ingestionIds.any { itemGraph.isIngestionRunning(it) }) {
                runBlocking {
                    delay(1000L)
                }
            }
        }
    }

    /**
     * Ingest all the Envelope Batches as quickly as possible.
     * This relies on all Item Identifiers having been pre-warmed, which will remove transaction interdependencies.
     */
    fun ingestFiles(archiveFiles: List<File>): List<UUID> {
        val ingestionIds = mutableListOf<UUID>()
        try {
            runBlocking {
                // The channel only needs space to read one spare entry whilst the other one is processed.
                val envelopeBatchChan = Channel<Collection<EnvelopeBatch>>(2)

                val readTask = launch(CoroutineName("read-gz") + Dispatchers.IO) {
                    archiveFiles.forEach { archiveFile ->
                        readEnvelopeBatchArchive(archiveFile) {
                            // We can't ingest any ambiguously identified Items through EnvelopeBatches.
                            // More importantly, the optimization relies on this, so make errors explicit.
                            envelopeBatchChan.send(it)
                        }
                    }
                }

                val ingestTask = launch(CoroutineName("ingest-xml") + Dispatchers.IO) {
                    ingestionIds.addAll(ingestEnvelopeBatchesFromChannel(envelopeBatchChan))
                }

                logger.info("Wait for read to finish...")
                readTask.join()
                envelopeBatchChan.close()

                logger.info("Read finished. Waiting for insert to finish...")
                ingestTask.join()
            }
        } catch (e: Exception) {
            logger.warn("Error! $e")
            e.printStackTrace()
        }
        return ingestionIds
    }

    /**
     * Given a channel of [EnvelopeBatch]es ingest into the Item Graph in large batches.
     */
    private suspend fun ingestEnvelopeBatchesFromChannel(
        envelopeBatchChan: Channel<Collection<EnvelopeBatch>>,
    ): List<UUID> {
        logger.info("Start ingesting batches from channel $envelopeBatchChan...")

        val ingestionIds = mutableListOf<UUID>()

        var batches = mutableListOf<EnvelopeBatch>()

        try {
            for (envelopeBatch in envelopeBatchChan) {
                batches.addAll(envelopeBatch)
                if (batches.count() > batchSize) {
                    val countItems = batches.sumOf { it.countItems() }
                    logDuration(this.logger, "ingest collection of ${batches.count()} batches containing $countItems") {


                        ingestionIds.add(
                            itemGraph.ingest(batches)
                        )
                    }
                    batches = mutableListOf()
                }
            }
            logger.info("Ingest last batch of ${batches.count()}...")
            ingestionIds.add(itemGraph.ingest(batches))
        } catch (e: Exception) {
            logger.warn("Error! $e")
            e.printStackTrace()
        }

        logger.info("Finish ingesting batches from channel $envelopeBatchChan!")
        return ingestionIds
    }

    /**
     * Reader for an EnvelopeBatch Archive, as created with [org.crossref.manifold.ingestion.EnvelopeBatchArchiveWriter].
     */
    private suspend fun readEnvelopeBatchArchive(file: File, f: suspend (batches: List<EnvelopeBatch>) -> Unit) {
        /** Mapper that can read JSON files from the Tar file.
         * AUTO_CLOSE_SOURCE is important for tar streams as they are files of files, therefore have specific file-handling
         * semantics. Normal parsing would close the stream prematurely.
         */
        val mapper = JsonMapper.builder()
            .addModule(JavaTimeModule())
            .configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, false)
            .build().registerKotlinModule()

        scanTarGz(file) { _, reader ->
            val myObjects: List<EnvelopeBatch?>? =
                mapper.readValue(reader, object : TypeReference<List<EnvelopeBatch?>?>() {})

            val batches = myObjects?.mapNotNull { it }
            if (batches != null) {
                f(batches)
            }
        }
    }
}
