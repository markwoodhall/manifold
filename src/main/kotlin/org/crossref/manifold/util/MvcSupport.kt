package org.crossref.manifold.util

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import org.springframework.core.MethodParameter
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component
import org.springframework.web.context.request.NativeWebRequest
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.HandlerMapping
import org.springframework.web.servlet.mvc.method.annotation.ServletModelAttributeMethodProcessor
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.time.temporal.TemporalQueries
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * When a URL path is supplied with a non-URL DOI, redirect back to the same path but with the full DOI URL.
 */
class NonUrlDoiInterceptor : HandlerInterceptor {
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE)?.let {
            val maybeDoi = ((it as Map<*, *>).getOrDefault("identifier", "~") as String).substring(1)
            return if (maybeDoi.startsWith("10.")) {
                response.status = HttpServletResponse.SC_MOVED_PERMANENTLY
                response.setHeader("Location", "${request.requestURI.removeSuffix(maybeDoi)}https://doi.org/$maybeDoi")
                false
            } else {
                true
            }
        }
        return true
    }
}

/**
 * Our REST API uses kebab case request parameters but Spring MVC does not support these out of the box when
 * instantiating [org.springframework.web.bind.annotation.ModelAttribute]s.
 *
 * In conjunction with [ApiParameter] this argument resolver adds that support.
 */
class ApiParameterArgumentResolver : ServletModelAttributeMethodProcessor(false) {
    override fun supportsParameter(parameter: MethodParameter): Boolean =
        parameter.hasParameterAnnotation(ApiParameter::class.java)

    override fun resolveConstructorArgument(paramName: String, paramType: Class<*>, request: NativeWebRequest): Any? =
        super.resolveConstructorArgument(paramName, paramType, request)
            ?: request.getParameter(PropertyNamingStrategies.KebabCaseStrategy().translate(paramName))
}

/**
 * To be used in place of [org.springframework.web.bind.annotation.ModelAttribute] when support for kebab case request
 * parameters is required.
 */
@Target(AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class ApiParameter

@Component
class StringToOffsetDateTimeConverter : Converter<String, OffsetDateTime> {
    override fun convert(source: String): OffsetDateTime = parseToOffsetDateTime(
        source,
        DateTimeFormatter.ISO_LOCAL_DATE
    ) ?: parseToOffsetDateTime(
        source,
        DateTimeFormatter.ISO_LOCAL_DATE_TIME
    ) ?: OffsetDateTime.parse(source)

    private fun parseToOffsetDateTime(source: String, formatter: DateTimeFormatter): OffsetDateTime? =
        try {
            formatter.parse(source).let { temporal ->
                temporal.query(TemporalQueries.localDate()).let { date ->
                    (temporal.query(TemporalQueries.localTime()) ?: LocalTime.MIDNIGHT).let { time ->
                        OffsetDateTime.of(date, time, ZoneOffset.UTC)
                    }
                }
            }
        } catch (e: DateTimeParseException) {
            null
        }
}
