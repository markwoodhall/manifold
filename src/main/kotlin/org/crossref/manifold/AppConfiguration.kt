package org.crossref.manifold

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import io.swagger.v3.core.jackson.ModelResolver
import org.crossref.manifold.util.ApiParameterArgumentResolver
import org.crossref.manifold.util.NonUrlDoiInterceptor
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory
import org.springframework.boot.web.servlet.server.ServletWebServerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.retry.annotation.EnableRetry
import org.springframework.scheduling.annotation.AsyncConfigurer
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.util.pattern.PathPatternParser
import java.time.InstantSource
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Spring Boot configuration.
 */
@Configuration
@EnableRetry
@EnableAsync
@EnableScheduling
class AppConfiguration : WebMvcConfigurer, AsyncConfigurer {
    /**
     * Servlet web factory, required to smooth interop with transitive dependencies of Cayenne.
     */
    @Bean
    fun servletWebServerFactory(): ServletWebServerFactory? {
        return TomcatServletWebServerFactory()
    }

    @Bean
    fun modelResolver(objectMapper: ObjectMapper): ModelResolver = ModelResolver(objectMapper)

    @Bean
    fun instantSource(): InstantSource = InstantSource.system()

    override fun configureContentNegotiation(configurer: ContentNegotiationConfigurer) {
        configurer.favorParameter(true)
            .parameterName("format")
            .ignoreAcceptHeader(false)
            .defaultContentType(MediaType.APPLICATION_JSON)
            .mediaType("application/json", MediaType.APPLICATION_JSON)
            .mediaType("application/vnd.scholix+json", MediaType("application", "vnd.scholix+json"))
            .mediaType("application/rdf+xml", MediaType("application", "rdf+xml"))
            .mediaType("text/turtle", MediaType("text", "turtle"))
            .mediaType("application/ld+json", MediaType("application", "ld+json"))
            .mediaType("json", MediaType.APPLICATION_JSON)
            .mediaType("scholix", MediaType("application", "vnd.scholix+json"))
            .mediaType("rdf-xml", MediaType("application", "rdf+xml"))
            .mediaType("rdf-turtle", MediaType("text", "turtle"))
            .mediaType("json-ld", MediaType("application", "ld+json"))
    }

    override fun configurePathMatch(configurer: PathMatchConfigurer) {
        configurer.setPatternParser(PathPatternParser())
    }

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(NonUrlDoiInterceptor())
    }

    override fun addArgumentResolvers(resolvers: MutableList<HandlerMethodArgumentResolver>) {
        resolvers.add(ApiParameterArgumentResolver())
    }

    override fun extendMessageConverters(converters: MutableList<HttpMessageConverter<*>>) {
        for (converter in converters) {
            if (converter is MappingJackson2HttpMessageConverter) {
                converter.objectMapper.propertyNamingStrategy = PropertyNamingStrategies.KEBAB_CASE
                converter.objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
                break
            }
        }
    }

    override fun getAsyncExecutor(): Executor = Executors.newCachedThreadPool()
}
