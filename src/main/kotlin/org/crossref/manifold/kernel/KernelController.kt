package org.crossref.manifold.kernel

import org.crossref.manifold.api.PropertyStatementsWithProvenanceView
import org.crossref.manifold.api.RelationshipStatementsWithProvenanceView
import org.crossref.manifold.api.RelationshipTypesView
import org.crossref.manifold.api.ResponseEnvelope
import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.itemgraph.RelationshipTypeDao
import org.crossref.manifold.itemgraph.stats.ManifoldStats
import org.crossref.manifold.itemgraph.stats.RelationshipStatsDao
import org.crossref.manifold.retrieval.IdentifierRetriever
import org.crossref.manifold.retrieval.statements.PropertyStatementFilterBuilder
import org.crossref.manifold.retrieval.statements.PropertyStatementRetriever
import org.crossref.manifold.retrieval.statements.RelationshipStatementFilterBuilder
import org.crossref.manifold.retrieval.statements.RelationshipStatementRetriever
import org.crossref.manifold.views.PropertyStatementWithProvenance
import org.crossref.manifold.views.RelationshipStatementWithProvenance
import org.springframework.web.bind.annotation.*

/**
 * REST APIs for Manifold kernel-level data.
 */
@RestController
class KernelController(
    private val relationshipStatsDao: RelationshipStatsDao,
    private val relationshipStatementFilterBuilder: RelationshipStatementFilterBuilder,
    private val propertyStatementFilterBuilder: PropertyStatementFilterBuilder,
    private val relationshipStatementRetriever: RelationshipStatementRetriever,
    private val propertyStatementRetriever: PropertyStatementRetriever,
    private val relationshipTypeDao: RelationshipTypeDao,
    private val itemDao: ItemDao,
) {
    /**
     * Statistics for the whole Item Graph.
     */
    @GetMapping("/v2/kernel/stats")
    fun getStats(): ResponseEnvelope<ManifoldStats> {
        val stats = relationshipStatsDao.getStats()

        return ResponseEnvelope("ok", "manifold-stats", message = stats)
    }

    /**
     * Get the list of known Relationship types.
     */
    @GetMapping("/v2/kernel/relationship-types", produces = ["application/json"])
    fun getRelationshipTypes(): ResponseEnvelope<RelationshipTypesView> {
        val types = relationshipTypeDao.getRelationshipTypes()

        return ResponseEnvelope("ok", "relationship-types-list", message = types)
    }

    @GetMapping("/v2/relationship-statements-with-provenance", produces = ["application/json"])
    fun relationshipStatements(
        @RequestParam(name = "cursor", required = false, defaultValue = "0") cursor: Long,
        @RequestParam(name = "rows", required = false, defaultValue = "20") rows: Int,
        @RequestParam(name = "subject", required = false) subject: String?,
        @RequestParam(name = "object", required = false) obj: String?,
        @RequestParam(name = "relationshipType", required = false) relationshipType: String?,
        @RequestParam(name = "assertedBy", required = false) assertedBy: String?,
    ): ResponseEnvelope<RelationshipStatementsWithProvenanceView> {
        val filter = relationshipStatementFilterBuilder.build(subject, obj, relationshipType, assertedBy)
        val (statements, nextCursor) = relationshipStatementRetriever.getPage(cursor, rows, filter)

        // This IdentifierRetriever has a lifetime only of this API request.
        val identifierRetriever = IdentifierRetriever(itemDao)

        // Load up a cache of all known Identifiers for entities mentioned in this page.
        identifierRetriever.prime(statements.flatMap {
            listOf(
                it.first.fromItemPk,
                it.first.toItemPk,
                it.first.assertingPartyPk
            )
        }.toSet())

        // Now map into the right view, incorporating the statement and its provenance.
        val statementViews = statements.map {
            RelationshipStatementWithProvenance.from(
                it.first,
                it.second,
                identifierRetriever,
            )
        }
        val message = RelationshipStatementsWithProvenanceView(statementViews, nextCursor)

        return ResponseEnvelope("ok", "relationship-statements-with-provenance-list", message = message)
    }

    @GetMapping("/v2/property-statements-with-provenance", produces = ["application/json"])
    fun propertyStatements(
        @RequestParam(name = "cursor", required = false, defaultValue = "0") cursor: Long,
        @RequestParam(name = "rows", required = false, defaultValue = "20") rows: Int,
        @RequestParam(name = "subject", required = false) subject: String?,
        @RequestParam(name = "assertedBy", required = false) assertedBy: String?,
    ): ResponseEnvelope<PropertyStatementsWithProvenanceView> {
        val filter = propertyStatementFilterBuilder.build(subject, assertedBy)
        val (statements, nextCursor) = propertyStatementRetriever.getPage(cursor, rows, filter)

        // This IdentifierRetriever has a lifetime only of this API request.
        val identifierRetriever = IdentifierRetriever(itemDao)

        // Load up a cache of all known Identifiers for entities mentioned in this page.
        identifierRetriever.prime(statements.flatMap { listOf(it.first.itemPk, it.first.assertingPartyPk) }
            .toSet())

        // Now map into the right view, incorporating the statement and its provenance.
        val statementViews = statements.map {
            PropertyStatementWithProvenance.from(
                it.first,
                it.second,
                identifierRetriever
            )
        }
        val message = PropertyStatementsWithProvenanceView(statementViews, nextCursor)

        return ResponseEnvelope("ok", "property-statements-with-provenance-list", message = message)
    }
}
