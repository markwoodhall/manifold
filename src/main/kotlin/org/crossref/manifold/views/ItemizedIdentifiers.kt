package org.crossref.manifold.views

import org.crossref.manifold.retrieval.IdentifierRetriever


/**
 * A set of Identifiers broken down by prefix (i.e. identifier type).
 * The resulting structure allows e.g. "x.doi", "x.orcid" etc.
 */
typealias ItemizedIdentifiers = Map<String, List<String>>

/** With a pre-primed [IdentifierRetriever], construct an ItemizedIdentifiers view object for the given Item PK.
 */
fun buildItemizedIdentifiers(
    itemPk: Long,
    identifierRetriever: IdentifierRetriever,
): ItemizedIdentifiers {
    val identifiers = identifierRetriever.getOrEmpty(itemPk)

    // Take the Identifiers and group them by their prefix
    return identifiers.groupBy(
        keySelector = {
           it.getHostDisplay()
        },
        valueTransform = { it.uri.toString() })
}
