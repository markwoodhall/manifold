package org.crossref.manifold.itemgraph

import org.crossref.manifold.db.BufferBatchDao
import org.crossref.manifold.db.BufferDao
import org.crossref.manifold.db.BufferType
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.IngestionDao
import org.crossref.manifold.ingestion.getUnambiguousUnresolvedIdentifiers
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.markBlanks
import org.crossref.manifold.util.logDuration
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.*

@Service
class ItemGraph(
    private val resolver: Resolver,
    private val itemTreeStatementMapper: ItemTreeStatementMapper,
    private val propertyDao: PropertyDao,
    private val relationshipTypeDao: RelationshipTypeDao,
    private val relationshipDao: RelationshipDao,
    private val ingestionDao: IngestionDao,
    private val bufferBatchDao: BufferBatchDao,
    private val bufferDao: BufferDao,
) {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    fun ingest(batches: Collection<EnvelopeBatch>): UUID {

            val batchId = bufferBatchDao.openBufferBatch()

            // Mark blank nodes according to their identifiers.
            // Asserting parties can also set these before assertion if they want.
            val withBlanks = batches.map { batch ->
                batch.withEnvelopes(batch.envelopes.map { envelope ->
                    envelope.withItemTrees(
                        envelope.itemTrees.map {
                            markBlanks(it)
                        })
                })
            }


            // Read-only resolve all the identifiers found in the collection of Envelope Batches.
            // Return those batches, partially resolved.
            val readOnlyResolved = logDuration(this.logger, "resolve RO") {
                resolver.resolveRO(withBlanks)
            }

            // Resolve read-write those identifiers that aren't yet resolved, and wait for them to flush.
            logDuration(this.logger, "buffer to resolve RW") {
                resolver.bufferToResolveRW(readOnlyResolved, batchId)
            }

            logDuration(this.logger, "wait for identifier buffer") {
                bufferDao.processBuffer(BufferType.ITEM_IDENTIFIER)
                bufferBatchDao.waitForBufferBatch(batchId, BufferType.ITEM_IDENTIFIER)
            }

            // Now re-run read-write resolution to pick up the ones we inserted.
            val readWriteResolved = logDuration(this.logger, "final resolve RO") {
                resolver.resolveRO(readOnlyResolved, true)
            }



            val envelopes = logDuration(this.logger, "create envelopes") {
                ingestionDao.createEnvelopes(ingestionDao.createEnvelopeBatches(readWriteResolved))
            }

            logDuration(this.logger, "assert envelopes") {
                assert(envelopes, batchId)
            }

           return batchId
    }

    fun isIngestionRunning(ingestionId: UUID): Boolean {
        // give the buffer processors a nudge.
        BufferType.values().forEach {
            bufferDao.processBuffer(it)
        }

        // Return on the first report of one still running. No need to iterate them all.
        return BufferType.values().any {
            bufferBatchDao.isBufferBatchRunning(ingestionId, it)
        }
    }

    /** Assert all Item Trees from the supplied Envelopes.
     * Performed on a whole batch of Envelopes for insert performance.
     * May throw exceptions if they arise.
     */
    private fun assert(resolved: Collection<EnvelopeBatch>, batchId: UUID) {



        logDuration(logger, "assert relationship statements") {
            val relationshipStatements =
                resolved.flatMap { it.envelopes }.flatMap { envelope ->

                    check(envelope.pk != null) { "All Envelope PKs must be resolved" }

                    envelope.itemTrees.flatMap { itemTree ->

                        itemTreeStatementMapper.toRelationshipStatementsByStrategy(
                            envelope,
                            itemTree
                        ).map { Pair(it, envelope.pk) }
                    }
                }

            relationshipDao.assertRelationships(
                relationshipStatements, batchId
            )
        }
        bufferDao.processBuffer(BufferType.RELATIONSHIP_ASSERTION)




        logDuration(logger, "assert property statements") {
            val propertyStatements =
                resolved.flatMap { it.envelopes }.flatMap { envelope ->
                    check(envelope.pk != null) { "Envelope PK must be resolved" }
                    envelope.itemTrees.flatMap { itemTree ->
                        itemTreeStatementMapper.toPropertyStatementsByStrategy(
                            envelope,
                            itemTree
                        ).map { Pair(it, envelope.pk) }
                    }
                }

            propertyDao.assertPropertyStatements(
                propertyStatements, batchId
            )
        }

        bufferDao.processBuffer(BufferType.PROPERTY_ASSERTION)

        logger.debug("Assert batch: finished")
    }

    /**
     * Get all [RelationshipAssertionStatement]s for the @param[subject], @param[relationship], @param[obj] triple, asserted by @param[assertedBy].
     * By default, returns only current relationships. If @param[current] is false, retrieves only non-current relationships.
     */
    fun getRelationshipStatements(
        subject: Identifier,
        relationship: String,
        obj: Identifier,
        assertedBy: Identifier,
        state: Boolean = true,
        current: Boolean = true,
    ): Collection<RelationshipAssertionStatement> {

        // Read-only because this is a query. If they don't exist, don't create them.
        val resolvedSubj = resolver.resolveRO(subject)
        val resolvedObj = resolver.resolveRO(obj)

        val resolvedAssertedBy = resolver.resolveRO(assertedBy)
        val resolvedRelationship = relationshipTypeDao.resolveRelationshipTypeRO(relationship)

        if (resolvedSubj.pk == null || resolvedObj.pk == null || resolvedRelationship == null || resolvedAssertedBy.pk == null) {
            return emptyList()
        }

        return relationshipDao.getAllStatements(
            resolvedAssertedBy.pk,
            resolvedSubj.pk,
            resolvedRelationship,
            resolvedObj.pk,
            state,
            current
        )
    }

    /**
     * Get all [PropertyAssertionStatement]s for the @param[subject], asserted by @param[assertedBy].
     * By default, returns only current properties. If @param[current] is false, retrieves only non-current
     * relationships.
     *
     * The semantics of the Item Graph dictates that only one PropertyStatement can be current at one time, so when
     * @param[current] is true, this will return either an empty collection, or a collection with one item.
     *
     * There can be any number of historical past non-current PropertyStatements, so when @param[current] is false, a
     * larger collection may be returned.
     */
    fun getPropertyStatements(
        subject: Identifier,
        assertedBy: Identifier,
        current: Boolean = true,
    ): Collection<PropertyAssertionStatement> {
        val resolvedSubj = resolver.resolveRO(subject)
        val resolvedAssertedBy = resolver.resolveRO(assertedBy)

        if (resolvedSubj.pk == null || resolvedAssertedBy.pk == null) {
            return emptyList()
        }

        return propertyDao.getAllStatements(resolvedSubj.pk, resolvedAssertedBy.pk, current)
    }
}
