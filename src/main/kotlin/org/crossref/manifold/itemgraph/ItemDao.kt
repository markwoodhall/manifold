package org.crossref.manifold.itemgraph

import org.crossref.manifold.db.BufferBatchDao
import org.crossref.manifold.db.BufferDao
import org.crossref.manifold.db.BufferType
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.PreparedStatementSetter
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.net.URI
import java.sql.ResultSet
import java.util.*

/** Pair of Identifier PK and corresponding Item PK.
 * A [Pair<Long, Long>] would be confusing and error-prone.
 */
data class IdentifierPkItemPk(val identifierPk: Long, val itemPk: Long)

/** DAO for Items and their Identifiers. Because Items are retrieved by their Identifiers these are grouped into the
 * same DAO.
 */
@Repository
class ItemDao(
    private val jdbcTemplate: JdbcTemplate,
    private val bufferBatchDao: BufferBatchDao,
    private val bufferDao: BufferDao,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    @Lazy
    @Autowired
    private lateinit var selfProxy: ItemDao

    /**
     * If the ItemIdentifier exists, add its PK and return, along with its Item's PK.
     * If not, return the ItemIdentifier unchanged.
     * @return The Identifier and Item PKs, if found.
     *
     * Only consult the URI, not the persistent flag.
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    fun findItemAndIdentifierPk(identifier: Identifier): IdentifierPkItemPk? =
        npTemplate.query(
            """WITH
                    scheme AS (SELECT pk FROM scheme WHERE val = :scheme),
                    host AS (SELECT pk FROM host WHERE val = :host),
                    SELECT iid.pk as identifier_pk, item_pk 
                    FROM item_identifier iid
                    WHERE 
                    iid.path = :path
                    AND iid.host_pk = host.pk
                    AND iid.query = :query
                    AND iid.scheme_pk = scheme.pk 
                    AND iid.port = :port
                    AND iid.fragment = :fragment
            """.trimIndent(),
            unresolvedRowMapper(identifier.uri)

        ) { rs: ResultSet, _: Int ->
            IdentifierPkItemPk(identifierPk = rs.getLong("identifier_pk"), itemPk = rs.getLong("item_pk"))
        }.firstOrNull()

    /**
     * For a collection of Identifier values, return a map that can be used to resolve them.
     * Read only, so will retrieve only those Identifiers already known. Those not yet known are not returned.
     * @return map of pairs of Identifier PK and corresponding Item PK.
     *
     * Use URI, not Identifier, as Identifier has extra fields (e.g. persistent) that aren't relevant for resolution
     * and would cause false negatives.
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun getIdentifierMappingsRO(
        identifiers: Collection<URI>,
    ): Map<URI, IdentifierPkItemPk> = if (identifiers.isEmpty()) {
        // Special case, the SQL templating doesn't like an empty list.
        emptyMap()
    } else {

        jdbcTemplate.execute(
            """
            CREATE TEMP TABLE identifiers_to_resolve (
                scheme VARCHAR NOT NULL,
                scheme_null BOOLEAN NOT NULL,
                host VARCHAR NOT NULL,
                host_null BOOLEAN NOT NULL,
                port INT NOT NULL,
                path VARCHAR NOT NULL,
                path_null BOOLEAN NOT NULL,
                query VARCHAR NULL,
                query_null BOOLEAN NOT NULL,
                fragment VARCHAR NULL,
                fragment_null BOOLEAN NOT NULL
            ) ON COMMIT DROP
            """
        )

        npTemplate.batchUpdate(
            """INSERT INTO identifiers_to_resolve (
                scheme, 
                scheme_null, 
                host, 
                host_null,
                port, 
                path, 
                path_null,
                query, 
                query_null,
                fragment,
                fragment_null
            ) VALUES (
                :scheme,
                :scheme_null,
                :host,
                :host_null,
                :port,
                :path,
                :path_null,
                :query,
                :query_null,
                :fragment,
                :fragment_null)""".trimMargin(),
            identifiers.map  {
                unresolvedRowMapper(it)
            }.toTypedArray()
        )

        // These indexes help the join.
        // The lack of them caused progressively worse performance when the number of item_identifiers nears 300 million.
        jdbcTemplate.execute(
            "CREATE INDEX ON identifiers_to_resolve (scheme, scheme_null)",
        )
        jdbcTemplate.execute(
            "CREATE INDEX ON identifiers_to_resolve (host, host_null)",
        )

        jdbcTemplate.execute(
            "ANALYZE identifiers_to_resolve",
        )

        var result = mutableMapOf<URI, IdentifierPkItemPk>()

        val preparedStatementSetter = PreparedStatementSetter { ps -> ps.fetchSize = 1000 }
        jdbcTemplate.queryForStream(
            """ 
                SELECT 
                    iid.pk AS identifier_pk,
                    item_pk,
                    s.val AS scheme,
                    s.val_null AS scheme_null,
                    h.val AS host,
                    h.val_null AS host_null,
                    iid.port AS port,
                    iid.path AS path,
                    iid.path_null AS path_null,
                    iid.query AS query,
                    iid.query_null AS query_null,
                    iid.fragment AS fragment,
                    iid.fragment_null AS fragment_null
                FROM identifiers_to_resolve itr
                JOIN scheme s ON s.val = itr.scheme AND s.val_null = itr.scheme_null
                JOIN host h ON h.val = itr.host AND h.val_null = itr.host_null
                JOIN item_identifier iid 
                    ON  iid.path = itr.path
                    AND iid.path_null = itr.path_null
                    AND iid.host_pk = h.pk
                    AND iid.query = itr.query
                    AND iid.query_null = itr.query_null
                    AND iid.scheme_pk = s.pk 
                    AND iid.port = itr.port 
                    AND iid.fragment = itr.fragment
                    AND iid.fragment_null = itr.fragment_null
            """,
            preparedStatementSetter,
        ) { rs: ResultSet, _: Int ->
            val identifier = uriRowMapper(rs)
            val identifierPk = rs.getLong("identifier_pk")
            val itemPk = rs.getLong("item_pk")

            identifier to IdentifierPkItemPk(identifierPk, itemPk)
        }.forEach {
            result[it.first] = it.second
        }

        result
    }

    /**
     * Map an unresolved Item to an insertion into the buffer.
     */
    fun unresolvedItemRowMapper(item: Item) : Map<String, *>  {
        // This is checked in calling code.
        check(item.identifiers.count() == 1) {"Item must have exactly one identifier."}
        val identifier = item.identifiers.first()

        return mapOf(
            "scheme" to (identifier.getScheme() ?: ""),
            "scheme_null" to (identifier.getScheme() == null),
            "host" to (identifier.getHost() ?: ""),
            "host_null" to (identifier.getHost() == null),
            "port" to identifier.getPort(),
            "path" to (identifier.getPath() ?: ""),
            "path_null" to (identifier.getPath() == null),
            "query" to (identifier.getQuery() ?: ""),
            "query_null" to (identifier.getQuery() == null),
            "fragment" to (identifier.getFragment() ?: ""),
            "fragment_null" to (identifier.getFragment() == null),
            "blank" to item.blank,
            "persistent" to identifier.persistent
        )
    }

    fun unresolvedRowMapper(identifier: URI) : Map<String, *> =
        mapOf(
            "scheme" to (identifier.scheme ?: ""),
            "scheme_null" to (identifier.scheme == null),
            "host" to (identifier.host ?: ""),
            "host_null" to (identifier.host == null),
            "port" to identifier.port,
            "path" to (identifier.path ?: ""),
            "path_null" to (identifier.path == null),
            "query" to (identifier.query ?: ""),
            "query_null" to (identifier.query == null),
            "fragment" to (identifier.fragment ?: ""),
            "fragment_null" to (identifier.fragment == null)
        )

    fun identifierRowMapper(rs: ResultSet) : Identifier {
        val uri = uriRowMapper(rs)
        val persistent = rs.getBoolean("persistent")

        return Identifier(uri=uri, persistent = persistent)
    }

    fun uriRowMapper(rs: ResultSet) : URI {
        val scheme = rs.getString("scheme")
        val schemeNull = rs.getBoolean("scheme_null")
        val host = rs.getString("host")
        val hostNull = rs.getBoolean("host_null")
        val port = rs.getInt("port")
        val path = rs.getString("path")
        val pathNull = rs.getBoolean("path_null")
        val query = rs.getString("query")
        val queryNull = rs.getBoolean("query_null")
        val fragment = rs.getString("fragment")
        val fragmentNull = rs.getBoolean("fragment_null")

        return URI(
            if (schemeNull) null else scheme,
            null,
            if (hostNull) null else host,
            port,
            if (pathNull) null else path,
            if (queryNull) null else query,
            if (fragmentNull) null else fragment
        )
    }

    /**
     * For a set of ItemPks, retrieve a mapping of each Item PK to its Item PK and resolved identifier.
     */
    fun fetchItemIdentifierPairsForItemPks(itemPks: Set<Long>): Collection<Pair<Long, Identifier>> =
        // Need to guard, Postgres doesn't like an empty list.
        if (itemPks.isEmpty()) {
            emptyList()
        } else {
            npTemplate.query(
                """
                SELECT 
                    itr.pk as identifier_pk,
                    item_pk,
                    path,
                    path_null,
                    h.val as host,
                    h.val_null as host_null,
                    query,
                    query_null,
                    s.val as scheme,
                    s.val_null as scheme_null,
                    port,
                    fragment,
                    fragment_null,
                    persistent
                FROM item_identifier itr
                JOIN scheme s ON s.pk = itr.scheme_pk 
                JOIN host h ON h.pk = itr.host_pk
                WHERE item_pk IN (:item_pks)
                """.trimIndent(),
                mapOf("item_pks" to itemPks)
            ) { rs, _ ->
                Pair(
                    rs.getLong("item_pk"),
                    identifierRowMapper(rs)
                )
            }
        }

    fun insertIdentifiers(identifiers: Collection<Item>) {
        val batchId = bufferBatchDao.openBufferBatch()

        selfProxy.addIdentifiersToBatch(identifiers, batchId)
        bufferDao.processBuffer(BufferType.ITEM_IDENTIFIER)
        bufferBatchDao.waitForBufferBatch(batchId, BufferType.ITEM_IDENTIFIER)
    }

    fun addIdentifiersToBatch(items: Collection<Item>, batchId: UUID) {
        bufferBatchDao.addToBufferBatch(
            BufferType.ITEM_IDENTIFIER,
            items.distinct().map {
                MapSqlParameterSource(
                    unresolvedItemRowMapper(it)
                )
            }, batchId
        )
    }
}
