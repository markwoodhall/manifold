package org.crossref.manifold.itemgraph.stats

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Statistics that represent the number of Items, Relationships etc in the store.
 */
data class ManifoldStats(
    /**
     * Total number of Items.
     */
    @JsonProperty("num-items")
    val numItems: Long,

    /**
     * Total number of known Relationship Statements.
     */
    @JsonProperty("total-relationship-statements")
    val totalRelationshipStatements: Long,

    /**
     * Total number of known Relationship Statement Summaries.
     */
    @JsonProperty("total-current-relationship-statements")
    val totalRelationshipStatementSummaries: Long,

    /**
     * Total number of known Property Statements.
     */
    @JsonProperty("total-property-statements")
    val totalPropertyStatements: Long,

    /**
     * Total number of known Property Statement Summaries.
     */
    @JsonProperty("total-current-property-statements")
    val totalPropertyStatementSummaries: Long,

    /**
     * Total number of known Identifiers, of any type.
     */
    @JsonProperty("total-item-identifiers")
    val numItemIdentifiers: Long,

    /**
     * Total number of known relationship types.
     */
    @JsonProperty("total-relationship-types")
    val numRelationshipTypes: Long,

    /**
     * Total number of envelopes ever asserted
     */
    @JsonProperty("total-envelopes")
    val numEnvelopes: Long,

    /**
     * Total number of envelopes ever asserted
     */
    @JsonProperty("total-envelope-batches")
    val numEnvelopeBatches: Long,
)
