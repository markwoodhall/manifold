package org.crossref.manifold.itemgraph

import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.springframework.jdbc.core.RowMapper

val envBatchProvMapper: RowMapper<EnvelopeBatchProvenance> = RowMapper { rs, _ ->
    EnvelopeBatchProvenance(
        userAgent = rs.getString("user_agent"),
        externalTrace = rs.getString("ext_trace"),
        envelopeBatchPk = rs.getLong("envelope_batch_pk"),
    )
}
