package org.crossref.manifold.itemgraph

import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.retrieval.IdentifierRetriever
import org.springframework.stereotype.Service

/**
 * A Collection is a grouping of Items, for example "the set of works", "the set of members".
 * An Item is put in a collection by having a 'collection' relationship asserted.
 */
@Service
class Collections(
    val collectionsDao: CollectionsDao,
    val resolver: Resolver,
    val itemDao: ItemDao,
) {

    companion object {
        /**
         * Base for collections such as 'works', 'journal-articles', etc.
         */
        private const val COLLECTIONS_URL_BASE = "https://id.crossref.org/collections/"


        fun collectionIdentifierForType(type: String) =
            COLLECTIONS_URL_BASE + type

        fun collectionIdentifierForTypeSubtype(type: String, subType: String) = "$COLLECTIONS_URL_BASE$type/$subType"

        /**
         * Records that the item is in the given Collection.
         */
        const val COLLECTION_RELATIONSHIP_TYPE = "collection"
    }

    /**
     * Given a set of Item PKs, return a set of Items with non-null Identifiers.
     */
    fun itemPksToItemsWithIdentifiers(collectionPks: Set<Long>): List<Item> {
        // Retriever works by supplying all PKs in one go, letting it cache them and then querying.
        val identifierRetriever = IdentifierRetriever(itemDao)
        identifierRetriever.prime(collectionPks)
        return collectionPks.mapNotNull {
            val identifier = identifierRetriever.get(it)?.firstOrNull()
            if (identifier != null) {
                Item().withIdentifier(identifier).withPk(it)
            } else {
                null
            }
        }
    }

    /**
     * For an Item PK, return the set of Collections it's part of as Items, with Identifiers.
     */
    fun getCollectionsForItemPk(itemPk: Long): Collection<Item> {
        val collectionPks = collectionsDao.getCollectionsForItem(itemPk, COLLECTION_RELATIONSHIP_TYPE)
        return itemPksToItemsWithIdentifiers(collectionPks)
    }

    /**
     * For an Item PK, return the collections it's part of, as URI strings.
     */
    fun getCollectionsUrisForItemPk(itemPk: Long): Set<String> {
        val collectionPks = collectionsDao.getCollectionsForItem(itemPk, COLLECTION_RELATIONSHIP_TYPE)
        return itemPksToItemsWithIdentifiers(collectionPks).mapNotNull { it.identifiers.firstOrNull()?.uri.toString() }.toSet()
    }

    /**
     * For a list of ItemPks, partition them into two lists.
     * The first is Item Pks that are Collection items. The second is those that aren't.
     */
    fun partitionItemPks(itemPks: Set<Long>): Pair<Set<Long>, Set<Long>> {
        val collectionItems = collectionsDao.findCollectionItems(itemPks, COLLECTION_RELATIONSHIP_TYPE)

        val nonCollectionItems = itemPks.minus(collectionItems)

        return Pair(collectionItems, nonCollectionItems)
    }
}