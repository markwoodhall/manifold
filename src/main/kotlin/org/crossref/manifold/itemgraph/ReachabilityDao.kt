package org.crossref.manifold.itemgraph

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository

@Repository
/**
 * For each item that's the root of a tree maintain a table of the items contained in its tree.
 * The index on this allows us to find out which trees a given item may be part of.
 */
class ReachabilityDao(
    jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * For a set of item root node PKs, update the reachability index for each, to the given depth.
     */
    fun updateTreeReachability(
        itemPks: Collection<Long>,
        depth: Int,
    ) {
        logger.info("Update reachability for $itemPks")
        itemPks.forEach { itemPk ->
            npTemplate.update(
                "CALL build_reachability(:root_item_pk, :depth)",
                mapOf(
                    "root_item_pk" to itemPk,
                    "depth" to depth,
                )
            )
        }
    }
}
