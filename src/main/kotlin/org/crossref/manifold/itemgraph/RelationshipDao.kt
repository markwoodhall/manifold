package org.crossref.manifold.itemgraph

import org.crossref.manifold.db.BufferBatchDao
import org.crossref.manifold.db.BufferType
import org.crossref.manifold.db.Fetched
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.Configuration.MAX_RETURNED_ROWS
import org.crossref.manifold.registries.RelationshipTypeRegistry
import org.crossref.manifold.retrieval.statements.RelationshipStatementFilter
import org.crossref.manifold.util.randomString
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.sql.Types
import java.time.InstantSource
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.util.*
import kotlin.math.max
import kotlin.math.min

/**
 * DAO for Property statements within the Item Graph.
 * Contains a cache of Relationship Types to map them in and out of the database because the vocabulary is small and
 * well known, and this is a very hot data path.
 */
@Repository
class RelationshipDao(
    private val jdbcTemplate: JdbcTemplate,
    private val relationshipTypeRegistry: RelationshipTypeRegistry,
    private val bufferBatchDao: BufferBatchDao,
    private val instantSource: InstantSource,
    @Value("\${${MAX_RETURNED_ROWS}:1000}") private val maxReturnedRows: Int,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    private val relAssStmtMapper: RowMapper<RelationshipAssertionStatement> = RowMapper { rs, _ ->
        val pk = rs.getLong("relationship_assertion_pk")
        RelationshipAssertionStatement(
            pk = if (rs.wasNull()) null else pk,
            fromItemPk = rs.getLong("from_item_pk"),
            relationshipType = rs.getString("relationship_type_value"),
            toItemPk = rs.getLong("to_item_pk"),
            assertingPartyPk = rs.getLong("party_pk"),
            status = rs.getBoolean("state"),
            assertedAt = rs.getObject("asserted_at", OffsetDateTime::class.java),
        )
    }

    private val relStmtMapper: RowMapper<RelationshipStatement> = RowMapper { rs, _ ->
        val pk = rs.getLong("relationship_pk")
        RelationshipStatement(
            pk = if (rs.wasNull()) null else pk,
            fromItemPk = rs.getLong("from_item_pk"),
            relationshipType = rs.getString("relationship_type_value"),
            toItemPk = rs.getLong("to_item_pk"),
            firstIngestedAt = rs.getObject("first_ingested_at", OffsetDateTime::class.java),
            lastIngestedAt = rs.getObject("last_ingested_at", OffsetDateTime::class.java),
            firstLinkedAt = rs.getObject("first_linked_at", OffsetDateTime::class.java),
            lastLinkedAt = rs.getObject("last_linked_at", OffsetDateTime::class.java),
        )
    }

    /** Insert the given [RelationshipAssertionStatement]s into the Item Graph. These can be from more than one Envelope.
     * @param[relationshipAssertionStatements] collection of Pairs of Relationship Statement and their corresponding Envelope PK.
     */
    fun assertRelationships(
        relationshipAssertionStatements: Collection<Pair<RelationshipAssertionStatement, Long>>,
        batchId: UUID,
    ) {
        bufferBatchDao.addToBufferBatch(
            BufferType.RELATIONSHIP_ASSERTION,
            relationshipAssertionStatements.mapNotNull { (rel, envPk) ->
                relationshipTypeRegistry.resolve(rel.relationshipType).also {
                    if (it == null) {
                        logger.error("Didn't recognise relationship type: ${rel.relationshipType}")
                    }
                }?.let {
                    MapSqlParameterSource(
                        mapOf(
                            "asserted_at" to rel.assertedAt,
                            "ingested_at" to instantSource.instant().atOffset(ZoneOffset.UTC),
                            "party_pk" to rel.assertingPartyPk,
                            "from_item_pk" to rel.fromItemPk,
                            "to_item_pk" to rel.toItemPk,
                            "relationship_type_pk" to it,
                            "envelope_pk" to envPk,
                            "state" to rel.status,
                        )
                    ).apply {
                        registerSqlType("asserted_at", Types.TIMESTAMP_WITH_TIMEZONE)
                        registerSqlType("ingested_at", Types.TIMESTAMP_WITH_TIMEZONE)
                    }
                }
            }, batchId
        )
    }

    /**
     * For a set of Relationship PKs, retrieve those that are currently already asserted by the Party Item PK.
     */
    fun getCurrentSubjRelationshipStatements(
        partyPks: Collection<Long>,
        subjectItemPks: Set<Long>,
    ): Set<RelationshipAssertionStatement> =
        // SQL can't cope with empty lists.
        if (partyPks.isEmpty() || subjectItemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT asserted_at, from_item_pk, rt.value AS relationship_type_value, to_item_pk, party_pk, " +
                        "state, NULL AS relationship_assertion_pk " +
                        "FROM relationship_assertion_current ra " +
                        "JOIN relationship_type rt ON rt.pk = ra.relationship_type_pk " +
                        "WHERE from_item_pk IN (:from_item_pks) " +
                        "AND party_pk IN (:party_pks) " +
                        "AND state",
                mapOf("party_pks" to partyPks, "from_item_pks" to subjectItemPks), relAssStmtMapper
            ).toSet()
        }

    /**
     * For a set of Relationship PKs, get those asserted by anyone.
     */
    fun getCurrentSubjRelationshipStatements(subjectItemPks: Set<Long>): Set<RelationshipAssertionStatement> =
        // SQL can't cope with empty lists.
        if (subjectItemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT ra.pk AS relationship_assertion_pk, asserted_at, from_item_pk, " +
                        "rt.value AS relationship_type_value, to_item_pk, party_pk, state " +
                        "FROM relationship_assertion_current ra " +
                        "JOIN relationship_type rt ON rt.pk = ra.relationship_type_pk " +
                        "WHERE from_item_pk in (:from_item_pks) " +
                        "AND state",
                mapOf("from_item_pks" to subjectItemPks), relAssStmtMapper
            ).toSet()
        }

    /** Get all statements, current or not.
     */
    fun getAllStatements(
        partyPk: Long,
        fromPk: Long,
        relationshipTypePk: Long,
        toPk: Long,
        state: Boolean = true,
        current: Boolean = true,
    ): Set<RelationshipAssertionStatement> =
        npTemplate.query(
            "SELECT asserted_at, from_item_pk, rt.value AS relationship_type_value, to_item_pk, party_pk, " +
                    "state, NULL AS relationship_assertion_pk " +
                    "FROM relationship_assertion ra " +
                    "JOIN relationship_type rt ON rt.pk = ra.relationship_type_pk " +
                    "WHERE from_item_pk = :from_item_pk " +
                    "AND to_item_pk = :to_item_pk " +
                    (if (current) "AND " else "AND NOT ") + "current " +
                    "AND relationship_type_pk = :relationship_type_pk " +
                    "AND party_pk = :party_pk " +
                    "AND state = :state",
            mapOf(
                "party_pk" to partyPk,
                "from_item_pk" to fromPk,
                "relationship_type_pk" to relationshipTypePk,
                "to_item_pk" to toPk,
                "state" to state
            ), relAssStmtMapper
        ).toSet()

    /**
     * Find current Relationship Statements to the Object, asserted by the Party.
     * I.e. Match triples < subjects , relationships , * >
     */
    fun getCurrentSubjRelationshipStatements(
        partyPks: Collection<Long>,
        subjectItemPks: Set<Long>,
        relationshipTypePks: Set<Long>,
    ): Set<RelationshipAssertionStatement> {
        // SQL can't cope with empty lists.
        return if (partyPks.isEmpty() || subjectItemPks.isEmpty() || relationshipTypePks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT asserted_at, from_item_pk, rt.value AS relationship_type_value, to_item_pk, party_pk, " +
                        "state, NULL AS relationship_assertion_pk " +
                        "FROM relationship_assertion_current ra " +
                        "JOIN relationship_type rt ON rt.pk = ra.relationship_type_pk " +
                        "WHERE from_item_pk in (:from_item_pks) " +
                        "AND relationship_type_pk IN (:relationship_type_pks) " +
                        "AND party_pk IN (:party_pks) " +
                        "AND state",
                mapOf(
                    "party_pks" to partyPks,
                    "from_item_pks" to subjectItemPks,
                    "relationship_type_pks" to relationshipTypePks
                ), relAssStmtMapper
            ).toSet()
        }
    }

    /**
     * Fetch a page of Statement Items starting from the given Statement Pk.
     * Return an optional starting Statement Pk if there are more items.
     */
    fun getRelationshipStatementRange(
        startPk: Long,
        count: Int,
        filter: RelationshipStatementFilter,
    ): List<Pair<RelationshipAssertionStatement, EnvelopeBatchProvenance>> =
        npTemplate.query(
            "SELECT ra.pk as relationship_assertion_pk, from_item_pk, to_item_pk, party_pk, state, asserted_at, " +
                    "value as relationship_type_value, eb.pk as envelope_batch_pk, user_agent, ext_trace " +
                    "FROM relationship_assertion ra " +
                    "JOIN envelope e ON ra.envelope_pk = e.pk " +
                    "JOIN envelope_batch eb ON e.envelope_batch_pk = eb.pk " +
                    "JOIN relationship_type rt ON rt.pk = ra.relationship_type_pk " +
                    "WHERE ra.pk >= :start_pk " +
                    (if (filter.subjectPk != null) {
                        "AND from_item_pk = :from_item_pk "
                    } else "") +
                    (if (filter.objectPk != null) {
                        "AND to_item_pk = :to_item_pk "
                    } else "") +
                    (if (filter.relationshipTypePk != null) {
                        "AND relationship_type_pk = :relationship_type_pk "
                    } else "") +
                    (if (filter.partyPk != null) {
                        "AND party_pk = :party_pk "
                    } else "") +
                    "ORDER BY relationship_assertion_pk LIMIT :count",
            mapOf(
                "start_pk" to startPk,
                "count" to count,
                "from_item_pk" to filter.subjectPk,
                "to_item_pk" to filter.objectPk,
                "relationship_type_pk" to filter.relationshipTypePk,
                "party_pk" to filter.partyPk
            )
        ) { rs, rowNum ->
            // These are our own RowMapper implementations and we know they don't return null.
            Pair(relAssStmtMapper.mapRow(rs, rowNum)!!, envBatchProvMapper.mapRow(rs, rowNum)!!)
        }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun getCurrentPartySubjectRelationships(
        assertingPartyPk: Long,
        subjectPks: Collection<Long>,
    ): Collection<RelationshipAssertionStatement> = if (subjectPks.isEmpty()) {
        emptyList()
    } else {
        jdbcTemplate.execute(
            """
            CREATE TEMP TABLE subjects (
                from_item_pk BIGINT NOT NULL
            ) ON COMMIT DROP
            """
        )

        npTemplate.batchUpdate(
            "INSERT INTO subjects (from_item_pk) VALUES (:from_item_pk)",
            subjectPks.distinct().map {
                mapOf(
                    "from_item_pk" to it
                )
            }.toTypedArray()
        )

        jdbcTemplate.query(
            """
            SELECT
                rac.pk AS relationship_assertion_pk,
                asserted_at, 
                rac.from_item_pk, 
                rt.value AS relationship_type_value, 
                to_item_pk, 
                party_pk, 
                state
            FROM relationship_assertion_current rac
            JOIN subjects s ON s.from_item_pk = rac.from_item_pk
            JOIN relationship_type rt ON rt.pk = rac.relationship_type_pk
            WHERE party_pk = ?
            AND state
            """, relAssStmtMapper, assertingPartyPk
        )
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun fetchRelationships(
        cursor: String?,
        itemPk: Long?,
        fromItemPk: Long?,
        toItemPk: Long?,
        relationshipType: String?,
        fromCreatedTime: OffsetDateTime?,
        untilCreatedTime: OffsetDateTime?,
        fromUpdatedTime: OffsetDateTime?,
        untilUpdatedTime: OffsetDateTime?,
        rows: Int?,
    ): Fetched<RelationshipStatement> {
        val sql =
            """
            SELECT r.pk  AS relationship_pk,
                r.from_item_pk,
                r.to_item_pk,
                rt.value AS relationship_type_value,
                r.first_ingested_at,
                r.last_ingested_at,
                r.first_linked_at,
                r.last_linked_at
            FROM relationship r
                     JOIN relationship_type rt ON r.relationship_type_pk = rt.pk
            """ +
                    (if (itemPk != null) "AND (r.from_item_pk = :itemPk OR r.to_item_pk = :itemPk) " else "") +
                    (if (fromItemPk != null) "AND r.from_item_pk = :fromItemPk " else "") +
                    (if (toItemPk != null) "AND r.to_item_pk = :toItemPk " else "") +
                    (if (relationshipType != null) "AND rt.value = :relationshipType " else "") +
                    (if (fromCreatedTime != null) "AND ((r.first_linked_at IS NULL AND r.first_ingested_at >= :fromCreatedTime) OR r.first_linked_at >= :fromCreatedTime) " else "") +
                    (if (untilCreatedTime != null) "AND ((r.first_linked_at IS NULL AND r.first_ingested_at <= :untilCreatedTime) OR r.first_linked_at <= :untilCreatedTime) " else "") +
                    (if (fromUpdatedTime != null) "AND (r.last_ingested_at >= :fromUpdatedTime OR (r.last_linked_at IS NOT NULL AND r.last_linked_at >= :fromUpdatedTime)) " else "") +
                    (if (untilUpdatedTime != null) "AND (r.last_ingested_at <= :untilUpdatedTime AND (r.last_linked_at IS NULL OR r.last_linked_at <= :untilUpdatedTime)) " else "") +
                    (if (cursor != null && cursor != "*") "AND r.pk > :lastKey " else "")
        val rowsToFetch = min(max(rows ?: maxReturnedRows, 0), maxReturnedRows)
        if (cursor == null || cursor == "*") {
            val params = getParams(
                itemPk,
                fromItemPk,
                toItemPk,
                relationshipType,
                fromCreatedTime,
                untilCreatedTime,
                fromUpdatedTime,
                untilUpdatedTime
            )
            // A count query will not return null.
            val totalRows = npTemplate.queryForObject(
                "SELECT count(*) FROM ($sql) counted", params, Integer::class.java,
            )?.toInt()!!
            val fetched = npTemplate.query("$sql ORDER by relationship_pk LIMIT $rowsToFetch", params, relStmtMapper)
            if (fetched.isEmpty()) {
                return Fetched(emptyList(), 0, rowsToFetch, null)
            }
            var nextCursor: String? = null
            if (cursor == "*") {
                // Retrieved relationship statements will always have a primary key.
                val lastKey = fetched.last().pk!!
                nextCursor = String.randomString()
                npTemplate.update(
                    "INSERT INTO open_cursor (cursor_name, row_count, created_at, last_used_at, last_key) " +
                            "VALUES (:nextCursor, :totalRows, NOW(), NOW(), :lastKey)",
                    mapOf(
                        "nextCursor" to nextCursor,
                        "totalRows" to totalRows,
                        "lastKey" to lastKey
                    )
                )
            }
            return Fetched(fetched, totalRows, rowsToFetch, nextCursor)
        } else {
            var fetched: List<RelationshipStatement> = emptyList()
            var totalRows = 0
            jdbcTemplate.query(
                "SELECT row_count, created_at, last_key FROM open_cursor WHERE cursor_name = ? FOR UPDATE",
                { rs ->
                    // Only one row will be returned as cursor_name is unique.
                    val params = getParams(
                        itemPk,
                        fromItemPk,
                        toItemPk,
                        relationshipType,
                        fromCreatedTime,
                        untilCreatedTime,
                        fromUpdatedTime,
                        untilUpdatedTime
                    ).apply {
                        addValue("lastKey", rs.getLong("last_key"))
                    }
                    totalRows = rs.getInt("row_count")
                    fetched =
                        npTemplate.query("$sql ORDER by relationship_pk LIMIT $rowsToFetch", params, relStmtMapper)
                },
                cursor
            )
            if (fetched.isEmpty()) {
                return Fetched(emptyList(), 0, rowsToFetch, null)
            }
            // Retrieved relationship statements will always have a primary key.
            val lastKey = fetched.last().pk!!
            val nextCursor = String.randomString()
            npTemplate.update(
                "UPDATE open_cursor " +
                        "SET cursor_name = :nextCursor, last_used_at = NOW(), last_key = :lastKey " +
                        "WHERE cursor_name = :cursor",
                mapOf(
                    "nextCursor" to nextCursor,
                    "lastKey" to lastKey,
                    "cursor" to cursor
                )
            )
            return Fetched(fetched, totalRows, rowsToFetch, nextCursor)
        }
    }

    private fun getParams(
        itemPk: Long?,
        fromItemPk: Long?,
        toItemPk: Long?,
        relationshipType: String?,
        fromCreatedTime: OffsetDateTime?,
        untilCreatedTime: OffsetDateTime?,
        fromUpdatedTime: OffsetDateTime?,
        untilUpdatedTime: OffsetDateTime?,
    ): MapSqlParameterSource = MapSqlParameterSource().apply {
        itemPk.also {
            addValue("itemPk", it)
        }
        fromItemPk.also {
            addValue("fromItemPk", it)
        }
        toItemPk.also {
            addValue("toItemPk", it)
        }
        relationshipType.also {
            addValue("relationshipType", relationshipType)
        }
        fromCreatedTime.also {
            addValue("fromCreatedTime", it)
            registerSqlType("fromCreatedTime", Types.TIMESTAMP_WITH_TIMEZONE)
        }
        untilCreatedTime.also {
            addValue("untilCreatedTime", it)
            registerSqlType("untilCreatedTime", Types.TIMESTAMP_WITH_TIMEZONE)
        }
        fromUpdatedTime.also {
            addValue("fromUpdatedTime", it)
            registerSqlType("fromUpdatedTime", Types.TIMESTAMP_WITH_TIMEZONE)
        }
        untilUpdatedTime.also {
            addValue("untilUpdatedTime", it)
            registerSqlType("untilUpdatedTime", Types.TIMESTAMP_WITH_TIMEZONE)
        }
    }
}
