package org.crossref.manifold.itemgraph


import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.getUnambiguousUnresolvedIdentifiers
import org.crossref.manifold.itemtree.*
import org.crossref.manifold.retrieval.view.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.net.URI
import java.util.*

/**
 * Resolve Items, and Item Trees against the Item Graph. 'Resolution' means having a known Primary Key (PK) in the
 * database.
 * Read-only resolution will query only for already known Items. It is useful for querying and first-pass on ingestion.
 * Read-write resolution will create Items and Identifiers. It is useful for ingestion.
 */
@Service
class Resolver(
    private val itemDao: ItemDao,
) {

    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Resolve Items by their identifiers using pre-existing data.
     * This is an optimisation before full tree resolution, and is both contention-safe and better optimised for SQL.
     */
    fun resolveRO(itemTree: Item): Item {

        // The pair mapping describes how to resolve ItemIdentifiers, using only their URI.
        val presentUnresolved = unresolvedIdentifiers(itemTree).map { it.uri }
        val pairMapping = itemDao.getIdentifierMappingsRO(presentUnresolved)

        return resolveItemTreeFromIdentifierMappings(
            itemTree,
            pairMapping
        )
    }

    /**
     * Resolve an Identifier.
     * Useful for serving queries that aren't part of a larger ItemTree.
     */
    fun resolveRO(identifier: Identifier): Item =
        resolveRO(Item().withIdentifier(identifier))


    /**
     * Resolve all Items in a collection of Envelope Batches by identifiers using pre-existing data.
     * This is optimised to fetch all identifiers in one batch of envelope batches.
     * Useful for large batches in bulk ingestion.
     */
    fun resolveRO(inputs: Collection<EnvelopeBatch>, expectFull: Boolean = false): Collection<EnvelopeBatch> {
        // Work with URIs, not [Identifier]s objects, as the Identifier has other fields (e.g. persistent) which aren't relevant to resolution,
        // and would lead to false negatives.
        val uris = inputs.flatMap { getUnambiguousUnresolvedIdentifiers(it) }.mapNotNull { it.identifiers.firstOrNull()?.uri }

        val identifierSize = uris.sumOf {it.toString().length}
        logger.debug("Resolve RO ${uris.count()} identifiers totalling $identifierSize characters.")

        val mapping = itemDao.getIdentifierMappingsRO(uris)
        logger.debug("Got mapping...")

        val result = inputs.map { envelopeBatch ->
            envelopeBatch.withEnvelopes(
                envelopeBatch.envelopes.map { envelope ->
                    envelope
                        .withAssertion(
                            envelope.assertion.withAssertingParty(
                                resolveItemTreeFromIdentifierMappings(envelope.assertion.assertingParty, mapping)
                            )
                        )
                        .withItemTrees(envelope.itemTrees.map { resolveItemTreeFromIdentifierMappings(it, mapping) })
                })
        }

        if (expectFull) {
            val unresolvedItems = result.flatMap { getUnambiguousUnresolvedIdentifiers(it) }.mapNotNull { it.identifiers.firstOrNull() }
            if (unresolvedItems.isNotEmpty()) {
                logger.error("Unexpectedly unresolved $unresolvedItems")
            }
        }

        return result
    }

    /**
     * Resolve all Items in an Envelope Batch by identifiers using pre-existing data.
     * This is optimised to fetch all identifiers in one batch.
     */
    fun resolveRO(input: EnvelopeBatch): EnvelopeBatch {
        val identifiers = getUnambiguousUnresolvedIdentifiers(input).mapNotNull { it.identifiers.firstOrNull()?.uri }

        val mapping = itemDao.getIdentifierMappingsRO(identifiers)

        return input.withEnvelopes(
            input.envelopes.map { envelope ->
                envelope
                    .withAssertion(
                        envelope.assertion.withAssertingParty(
                            resolveItemTreeFromIdentifierMappings(envelope.assertion.assertingParty, mapping)
                        )
                    )
                    .withItemTrees(envelope.itemTrees.map { resolveItemTreeFromIdentifierMappings(it, mapping) })
            })

    }

    /**
     * Add to batch for read-write resolution.
     */
    fun bufferToResolveRW(inputs: Collection<EnvelopeBatch>, batchId: UUID) {
        val identifiers = inputs.flatMap { getUnambiguousUnresolvedIdentifiers(it) }

        logger.debug("Queue to resolve RW identifiers for ${identifiers.count()} items.")
        itemDao.addIdentifiersToBatch(identifiers, batchId)
    }

    fun resolveSynchronousRW(itemTrees: Collection<Item>): List<Item> {
        val items = itemTrees.flatMap { getUnambiguousUnresolvedItems(it) }
        logger.debug("Queue to resolve RW identifiers for ${items.count()} items.")
        itemDao.insertIdentifiers(items)
        return itemTrees.map { resolveRO(it) }
    }

    /**
     * Fully or partially resolve an Item Tree based on the ItemIdentifier mapping.
     */
    private fun resolveItemTreeFromIdentifierMappings(
        itemTree: Item, identifierMappings: Map<URI, IdentifierPkItemPk>,
    ): Item {
        fun recurse(item: Item): Item {
            val resolved = resolveItemFromIdentifierMappings(item, identifierMappings)
            return resolved.withRelationships(resolved.rels.map { rel ->
                rel.withItem(recurse(rel.obj))
            })
        }

        return recurse(itemTree)
    }

    /**
     * Fully or partially resolve an Item based on the ItemIdentifier mapping.
     */
    private fun resolveItemFromIdentifierMappings(
        item: Item,
        identifierMappings: Map<URI, IdentifierPkItemPk>,
    ): Item {
        // Resolve the pairs of maybe-resolved ItemIdentifier, Item PK. There may be nulls.
        // If the ItemIdentifier wasn't known in the mapping, return it back with a null Item PK.
        val foundItemIdentifierItemPks: Collection<Pair<Identifier, Long?>> =
            item.identifiers.map {
                val identifierPkItemPk = identifierMappings[it.uri]
                if (identifierPkItemPk == null) {
                    Pair(it, null)
                } else {
                    Pair(it.withPk(identifierPkItemPk.identifierPk), identifierPkItemPk.itemPk)
                }
            }

        // The list of Identifiers. At this point they may or may not be resolved!
        val maybeResolvedIdentifiers = foundItemIdentifierItemPks.map(Pair<Identifier, Long?>::first)

        return if (item.identifiers.isEmpty()) {
            // If there are no Identifiers then we'll need to create a blank node, which happens in read-write resolution.
            // Pass through.
            item
        } else if (foundItemIdentifierItemPks.any { it.first.pk == null }) {
            // If any of the ItemIdentifiers was null, we failed to resolve it. We'll need read-write resolution, so pass.
            // Important for the relationship between read-only and read-write resolution:
            // Do not attach ItemIdentifier PKs, even if we know them, because the read-write resolution will need to fetch them again and create new item identifiers in a single transaction.
            item
        } else {
            // Now we're confident that all identifiers did resolve, we need to ask if the result was unambiguous.
            // Again, we can assume that there are no nulls in the Item PK field if all the Items resolved.
            // So, filtering out the nulls will have no effect but to remove the nullability of the type.
            val distinctItemPks = foundItemIdentifierItemPks.asSequence().mapNotNull { pair -> pair.second }.distinct()
                .toList()

            if (distinctItemPks.isEmpty()) {
                // If itemIdentifiers is not empty then the distinct values will not be empty.
                // This branch shouldn't ever happen. If it somehow does, fail-safe.
                item
            } else if (distinctItemPks.count() > 1) {
                // If they pointed to more than one distinct Item PK, that's a conflict.
                // We can't resolve it, so pass through item unchanged.
                item
            } else {
                // Else they point to one distinct Item PK. This is a successful match.
                val itemPk = distinctItemPks.first()

                // At this point we know they are all resolved.
                item.withPk(itemPk).withIdentifiers(maybeResolvedIdentifiers)
            }
        }
    }
}
