package org.crossref.manifold.itemgraph

object Consts {
    /**
     * Default depth of an item tree. Used for retrieval of trees and for maintaining item tree reachability index.
     */
    const val DEFAULT_TREE_DEPTH = 3
}