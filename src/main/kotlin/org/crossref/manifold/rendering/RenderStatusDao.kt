package org.crossref.manifold.rendering

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.PreparedStatementSetter
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.sql.Connection
import java.sql.ResultSet
import java.sql.Types

@Repository
class RenderStatusDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    private val renderStatusMapper: RowMapper<RenderStatus> = RowMapper { resultSet, _ ->
        rowMapper(resultSet)
    }

    fun rowMapper(resultSet: ResultSet): RenderStatus {
        return RenderStatus(
            itemPk = resultSet.getLong("item_pk"),
            stale = resultSet.getBoolean("stale"),
            pk = resultSet.getLong("pk"),
        )
    }

    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Callback with the next chunk of rows from the item status queue.
     * This will co-operate the locks of other concurrent calls, so can be scaled out.
     */
    @Transactional
    fun getFromQueue(count: Int, f: ((itemPkBatch: Collection<Long>) -> Unit)) {
        val params = mapOf("count" to count)

        val itemPks = npTemplate.query(
            """            
            DELETE FROM item_render_status_queue
            WHERE pk IN (
              SELECT pk
              FROM item_render_status_queue
              ORDER BY pk ASC
              FOR UPDATE SKIP LOCKED
              LIMIT :count
            )
            RETURNING item_pk;
        """.trimIndent(),
            params
        ) { rs: ResultSet, _: Int -> rs.getLong("item_pk") }.distinct()
        f(itemPks)
    }

    /**
     * Get current length of queue.
     */
    fun queueLength(): Long =
        npTemplate.query("SELECT count(pk) AS count FROM item_render_status_queue") { rs: ResultSet, _: Int ->
            rs.getLong(
                "count"
            )
        }.first()


    /**
     * For a set of item PKs, trigger the staleness of all item trees that it's part of.
     * Use the Reachability index.
     * See docs for set_stale_trees_from_items. If it finds an item in its own render index (i.e. there's a circular
     * reference) then it will not trigger that as in need of another render. To do so would create an infinite loop.
     */
    fun triggerStaleFromIndex(itemPks: Set<Long>) {
        logger.info("Trigger stale trees for items: $itemPks")

        val params = MapSqlParameterSource("itemPks", jdbcTemplate.execute { c: Connection ->
            c.createArrayOf("BIGINT", itemPks.toTypedArray())
        }).apply {
            registerSqlType("itemPks", Types.ARRAY)
        }

        npTemplate.update(
            "CALL set_stale_trees_from_items(:itemPks :: BIGINT[])",
            params
        )
    }
}
