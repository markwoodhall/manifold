package org.crossref.manifold.rendering

import org.crossref.manifold.itemtree.Item

/**
 * A contract for ItemTree renderers
 *
 * The render function should render an Item as a string
 * based on the ItemTree renderer ContentType
 */
interface ContentTypeRenderer {
    fun internalContentType(): ContentType
    fun collection(): String
    fun render(itemTree: Item): String?
}
