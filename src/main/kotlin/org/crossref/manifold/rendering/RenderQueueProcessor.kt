package org.crossref.manifold.rendering

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.crossref.manifold.itemgraph.Collections
import org.crossref.manifold.itemgraph.Consts
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.registries.AuthorityRegistry
import org.crossref.manifold.rendering.Configuration.RENDER_QUEUE_PROCESSOR_ENABLED
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.retrieval.itemtree.ItemFetchConfiguration
import org.crossref.manifold.retrieval.itemtree.ItemFetchStrategy
import org.crossref.manifold.util.Constants.BATCH_SIZE
import org.crossref.manifold.util.Constants.FIXED_DELAY
import org.crossref.manifold.util.Constants.INITIAL_DELAY
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.util.concurrent.TimeUnit

@Component
@ConditionalOnProperty(prefix = RENDERING, name = [RENDER_QUEUE_PROCESSOR_ENABLED])
class RenderQueueProcessor(
    private val renderStatusDao: RenderStatusDao,
    @Value("\${$RENDERING.$BATCH_SIZE:100}") private val batchSize: Int,

    private val authorityRegistry: AuthorityRegistry,
    private val collections: Collections,
    private val itemTreeRenderer: ItemTreeRenderer,
    private val itemResolver: Resolver,

    ) {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)
    val mapper = jacksonObjectMapper()


    @Scheduled(
        initialDelayString = "\${$RENDERING.$INITIAL_DELAY:0}",
        fixedDelayString = "\${$RENDERING.$FIXED_DELAY:60}",
        timeUnit = TimeUnit.SECONDS
    )
    fun write() {
        logger.debug("Checking for stale items that need to be rendered")

        // Retrieve these every run but cache for the duration.
        // Note for tests: Between runs a given item identifiers may resolve to a different item pk.
        // Do don't cache these for longer than a schedule run.
        var strategy = ItemFetchStrategy.fromPerspective(
            ItemFetchStrategy.DEFAULT,
            authorityRegistry.getAuthorityRootPks(itemResolver),
            authorityRegistry.getAuthorityPks(itemResolver),
            Consts.DEFAULT_TREE_DEPTH,
        )

        try {
        var halt = false
        while (!halt) {
            renderStatusDao.getFromQueue(batchSize) { itemPksToRender ->

                // We may get a chunk with duplicates from the queue.
                val distinctItemPks = itemPksToRender.toSet()

                /**
                 * We'll get a batch of ItemPks, which were put on the queue by StaleItemSqsWriter.
                 *
                 * Any kind of Item that was mentioned in a Property or Relationship assertion might
                 * be put on this queue. Only those that are Collection items will have renderers, so
                 * we can ignore non-Collection items when choosing renderers.
                 */
                val (collectionItems, nonCollectionItems) = collections.partitionItemPks(distinctItemPks)

                logger.info("Processing stale items: $itemPksToRender from render queue. Of which Collection Items: $collectionItems and non-Collection: $nonCollectionItems")

                // Only render Collection items.
                for (itemPk in collectionItems) {
                    try {
                        itemTreeRenderer.renderAll(itemPk, strategy)
                    } catch (e: Exception) {
                        // There could be errors caused by drift between database and queue.
                        // Report errors but don't mark the message as having failed.
                        logger.error("Failed to process $itemPk: ${e.message}")
                    }
                }

                /**
                 * All stale Items (Collection or non-Collection) may be part of the Item Trees for *other* Collection
                 * items. Based on their Item Tree reachability indexes, set those stale.
                 *
                 * Don't render them immediately, as that might lead to parallel processors duplicating work. Instead, send
                 * into the queue. They will be picked up in a scan of StaleItemSqsWriter.
                 */
                renderStatusDao.triggerStaleFromIndex(distinctItemPks)
            }

            halt = renderStatusDao.queueLength() == 0L
        }
    } catch (e: Exception) {
        logger.error("Error rendering: $e", e)
        }
    }

}
