package org.crossref.manifold.rendering

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.tags.Tag
import org.crossref.manifold.api.Configuration.API
import org.crossref.manifold.api.Configuration.BASE_URL
import org.crossref.manifold.api.Entry
import org.crossref.manifold.api.Feed
import org.crossref.manifold.api.Link
import org.crossref.manifold.api.item.RenderedItemsRequest
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.util.ApiParameter
import org.crossref.manifold.rendering.Configuration.ITEMS_API
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.Configuration.RENDER_S3_BUCKET
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import org.springframework.web.util.UriComponentsBuilder
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

const val ATOM_MIME_TYPE = "application/atom+xml"

/**
 * REST API for metadata versions
 */
@Tag(name = "Items")
@RestController
@ConditionalOnProperty(prefix = RENDERING, name = [ITEMS_API])
class ItemsController(
    val itemResolver: Resolver,
    val renderedItemDao: RenderedItemDao,
    val renderedItemsService: RenderedItemsService,
    val renderedItemStorageDao: RenderedItemStorageDao,
    @Value("\${$API.$BASE_URL:}") val baseUrl: String,
    @Value("\${$RENDERING.$RENDER_S3_BUCKET}") val bucket: String,
) {

    val mapper: ObjectMapper = XmlMapper().configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true)

    fun getLinkBuilder(
        path: String,
        contentType: String? = null,
        version: String? = null,
    ): UriComponentsBuilder {
        val rootUrl = if (baseUrl.isEmpty()) ServletUriComponentsBuilder.fromCurrentContextPath().build() else baseUrl
        val builder = ServletUriComponentsBuilder.fromUriString("$rootUrl/v1/$path")

        if (contentType != null) {
            // Content types have "+" which needs encoding.
            builder.queryParam("content-type", URLEncoder.encode(contentType, StandardCharsets.UTF_8))
        }

        if (version != null) {
            builder.queryParam("version", version)
        }

        return builder
    }

    fun getItemsLink(
        identifier: String?,
        contentType: String? = null,
        version: String? = null,
    ): String {
        val builder = getLinkBuilder("items", contentType, version)
        if (identifier != null) {
            builder.pathSegment(identifier)
        }
        return builder.build().toString()
    }

    fun getFeedLink(
        identifier: String?,
        contentType: String? = null,
    ): String {
        val builder = getLinkBuilder("items/feed.xml", contentType)
        if (identifier != null) {
            builder.queryParam("item", identifier)
        }

        return builder.build().toString()
    }

    fun getFeedNextLink(
        identifier: String? = null,
        contentType: String? = null,
        rows: Int?,
        cursor: Long?,
        fromDate: String?,
        untilDate: String?,
    ): String {
        val builder = getLinkBuilder("items/feed.xml", contentType)
        if (identifier != null) {
            builder.queryParam("item", identifier)
        }

        if (cursor != null) {
            builder.queryParam("cursor", cursor)
        }

        if (rows != null) {
            builder.queryParam("rows", rows)
        }

        if (fromDate != null) {
            builder.queryParam("fromDate", fromDate)
        }

        if (untilDate != null) {
            builder.queryParam("untilDate", untilDate)
        }

        return builder.build().toString()
    }

    fun respondWithItem(identifier: String, f: ((item: Item) -> RenderedItem)): ResponseEntity<String> {
        val resolved: Item = identifier.toLongOrNull()?.let {
            itemResolver.resolveRO(Item(pk = identifier.toLong()))
        } ?: itemResolver.resolveRO(Identifier.new(identifier))

        if (resolved.pk == null) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't find item - $identifier")
        }

        val item = f(resolved)

        val storedItem = renderedItemStorageDao.fetchContent(item)

        val storedContentType = storedItem.contentType.mimeType
        val feedUrl = getFeedLink(identifier, storedItem.contentType.mimeType)
        val headers = HttpHeaders()

        headers.add("ETag", storedItem.hash)
        headers.add("Content-Type", storedContentType)
        headers.add("Link", "<$feedUrl>, rel=\"feed\"")

        return ResponseEntity.ok()
            .lastModified(storedItem.updatedAt?.toInstant()!!)
            .headers(headers)
            .body(storedItem.content)
    }

    @Operation(
        summary = """
            Retrieves the current metadata for a given Item, using its PK or its URL identifier. 
            This endpoints always servers the latest version of the based on render time and weighted
            content type.
            """,
        parameters = [
            Parameter(
                name = "identifier",
                description = "The identifier of the Item, either its URL identifier, or its PK",
                examples = [
                    ExampleObject(
                        value = "https://doi.org/10.5555/12345678",
                        summary = "DOI",
                        name = "",
                    ),
                ],
            ),
            Parameter(
                name = "version",
                description = "The version of the item to retrieve in epoch milliseconds",
                examples = [
                    ExampleObject(
                        value = "1683210687108",
                        summary = "Version",
                        name = "",
                    ),
                ],
            ),
            Parameter(
                name = "content-type",
                description = "The content-type of the item to retrieve",
                examples = [
                    ExampleObject(
                        value = "application/citeproc+json",
                        summary = "Content Type",
                        name = "",
                    ),
                ],
            ),
        ],
    )
    @GetMapping("/v1/items/{*identifier}")
    fun getMetadata(
        @PathVariable
        identifier: String,
        @RequestParam(value = "version", required = false)
        version: Long?,
        @RequestParam(value = "content-type", required = false)
        contentType: String?,
    ): ResponseEntity<String> {
        val noArgs = version == null && contentType == null
        val missingArguments =
            (contentType.isNullOrEmpty() && version != null) || (!contentType.isNullOrEmpty() && version == null)

        if (missingArguments) {
            throw ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "version and content-type arguments work together, when one is supplied, the other is required too.",
            )
        }

        // Need to trim the leading slash.
        val trimmedIdentifier = identifier.substring(1)

        return respondWithItem(trimmedIdentifier) { resolved ->
            if (noArgs) {
                val items = renderedItemDao.getRenderedItems(resolved.pk!!).filterNotNull()
                if (items.isEmpty()) throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't retrieve data for that item.")
                items.sortedBy { i -> ContentType.weight(i.contentType) }.first()
            } else {
                val resolvedContentType = ContentType.fromMimeType(contentType!!)
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect content type specified - $contentType")
                renderedItemDao.getRenderedItemVersion(resolved.pk!!, resolvedContentType, version!!)
                    ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find item for version - $version with content-type $contentType")
            }
        }
    }

    @GetMapping("/v1/items/feed.xml")
    fun getFeed(
        @ApiParameter
        itemsRequest: RenderedItemsRequest,
    ): ResponseEntity<String> {
        val headers = HttpHeaders()
        headers.add("Content-Type", ATOM_MIME_TYPE)

        val contentType = itemsRequest.contentType
        val resolvedContentType = itemsRequest.resolvedContentType
        val item = itemsRequest.item

        val renders = renderedItemsService.getItems(itemsRequest)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't find item - $item")

        if (!contentType.isNullOrBlank() && resolvedContentType == null) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect content type specified - $contentType")
        }

        val feedLink = getFeedLink(
            item,
            contentType,
        )

        val links = mutableListOf(
            Link(
                href = feedLink,
                rel = "self",
                type = ATOM_MIME_TYPE,
            ),
            Link(
                href = feedLink,
                rel = "first",
                type = ATOM_MIME_TYPE,
            ),
        )

        if (renders.any()) {
            links.add(
                Link(
                    href = getFeedNextLink(
                        item,
                        rows = itemsRequest.maxRows,
                        cursor = renders.lastOrNull()?.pk,
                        fromDate = itemsRequest.fromDate,
                        untilDate = itemsRequest.untilDate,
                    ),
                    rel = "next",
                    type = ATOM_MIME_TYPE,
                ),
            )
        }

        val feed = Feed(
            id = feedLink,
            title = "Crossref Metadata Feed",
            links = links,
            entries = renders.map { r ->
                Entry(
                    title = "Item ${r.itemPk} in ${r.contentType.mimeType}",
                    id = getItemsLink(r.itemPk.toString(), r.contentType.mimeType, r.version.toString()),
                    updated = r.updatedAt.toString(),
                    links = listOf(
                        Link(
                            href = getItemsLink(r.itemPk.toString()),
                            rel = "alternate",
                            type = r.contentType.mimeType,
                        ),
                        Link(
                            href = getItemsLink(r.itemPk.toString(), r.contentType.mimeType, r.version.toString()),
                            rel = "related",
                            type = r.contentType.mimeType,
                        ),
                        Link(
                            href = "s3://$bucket/${r.pointer}",
                            rel = "enclosure",
                            type = r.contentType.mimeType,
                        ),
                    ),
                )
            },
            updated = renders.firstOrNull()?.updatedAt.toString(),
        )

        val xml = mapper.writeValueAsString(feed)

        return ResponseEntity.ok()
            .headers(headers)
            .body(xml)
    }
}
