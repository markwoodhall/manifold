package org.crossref.manifold.rendering

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.sql.Types
import java.time.OffsetDateTime
import java.util.StringJoiner

@Repository
class RenderedItemDao(
    jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    private val renderedItemMapper: RowMapper<RenderedItem> = RowMapper { rs, _ ->
        RenderedItem(
            pk = rs.getLong("pk"),
            itemPk = rs.getLong("item_pk"),
            contentType = ContentType.fromMimeType(rs.getString("content_type"))!!,
            current = rs.getBoolean("current"),
            pointer = rs.getString("pointer"),
            hash = rs.getString("hash"),
            updatedAt = rs.getObject("updated_at", OffsetDateTime::class.java),
            version = rs.getLong("version"),
        )
    }

    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Transactional
    fun createRenderedItems(
        renderedItems: Collection<RenderedItem>,
    ) {
        val props = renderedItems.map { renderedItem ->
            MapSqlParameterSource(
                mapOf(
                    "item_pk" to renderedItem.itemPk,
                    "current" to renderedItem.current,
                    "content_type" to renderedItem.contentType.mimeType,
                    "pointer" to renderedItem.pointer,
                    "hash" to renderedItem.hash,
                    "updated_at" to renderedItem.updatedAt,
                    "version" to renderedItem.version,
                ),
            ).apply { registerSqlType("updated_at", Types.TIMESTAMP_WITH_TIMEZONE) }
        }.toTypedArray()

        npTemplate.batchUpdate(
            """
            UPDATE rendered_item 
            SET current = false
            WHERE item_pk = :item_pk and content_type = :content_type
            """,
            props,
        )

        npTemplate.batchUpdate(
            """
            INSERT INTO rendered_item ( item_pk, content_type, pointer, hash, current, updated_at, version )
            VALUES ( :item_pk, :content_type, :pointer, :hash, :current, :updated_at, :version );
            """,
            props,
        )
    }

    fun getRenderedItem(itemPk: Long, contentType: ContentType): RenderedItem? {
        val renderedItems = npTemplate.query(
            """
            select pk, item_pk, content_type, pointer, hash, current, updated_at, version
            from rendered_item 
            where item_pk = :item_pk and content_type = :content_type and current = true
            """,
            mapOf(
                "item_pk" to itemPk,
                "content_type" to contentType.mimeType,
            ),
            renderedItemMapper,
        )

        return renderedItems.firstOrNull()
    }

    fun getRenderedItemVersion(itemPk: Long, contentType: ContentType, version: Long): RenderedItem? {
        val renderedItems = npTemplate.query(
            """
            select pk, item_pk, content_type, pointer, hash, current, updated_at, version
            from rendered_item 
            where item_pk = :item_pk and content_type = :content_type and version = :version
            """,
            mapOf(
                "item_pk" to itemPk,
                "content_type" to contentType.mimeType,
                "version" to version,
            ),
            renderedItemMapper,
        )

        return renderedItems.firstOrNull()
    }

    /**
     * Gets all current rendered items for @param[itemPk], where current
     * means the latest version, there should only be one per content type
     */
    fun getRenderedItems(itemPk: Long): Collection<RenderedItem?> {
        return npTemplate.query(
            """
            select pk, item_pk, content_type, pointer, hash, current, updated_at, version
            from rendered_item
            where item_pk = :item_pk and current = true
            """,
            mapOf(
                "item_pk" to itemPk,
            ),
            renderedItemMapper,
        )
    }

    /**
     * Gets all rendered items (current or historical) for @param[itemPk], where current
     * means the latest version, there should only be one per content type
     */
    fun getHistoricalRenderedItems(itemPk: Long, contentType: String): Collection<RenderedItem> {
        return npTemplate.query(
            """
            select pk, item_pk, content_type, pointer, hash, current, updated_at, version
            from rendered_item
            where item_pk = :item_pk and content_type = :content_type
            """,
            mapOf(
                "item_pk" to itemPk,
                "content_type" to contentType
            ),
            renderedItemMapper,
        )
    }


    /**
     * Gets the render history of @param[itemPk] by optional @param[contentType] and ordered
     * by the renders last updated date. 
     */
    fun getRenderedItemHistory(
        itemPk: Long,
        contentType: ContentType? = null,
    ): Collection<RenderedItem> {
        return queryRenderedItems(
            RenderedItemQuery(
                itemPk = itemPk,
                contentType = contentType,
            ),
        )
    }

    /**
     * Gets rendered items by @param[query]
     */
    fun queryRenderedItems(query: RenderedItemQuery): Collection<RenderedItem> {
        val sql = StringBuilder()
        sql.append(
            """
            select pk, item_pk, content_type, pointer, hash, current, updated_at, version
            from rendered_item 
            """,
        )

        val where = StringJoiner(" AND ", " WHERE ", "").setEmptyValue("")

        query.itemPk?.let {
            where.add("item_pk = :item_pk")
        }

        query.contentType?.let {
            where.add("content_type = :content_type")
        }

        query.cursor?.let {
            where.add("pk < :cursor ")
        }

        query.fromDateTime?.let {
            where.add("updated_at >= :from_date")
        }

        query.untilDateTime?.let {
            where.add("updated_at <= :until_date")
        }

        sql.append(where)
        sql.append(" order by pk desc limit :size")

        logger.debug(sql.toString())

        return npTemplate.query(
            sql.toString(),
            mapOf(
                "content_type" to query.contentType?.mimeType,
                "size" to query.size,
                "cursor" to query.cursor,
                "from_date" to query.fromDateTime,
                "until_date" to query.untilDateTime,
                "item_pk" to query.itemPk,
            ),
            renderedItemMapper,
        )
    }
}
