package org.crossref.manifold.rendering

object Configuration {
    const val RENDERING = "rendering"
    const val RENDERER_ENABLED = "renderer-enabled"
    const val CITEPROC_ENABLED = "citeproc-enabled"
    const val ORGANIZATIONS_ENABLED = "organizations-enabled"
    const val MEMBERS_ENABLED = "members-enabled"
    const val WORKS_API = "works-api"
    const val ITEMS_API = "items-api"
    const val MEMBERS_API = "members-api"
    const val RENDER_QUEUE_PROCESSOR_ENABLED = "render-queue-processor-enabled"
    const val RENDER_S3_BUCKET = "render-s3-bucket"
}
