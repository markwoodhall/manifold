package org.crossref.manifold.rendering

import org.apache.commons.codec.digest.DigestUtils
import org.crossref.manifold.itemgraph.Collections
import org.crossref.manifold.itemgraph.Consts.DEFAULT_TREE_DEPTH
import org.crossref.manifold.itemgraph.ReachabilityDao
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.rendering.Configuration.RENDERER_ENABLED
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.retrieval.itemtree.ItemFetchConfiguration
import org.crossref.manifold.retrieval.itemtree.ItemTreeRetriever
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

@Component
@ConditionalOnProperty(prefix = RENDERING, name = [RENDERER_ENABLED])
class ItemTreeRenderer(
    private val itemTreeRetriever: ItemTreeRetriever,
    private val renderStatusDao: RenderStatusDao,
    private val renderedItemDao: RenderedItemDao,
    private val renderedItemStorageDao: RenderedItemStorageDao,
    private val renderers: Collection<ContentTypeRenderer>,
    private val collectionsService: Collections,
    private val reachabilityDao: ReachabilityDao
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * For a set of collections, return the set of renderers that produce all the content types for those collections.
     */
    fun getRenderersForCollections(collections: Set<String>) : Collection<ContentTypeRenderer> =
        renderers.filter {
            collections.contains(it.collection())
        }

    fun getRendererForContentType(contentType: ContentType) : ContentTypeRenderer? =
        renderers.firstOrNull { it.internalContentType() == contentType }

    /**
     * Render an Item Tree with the given ContentTypeRenderer and store it.
     * If there was no change, don't re-store.
     */
    fun render(itemTree: Item?, renderer: ContentTypeRenderer) {
        checkNotNull(itemTree) { "Expected $itemTree to be non null" }
        checkNotNull(itemTree.pk) { "Expected $itemTree to have a pk" }

        logger.info("Rendering ${itemTree.pk} with ${renderer.internalContentType().toString()}")

        val beanContentType = renderer.internalContentType()
        val updatedAt = OffsetDateTime.now()
        val millis = updatedAt.toInstant().toEpochMilli()
        val pointer = "render/item/${itemTree.pk}/${beanContentType.mimeType}/$millis"

        val content = renderer.render(itemTree)

        // Some renderers optionally produce content, so check here if anything was
        // rendered before doing anything else
        if (content != null) {
            val hash = DigestUtils.md5Hex(content.toByteArray())
            val existing = renderedItemDao.getRenderedItem(itemTree.pk, beanContentType)

            // If this is rendered or the first time, or it changed
            if (existing == null || existing.hash != hash) {
                val renderedItem = RenderedItem(
                    itemPk = itemTree.pk,
                    contentType = beanContentType,
                    pointer = pointer,
                    current = true,
                    updatedAt = updatedAt,
                    content = content,
                    hash = hash,
                )
                renderedItemStorageDao.storeItem(renderedItem)
                renderedItemDao.createRenderedItems(listOf(renderedItem))
            }
        }
    }

    /**
     * Retrieves the @param[itemPk] using the supplied fetch configuration and renders
     * it as all available content types based on the items collections.
     */
    fun renderAll(itemPk: Long, fetchConfig: ItemFetchConfiguration) {

        val itemTree = itemTreeRetriever.get(itemPk, fetchConfig)

        if (itemTree?.pk != null) {
            // The collections that this item is in determine what renderers.
            val collections = collectionsService.getCollectionsUrisForItemPk(itemPk)
            val renderers = getRenderersForCollections(collections)

            renderers.forEach {
                render(itemTree, it)
            }

            // If an item is in one or more collections, it is a candidate for being rendered.
            // If it was marked as stale, the structure of the tree may have changed (e.g. extended by a new relationship).
            // So re-generate that tree reachability index.
            if (collections.isNotEmpty()) {
                reachabilityDao.updateTreeReachability(listOf(itemPk), DEFAULT_TREE_DEPTH)
            }
        } else {
            logger.error("Failed to find item tree from Item PK $itemPk")
        }
    }

    /**
     * Renders the @param[itemTree] as @param[contentType]
     * supplied
     */
    fun renderContentType(itemTree: Item?, contentType: ContentType) {
        val renderer = getRendererForContentType(contentType)
        if (renderer != null) {
            render(itemTree, renderer)
        } else {
            logger.error("Can't find renderer for content type $contentType")
        }
    }

    /**
     * Gets the RenderedItem for @param[itemTree] based on the @param[contentType].
     *
     * @return if the RenderedItem already exists in storage then it will be loaded,
     * if not it will be rendered and then loaded
     */
    fun getRenderedItemTree(itemTree: Item, contentType: ContentType): RenderedItem? {
        checkNotNull(itemTree.pk) { "Expected $itemTree to have a pk" }

        val rendered = renderedItemStorageDao.getItem(itemTree.pk, contentType)
            ?.let {
                renderContentType(itemTree, contentType)
                renderedItemStorageDao.getItem(itemTree.pk, contentType)
            }
        return rendered
    }
}
