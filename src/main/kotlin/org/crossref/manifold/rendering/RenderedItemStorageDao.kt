package org.crossref.manifold.rendering

import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.Configuration.RENDER_S3_BUCKET
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Repository
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.GetObjectRequest
import software.amazon.awssdk.services.s3.model.PutObjectRequest

@Repository
@ConditionalOnProperty(prefix = RENDERING, name = [RENDER_S3_BUCKET])
class RenderedItemStorageDao(
    private val s3Client: S3Client,
    @Value("\${$RENDERING.$RENDER_S3_BUCKET}") val bucket: String,
    private val renderedItemDao: RenderedItemDao,
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    init {
        if (bucket.isBlank()) {
            throw IllegalArgumentException(
                """
                RenderedItemStorageDao expects to be able to save rendered items to S3 but no bucket was specified,
                either directly or via $RENDERING.$RENDER_S3_BUCKET.
                """,
            )
        }
    }

    fun storeItem(renderedItem: RenderedItem) {
        logger.info("Uploading rendered item ${renderedItem.pointer} to $bucket")
        s3Client.putObject(
            PutObjectRequest.builder()
                .bucket(bucket)
                .key(renderedItem.pointer)
                .contentType(renderedItem.contentType.mimeType)
                .build(),
            RequestBody.fromString(renderedItem.content),
        )
    }

    /**
     * @return the RenderedItem @param[item] with its
     * content populated
     */
    fun fetchContent(item: RenderedItem): RenderedItem {
        val pointer = item.pointer
        val s3Object = s3Client.getObject(
            GetObjectRequest.builder()
                .bucket(bucket)
                .key(pointer)
                .build(),
        )

        item.content = s3Object.buffered().reader().readText()
        return item
    }

    fun getItem(itemPk: Long, contentType: ContentType): RenderedItem? {
        val rendered = renderedItemDao.getRenderedItem(itemPk, contentType)
        val pointer = rendered?.pointer ?: return null

        val s3Object = s3Client.getObject(
            GetObjectRequest.builder()
                .bucket(bucket)
                .key(pointer)
                .build(),
        )

        rendered.content = s3Object.buffered().reader().readText()
        return rendered
    }
}
