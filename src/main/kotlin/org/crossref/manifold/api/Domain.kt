/**
 * Data structures for the API.
 */
package org.crossref.manifold.api

import org.crossref.manifold.views.PropertyStatementWithProvenance
import org.crossref.manifold.views.RelationshipStatementWithProvenance
import java.time.OffsetDateTime

/**
 * Container for all API responses.
 */
data class ResponseEnvelope<K>(
    val status: String,
    val messageType: String,
    val messageVersion: String = "1.0.0",
    val message: K
)


/**
 * List of Relationship types.
 */
data class RelationshipTypesView(
    val items: List<String>
)

data class RelationshipView(
    val subject: ItemView,
    val `object`: ItemView,
    val relationshipType: String,
    val createdTime: OffsetDateTime,
    val updatedTime: OffsetDateTime
)

data class ItemView(
    val identifiers: List<IdentifierView>,
)

data class IdentifierView(
    val identifierType: String,
    val identifier: String
)

data class RelationshipStatementsWithProvenanceView(
    val relationshipStatementsWithProvenance: List<RelationshipStatementWithProvenance>,
    val nextCursor: Long?
)


data class PropertyStatementsWithProvenanceView(
    val propertyStatementsWithProvenance: List<PropertyStatementWithProvenance>,
    val nextCursor: Long?
)
