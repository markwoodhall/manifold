package org.crossref.manifold.api

object Configuration {
    const val API = "api"
    const val BASE_URL = "base-url"
}
