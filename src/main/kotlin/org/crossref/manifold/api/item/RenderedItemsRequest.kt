package org.crossref.manifold.api.item

import org.crossref.manifold.rendering.ContentType
import java.time.LocalDate
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneOffset
import kotlin.math.min

const val MAX_ROW_SIZE = 100

class RenderedItemsRequest(
    val cursor: Long?,
    val rows: Int?,
    val maxRows: Int = min(rows ?: MAX_ROW_SIZE, MAX_ROW_SIZE),
    val item: String?,
    val contentType: String?,
    val resolvedContentType: ContentType? = ContentType.fromMimeType(contentType.toString()),
    val fromDate: String?,
    val untilDate: String?,
    val fromDateTime: OffsetDateTime? = fromDate?.let {
        LocalDate.parse(fromDate)
            .atStartOfDay(ZoneOffset.UTC)
            .toOffsetDateTime()
    },
    val untilDateTime: OffsetDateTime? = untilDate?.let {
        LocalDate.parse(untilDate)
            .atTime(LocalTime.parse("23:59:59"))
            .atZone(ZoneOffset.UTC)
            .toOffsetDateTime()
    },
)
