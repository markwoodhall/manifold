package org.crossref.manifold.api.relationship

import org.crossref.manifold.api.ApiMessageType
import org.crossref.manifold.api.ApiResponse
import org.springframework.http.HttpStatus

class RelationshipsResponse(
    message: RelationshipsMessage
) : ApiResponse<RelationshipsMessage>(
    HttpStatus.OK,
    ApiMessageType.RELATIONSHIP_LIST,
    "1.0.0",
    message
)
