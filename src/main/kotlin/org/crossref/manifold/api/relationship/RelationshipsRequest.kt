package org.crossref.manifold.api.relationship

import org.crossref.manifold.api.ApiRequest
import java.time.OffsetDateTime

class RelationshipsRequest(
    cursor: String?,
    rows: Int?,
    val itemId: String?,
    val subjectId: String?,
    val objectId: String?,
    val relationshipType: String?,
    val fromCreatedTime: OffsetDateTime?,
    val untilCreatedTime: OffsetDateTime?,
    val fromUpdatedTime: OffsetDateTime?,
    val untilUpdatedTime: OffsetDateTime?,
) : ApiRequest(cursor, rows)
