package org.crossref.manifold.relationship

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.tags.Tag
import org.crossref.manifold.api.relationship.RelationshipsRequest
import org.crossref.manifold.api.relationship.RelationshipsResponse
import org.crossref.manifold.util.ApiParameter
import org.crossref.manifold.relationship.Constants.API
import org.crossref.manifold.relationship.Constants.RELATIONSHIPS
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Tag(name = "Relationships")
@RestController
@RequestMapping("/v1/relationships")
@ConditionalOnProperty(prefix = RELATIONSHIPS, name = [API])
class RelationshipController(private val relationshipService: RelationshipService) {
    @Operation(
        summary = "Retrieve the set of asserted relationships that match the given criteria",
        parameters = [Parameter(
            name = "subject-id",
            description = "An identifier for the subject of the relationships. " +
                    "If none is provided then all identifiers are searched."
        ), Parameter(
            name = "object-id",
            description = "An identifier for the object of the relationships. " +
                    "If none is provided then all identifiers are searched."
        ), Parameter(
            name = "item-id",
            description = "Equivalent to subject-id OR object-id."
        ), Parameter(
            name = "relationship-type",
            description = "The type of relationship to be searched. " +
                    "If none is provided then all relationships are searched."
        ), Parameter(
            name = "cursor",
            description = "Positional marker that allows large result sets to be scrolled a page at a time. " +
                    "If none is provided then the first page of results is returned with no cursor pointing to the next page.",
            examples = [ExampleObject(
                value = "*",
                summary = "First page",
                name = "Return the first page of results from a new query and a cursor pointing to the next page.",
            )]
        ), Parameter(
            name = "rows",
            description = "The number of rows to be returned in the next page. Max value is 1000 (default). Min value is 0."
        )]
    )
    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getRelationships(
        @Parameter(hidden = true)
        @ApiParameter
        relationshipsRequest: RelationshipsRequest,
    ): RelationshipsResponse =
        RelationshipsResponse(relationshipService.getRelationships(relationshipsRequest))
}
