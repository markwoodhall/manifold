package org.crossref.manifold.relationship

import org.crossref.manifold.api.IdentifierView
import org.crossref.manifold.api.ItemView
import org.crossref.manifold.api.RelationshipView
import org.crossref.manifold.api.relationship.RelationshipsMessage
import org.crossref.manifold.api.relationship.RelationshipsRequest
import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.itemgraph.RelationshipDao
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.relationship.Constants.API
import org.crossref.manifold.relationship.Constants.RELATIONSHIPS
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service
import java.time.OffsetDateTime

@Service
@ConditionalOnProperty(prefix = RELATIONSHIPS, name = [API])
class RelationshipService(
    private val resolver: Resolver,
    private val itemDao: ItemDao,
    private val relationshipDao: RelationshipDao,
) {
    fun getRelationships(relationshipsRequest: RelationshipsRequest): RelationshipsMessage {
        var resolvedItemPk: Long? = null
        if (relationshipsRequest.itemId != null) {
            resolvedItemPk = resolver.resolveRO(Identifier.new(relationshipsRequest.itemId)).pk
            if (resolvedItemPk == null) {
                return RelationshipsMessage()
            }
        }
        var resolvedSubjectPk: Long? = null
        if (relationshipsRequest.subjectId != null) {
            resolvedSubjectPk = resolver.resolveRO(Identifier.new(relationshipsRequest.subjectId)).pk
            if (resolvedSubjectPk == null) {
                return RelationshipsMessage()
            }
        }
        var resolvedObjectPk: Long? = null
        if (relationshipsRequest.objectId != null) {
            resolvedObjectPk = resolver.resolveRO(Identifier.new(relationshipsRequest.objectId)).pk
            if (resolvedObjectPk == null) {
                return RelationshipsMessage()
            }
        }
        val fetchedRelationships =
            relationshipDao.fetchRelationships(
                relationshipsRequest.cursor,
                resolvedItemPk,
                resolvedSubjectPk,
                resolvedObjectPk,
                relationshipsRequest.relationshipType,
                relationshipsRequest.fromCreatedTime,
                relationshipsRequest.untilCreatedTime,
                relationshipsRequest.fromUpdatedTime,
                relationshipsRequest.untilUpdatedTime,
                relationshipsRequest.rows
            )
        val itemsByItemPk: Map<Long, ItemView> =
            itemDao.fetchItemIdentifierPairsForItemPks(fetchedRelationships.rows.flatMap {
                setOf(
                    it.fromItemPk,
                    it.toItemPk
                )
            }.toSet())
                .groupBy({ it.first },
                    { IdentifierView(it.second.getHostDisplay(), it.second.uri.toString()) })
                .mapValues {
                    ItemView(
                        it.value
                    )
                }
        return RelationshipsMessage(
            fetchedRelationships.nextCursor,
            fetchedRelationships.totalRows,
            fetchedRelationships.fetchSize,
            fetchedRelationships.rows.map {
                RelationshipView(
                    itemsByItemPk[it.fromItemPk] as ItemView,
                    itemsByItemPk[it.toItemPk] as ItemView,
                    it.relationshipType,
                    it.firstLinkedAt ?: it.firstIngestedAt,
                    maxOf(it.lastIngestedAt, it.lastLinkedAt ?: OffsetDateTime.MIN)
                )
            })
    }
}
