package org.crossref.manifold.itemtree

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import java.net.MalformedURLException
import java.net.URI
import java.net.URISyntaxException
import java.net.URL

/**
 * We may be supplied with URIs with encoded or un-encoded parts.
 * Whilst some encodings are compulsory (reserved characters) some are not.
 * Java URI comparison is based on the string in the constructor, which means that two otherwise identical URIs, but
 * expressed with different encodings, would not be considered equal.
 *
 * We store the un-encoded string in the database, so when they are retrieved they are always minimally encoded.
 *
 * Construct this URI so that it's represented in the in-memory item tree this way too.
 *
 * If we didn't, we'd be unable to use the mappings in item tree mapper.
 */
fun recodeUri(input: URI) : URI =
    URI(input.scheme, null, input.host, input.port, input.path, input.query, input.fragment)


/**
 * If the string looks like a DOI, construct that URI.
 * Because a DOI can contain arbitrary printable unicode characters, including those 'reserved' in the URI specification,
 * and because we may find a DOI expressed as a URL-like string, we'll first try to parse as a URI. But failing that,
 * we'll take the string and construct a URI with it as the path.
 */
fun getDoi(input: String) : URI? {
    val doi = "https://doi.org/"

    return if (input.startsWith(doi)) {
        val lowercase = input.lowercase()

        try {
            recodeUri(URI(lowercase))
        } catch (_: URISyntaxException) {
            // if it fails don't treat as a URL, just take the whole string as the path
            // a URL might try to destructure e.g. question mark and # which is significant
            val suffix = lowercase.substring(doi.length - 1)
            recodeUri(URI("https", null, "doi.org", -1, suffix, null, null))
        }
    } else null
}

/**
 * Parse a found URI. If it can't be represented as a URI then try it as a URL. Return null if we really can't parse it.
 * See note on [recodeUri].
 */
fun tolerantParse(input: String) : Identifier? {
    try {
        return Identifier(uri= recodeUri(URI(input)))
    } catch (_: URISyntaxException) {
        // It may fail because it has un-encoded parts. The URL parser is more tolerant, to use that to parse the input.
        try {
            val url = URL(input)
            return Identifier(uri = recodeUri(URI(url.protocol, url.host, url.path, url.query, url.ref)))
        } catch (malformedUrl: Exception) {
            // If there's no protocol then the URL won't parse. Pass.
        }
    }

    return null
}

/**
 * An Item Identifier within an Item Tree.
 * @param uri URL of the identifier.
 * @param pk primary key of the ItemIdentifier (not its corresponding Item!) in the Item Graph. This is null until it's resolved.
 */
@JsonDeserialize(using = IdentifierDeserializer::class)
data class Identifier(

    @JsonProperty("uri")
    @get:JsonProperty("uri")
    val uri : URI,

    val persistent: Boolean = false,

    @JsonIgnore
    val pk: Long? = null
) {
    companion object {
        /**
         * Parse a URI-like string into a URI, with heuristics.
         */
        fun new(input: String) : Identifier {
            val doi = getDoi(input)
            if (doi != null) {
                return Identifier(uri=recodeUri(doi), persistent=true)
            }

            val tolerant = tolerantParse(input)
            if (tolerant != null) {
                return tolerant
            }

            return Identifier(uri=URI("https", null, "id.crossref.org", -1, "/unknown/$input", null, null ))
        }

        /**
         * If 'persistent' is supplied, override.
         */
        fun new(input: String, persistent: Boolean) : Identifier {
            return new(input).withPersistent(persistent)
        }

        fun new(input: String, persistent: Boolean, pk: Long) : Identifier {
            return new(input).withPersistent(persistent).withPk(pk)
        }
    }

    @JsonIgnore
    fun getScheme(): String? = uri.scheme

    @JsonIgnore
    fun getHost(): String? = uri.host

    /**
     * Get the host for display purposes.
     * Return "none" on those rare occasions where there isn't one.
     */
    @JsonIgnore
    fun getHostDisplay(): String = uri.host ?: "none"

    /** Default -1 if not supplied.
     */
    @JsonIgnore
    fun getPort(): Int = uri.port

    @JsonIgnore
    fun getPath(): String? = uri.path

    @JsonIgnore
    fun getQuery(): String? = uri.query

    @JsonIgnore
    fun getFragment(): String? = uri.fragment

    /**
     * Return a copy of this object with the given PK.
     */
    fun withPk(newPk: Long) = Identifier(uri, persistent, newPk)
    fun withPersistent(persistent: Boolean): Identifier = Identifier(uri, persistent, pk)
}

