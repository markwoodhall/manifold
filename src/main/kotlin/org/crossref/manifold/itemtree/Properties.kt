package org.crossref.manifold.itemtree

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode

/** Set of properties for the Item asserted by a party.
 */
data class Properties(
    val values: ObjectNode,

    /**
     * Multiple parties making this assertion.
     * Allow this to be constructed with a default asserted by no-one.
     */
    val assertedBy: List<Item> = emptyList(),
) {

    /**
     * Return new Properties object with the values of the argument merged in.
     * New properties replace old ones key-wise.
     */
    fun merge(newProps: Properties): Properties {
        val copy = values.deepCopy()
        for ((k, v) in newProps.values.fields()) {
            copy.replace(k, v)
        }

        return Properties(copy, assertedBy)
    }

    companion object
}

fun jsonObjectFromMap(input: Map<String, Any?>): ObjectNode {
    val mapper = ObjectMapper()
    return mapper.valueToTree(input)
}

/** Convert a simple Map to a list of Properties, one per key.
 */
fun propertiesFromSimpleMap(input: Map<String, Any?>): Properties {
    return Properties(jsonObjectFromMap(input))
}
