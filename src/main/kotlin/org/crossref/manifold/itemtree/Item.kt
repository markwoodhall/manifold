package org.crossref.manifold.itemtree

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty


data class Item(
    /**
     * Identifiers by which this Item is known. There can be zero, one or more Item Identifiers. Ordered.
     * In some cases the order may be useful, e.g. sorting Identifiers by preference.
     */
    @JsonProperty("identifiers")
    @get:JsonProperty("identifiers")
    val identifiers: List<Identifier> = emptyList(),

    @JsonProperty("blank")
    @get:JsonProperty("blank")
    val blank: Boolean = false,

    /**
     * Relationships to other Items. This is ordered, giving users the ability to consider order meaningful or not.
     * In some cases the order may be useful.
     */
    @JsonProperty("relationships")
    @get:JsonProperty("relationships")
    val rels: List<Relationship> = emptyList(),

    /**
     * A map of properties attached to this Item, e.g. title, along with the asserting party for each.
     */
    @JsonProperty("properties")
    @get:JsonProperty("properties")
    val properties: List<Properties> = emptyList(),

    /**
     * Primary Identifier for this Item. May be null when not known.
     */
    val pk: Long? = null,
) {
    companion object


    /**
     * Copy constructor to add new ID.
     */
    fun withPk(newPk: Long) = Item(identifiers, blank, rels, properties, newPk)
    fun withIdentifiers(newIdentifiers: List<Identifier>): Item = Item(newIdentifiers, blank, rels, properties, pk)
    fun withIdentifier(newIdentifier: Identifier): Item = this.withIdentifiers(listOf(newIdentifier))


    /** Replace all relationships with a new set.
     */
    fun withRelationships(newRelationships: List<Relationship>) = Item(
        identifiers,
        blank,
        newRelationships,
        properties,
        pk
    )

    fun withRelationship(relationship: Relationship): Item =
        this.withRelationships(this.rels + relationship)

    fun hasNoIdentifiers(): Boolean = this.identifiers.isEmpty()

    /** Exactly one Identifier is required to make this unambiguous.
     * When an Item is unambiguous we can find it in the graph.
     * If it's ambiguous, we might find two Items, or be unable to find any Items.
     *
     * Jackson will try to serialize this field as a getter without the JsonIgnore.
     */
    @JsonIgnore
    fun isAmbiguous(): Boolean = this.identifiers.count() != 1

    fun withProperties(newProperties: List<Properties>) = Item(identifiers, blank, rels, newProperties, pk)
    fun withPropertiesFromMap(newProperties: Map<String, Any?>) =
        this.withProperties(listOf(propertiesFromSimpleMap(newProperties)))

    /**
     * Search the tree for subject, relationshipType, object triple.
     * Naive brute-force Look for these at any level of the tree.
     */
    fun containsTriple(subjectUri: String, relationshipType: String, objectUri: String, assertedBy: String): Boolean {
        fun recurse(item: Item): Boolean =
            if (item.identifiers.any { it.uri.toString() == subjectUri } &&
                item.rels.any { rel ->
                    rel.relTyp == relationshipType
                            && rel.obj.identifiers.any { it.uri.toString() == objectUri }
                            && rel.assertedBy.any { it.identifiers.any { it.uri.toString() == assertedBy } }
                })
                true
            else
                item.rels.any { recurse(it.obj) }

        return recurse(this)
    }

    fun containsProperty(subjectUri: String, property: String, value: String, assertedBy: String): Boolean {
        fun recurse(item: Item): Boolean =
            if (item.identifiers.any { it.uri.toString() == subjectUri } &&
                item.properties.any { it.values[property]?.textValue() == value
                        && it.assertedBy.any { it.identifiers.any { it.uri.toString() == assertedBy } } }
            )
                true
            else
                item.rels.any { recurse(it.obj) }

        return recurse(this)
    }

    fun countItems() : Int = 1 + this.rels.sumOf { it.obj.countItems() }

    /**
     * Return a copy of this with the 'blank' flag set true.
     */
    fun withBlank(blank: Boolean=true): Item = Item(identifiers, blank=blank, rels, properties)
}
