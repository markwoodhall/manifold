package org.crossref.manifold.modules.graphviz

import org.crossref.manifold.itemgraph.stats.RelationshipStatsDao
import org.springframework.core.env.Environment
import org.springframework.core.env.get
import org.springframework.stereotype.Service
import javax.annotation.PreDestroy


/**
 * If the user supplies a "DUMP_GRAPHVIZ" argument, save the whole database to that filename in GraphViz DOT format before exit.
 */
@Service
class Dump(
    private val env: Environment? = null,
    private val relationshipStatsDao: RelationshipStatsDao? = null
) {
    @PreDestroy
    fun destroy() {
        val filename = env?.get("GRAPHVIZ_DUMP")

        if (filename != null) {
            println("Dump GraphViz: $filename")
            relationshipStatsDao!!.dbToGraphViz(filename)
        }
    }
}
