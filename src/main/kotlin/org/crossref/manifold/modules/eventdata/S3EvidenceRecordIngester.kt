package org.crossref.manifold.modules.eventdata

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.eventdata.Module.Companion.EVENT_DATA
import org.crossref.manifold.modules.eventdata.support.EvidenceRecord
import org.crossref.manifold.modules.eventdata.support.ingester.convertEvidenceRecordToEnvelopeBatch
import org.crossref.messaging.aws.autoconfig.S3AutoConfig.Companion.S3_NOTIFICATION_QUEUE
import org.crossref.messaging.aws.s3.S3EventNotification
import org.crossref.messaging.aws.sqs.SqsListener
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.GetObjectRequest

class S3EvidenceRecordIngester(
    private val itemGraphIngester: ItemGraph,
    private val s3Client: S3Client,
    private val agentsAllowList: Set<String>
) {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    private val mapper: ObjectMapper = ObjectMapper()
        .registerModule(JavaTimeModule())
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .setDateFormat(StdDateFormat().withColonInTimeZone(true))
        .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
        .registerModule(KotlinModule())

    @SqsListener("$EVENT_DATA.$S3_NOTIFICATION_QUEUE")
    fun onMessageReceived(
        s3EventNotification: S3EventNotification
    ) {
        try {
            for ((objectKey, bucketName) in s3EventNotification.records) {
                val s3Object = s3Client.getObject(
                    GetObjectRequest.builder()
                        .bucket(bucketName)
                        .key(objectKey)
                        .build()
                )

                val content = s3Object.buffered().reader().readText()

                val evidenceRecord: EvidenceRecord = mapper.readValue(content)

                if (evidenceRecord.sourceId in agentsAllowList) {
                    logger.info("Launching evidence record s3 ingestion...")
                    val envelopeBatches = convertEvidenceRecordToEnvelopeBatch(
                        objectKey,
                        evidenceRecord
                    )

                    itemGraphIngester.ingest(envelopeBatches)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}