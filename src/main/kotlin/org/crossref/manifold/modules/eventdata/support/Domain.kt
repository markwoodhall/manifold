package org.crossref.manifold.modules.eventdata.support

import com.fasterxml.jackson.annotation.JsonProperty

data class Author(val url: String)

data class EventObject(val pid: String?)

data class EventSubject(
    val pid: String,
    val url: String?,
    val title: String? = null,
    val author: Author? = null,
    @JsonProperty(value = "original-tweet-url") val originalTweetUrl: String? = null,
    @JsonProperty(value = "original-tweet-author") val originalTweetAuthor: String? = null,
    @JsonProperty(value = "alternative-id") val alternativeId: String? = null
)

data class EvidenceRecord(
    val pages: List<Page>,
    @JsonProperty("source-id")
    val sourceId: String,
    val timestamp: String,
)

data class Page(val actions: List<Action> = emptyList())

data class Action(
    val id: String?,
    @JsonProperty("occurred-at")
    val occurredAt: String,
    val matches: List<Match>,
    val url: String,
    val subj: ActionSubject?,
    @JsonProperty("relation-type-id")
    val relationTypeId: String
)

data class Event(
    val license: String,
    @JsonProperty("obj_id")
    val objId: String,
    @JsonProperty("source_token")
    val sourceToken: String,
    @JsonProperty("occurred_at")
    val occurredAt: String,
    @JsonProperty("subj_id")
    val subjId: String,
    val id: String,
    @JsonProperty("evidence_record")
    val evidenceRecord: String,
    val action: String,
    val subj: EventSubj,
    @JsonProperty("source_id")
    val sourceId: String,
    val obj: Obj,
    @JsonProperty("relation_type_id")
    val relationTypeId: String,
)

data class EventSubj(
    val pid: String,
    val url: String,
    val type: String?,
    val title: String?,
    val issued: String?,
    val author: Author?,
    val originalTweetAuthor: String?,
    val originalTweetUrl: String?,
    val alternativeId: String?
)

data class Obj(
    val pid: String,
    val url: String,
    val method: String,
    val verification: String,
)

data class Match(
    val type: String,
    val value: String,
    val match: String,
    val method: String?,
    val verification: String?,
)

data class ActionSubject(
    val type: String? = null,
    val title: String? = null,
    val issued: String? = null,
    val author: Author? = null,
    val url: String? = null,
    @JsonProperty("json-url")
    val jsonUrl: String? = null,
    @JsonProperty("original-tweet-url")
    val originalTweetUrl: String? = null,
    @JsonProperty("original-tweet-author")
    val originalTweetAuthor: String? = null
)