package org.crossref.manifold.modules.crossreforganizations

import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.modules.Manifest
import org.crossref.manifold.modules.ModuleRegistrar
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.Scheduled

@ConditionalOnProperty(prefix = Module.CROSSREFORGANIZATIONS, name = [Module.SUGAR_IMPORT_ENABLED])
@Configuration(Module.CROSSREFORGANIZATIONS)
class Module(
    moduleRegistrar: ModuleRegistrar,
    private val resolver: Resolver,
    private val itemGraph: ItemGraph,
    @Value("\${$CROSSREFORGANIZATIONS.$SUGAR_URL}") private val url: String,
    @Value("\${$CROSSREFORGANIZATIONS.$SUGAR_USERNAME}") private val username: String,
    @Value("\${$CROSSREFORGANIZATIONS.$SUGAR_PASSWORD}") private val password: String,
) {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    companion object {
        const val CROSSREFORGANIZATIONS = "crossref-organizations"
        const val SUGAR_IMPORT_ENABLED = "sugar-import-enabled"
        const val SUGAR_IMPORT_CRON = "sugar-import-cron"
        const val SUGAR_URL = "sugar-url"
        const val SUGAR_USERNAME = "sugar-username"
        const val SUGAR_PASSWORD = "sugar-password"
    }

    init {
        moduleRegistrar.register(
            Manifest(
                name = CROSSREFORGANIZATIONS,
                description = "Crossref Organizations",
            ),
        )
    }

    fun sugarIngestion(): SugarIngestion = SugarIngestion(SugarService(url), itemGraph)

    @Scheduled(cron = "\${$CROSSREFORGANIZATIONS.$SUGAR_IMPORT_CRON}")
    fun importFromSugar() {
        val ingestion = sugarIngestion()
        ingestion.ingest(username, password)
    }
}
