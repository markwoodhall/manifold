package org.crossref.manifold.modules.crossreforganizations

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.modules.consts.RelationshipTypes
import org.crossref.manifold.rendering.Configuration.MEMBERS_ENABLED
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.ContentType
import org.crossref.manifold.rendering.ContentTypeRenderer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import java.net.URI

@Component
@ConditionalOnProperty(prefix = RENDERING, name = [MEMBERS_ENABLED], matchIfMissing = true)
class MemberRenderer() : ContentTypeRenderer {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    private val mapper = jacksonObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategies.KEBAB_CASE)

    override fun internalContentType(): ContentType = ContentType.MEMBER_JSON
    override fun collection(): String = "https://id.crossref.org/collections/organization"

    override fun render(itemTree: Item): String? {
        logger.info("Rendering ${itemTree.pk} using ${internalContentType()}")

        val memberOf = itemTree.rels.firstOrNull { it.relTyp == RelationshipTypes.MEMBER_OF }?.obj
        if (memberOf?.identifiers?.none { it.uri == URI(Items.CROSSREF_AUTHORITY) } ?: true) return null

        val property = itemTree.properties.firstOrNull()
        if (property == null) {
            logger.error("Tried to render ${itemTree.pk} as member JSON but the expected property assertions cannot be found")
            return null
        }

        val primaryName = property.values.get("name").textValue()
        val type = property.values.get("type").textValue()
        val sponsoredBy = itemTree.rels.firstOrNull { it.relTyp == RelationshipTypes.SPONSOR }?.obj

        var memberPath = itemTree.identifiers.firstOrNull()?.uri?.path

        val crossrefMemberId = try
        {
            var memberPattern = """/(\d*)$""".toRegex()
            if (memberPath != null) {
                memberPattern.findAll(memberPath).firstOrNull()?.groups?.get(1)?.value?.toInt()
            } else {
                null
            }
        } catch (e: Exception) {
            logger.error("Error parsing member id: $memberPath error: $e")
            null
        }

        val member = Member(
            id = crossrefMemberId,
            type = type,
            primaryName = primaryName,
            identifiers = itemTree.identifiers.map { OrganizationIdentifier(it.uri) },
            relationships = OrganizationRelationship(
                memberOf = memberOf?.let {
                    NamedOrganizationIdentifiers(
                        memberOf.identifiers.map { OrganizationIdentifier(it.uri) },
                        "Crossref",
                    )
                },
                sponsoredBy = sponsoredBy?.let {
                    NamedOrganizationIdentifiers(
                        sponsoredBy.identifiers.map { OrganizationIdentifier(it.uri) },
                        sponsoredBy.properties.firstOrNull()?.values?.get("name")?.textValue(),
                    )
                },
            ),
        )

        return mapper.writeValueAsString(member)
    }
}
