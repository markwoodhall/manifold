package org.crossref.manifold.modules.crossreforganizations

import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Properties
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.itemtree.jsonObjectFromMap
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.modules.consts.RelationshipTypes
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.OffsetDateTime

class SugarIngestion(
    private val sugarService: SugarService,
    private val itemGraph: ItemGraph,
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Ingest organization data downloaded from Sugar into the Item Graph.data
     *
     * @return the ingestion id, which can be used to track progress
     */
    fun ingest(username: String, password: String) {
        logger.info("Starting organization ingestion")

        val token = sugarService.authenticate(username, password)
        var organizations = PagedSugarOrganizations(0, arrayOf())
        val crossrefMemberIds = mutableMapOf<String, SugarOrganization>()

        // The Sugar API returnx nextOffset of -1 when there are no more items
        while (organizations.nextOffset != -1) {
            organizations = sugarService.getOrganizations(token, organizations.nextOffset)

            // Store the organization in memory by its id. We need the id mapping anyway
            // to be able to look up sponsorId, which is the Sugar Id. Instead of just holding
            // the mapping in memory, we store the entire thing for now, since it saves us calling
            // the Sugar API again later, and with only ~20k members the memory footprint should be low
            organizations.records.forEach { o ->
                o.crossrefMemberId?.let {
                    // If we stumble upon duplicate sugar ids then log an error,
                    // we can alert on this at some point, remove the key so that
                    // no ingestion occurs
                    if (crossrefMemberIds.containsKey(o.id)) {
                        val existingOrg = crossrefMemberIds[o.id]
                        logger.error("Duplicate Sugar Id ${o.id} was detected in objects $o and $existingOrg")
                        crossrefMemberIds.remove(o.id)
                    } else {
                        crossrefMemberIds[o.id] = o
                    }
                }
            }

            // Sleep to limit the number of called per second to the Sugar API
            Thread.sleep(100)
        }

        val envelopeBatch = EnvelopeBatch(
            crossrefMemberIds.map { o ->
                var item = Item()
                    .withIdentifier(Identifier.new(o.value.identifier))
                    .withProperties(
                        listOf(
                            Properties(
                                jsonObjectFromMap(
                                    mapOf(
                                        "name" to o.value.name,
                                        "type" to "org",
                                    ),
                                ),
                            ),
                        ),
                    )

                item = if (o.value.active) {
                    if (o.value.isMember) {
                        item.withRelationships(
                            listOf(
                                Relationship(
                                    RelationshipTypes.COLLECTION,
                                    Item().withIdentifier(Identifier.new("https://id.crossref.org/collections/organization")),
                                ),
                                Relationship(
                                    RelationshipTypes.MEMBER_OF,
                                    Item().withIdentifier(Identifier.new(Items.CROSSREF_AUTHORITY)),
                                ),
                            ),
                        )
                    } else {
                        item.withRelationship(
                            Relationship(
                                RelationshipTypes.COLLECTION,
                                Item().withIdentifier(Identifier.new("https://id.crossref.org/collections/organization")),
                            ),
                        )
                    }
                } else {
                    item
                }

                if (o.value.isSponsored) {
                    if (!o.value.sponsorId.isNullOrEmpty()) {
                        val sponsorIdentifier = crossrefMemberIds[o.value.sponsorId]?.identifier
                        if (sponsorIdentifier != null) {
                            item = item.withRelationship(
                                Relationship(
                                    RelationshipTypes.SPONSOR,
                                    Item().withIdentifier(Identifier.new(sponsorIdentifier)),
                                ),
                            )
                        }
                    }
                }

                Envelope(
                    listOf(item),
                    ItemTreeAssertion(
                        OffsetDateTime.now(),
                        MergeStrategy.CLOSED,
                        Item().withIdentifier(Identifier.new(Items.CROSSREF_AUTHORITY)),
                    ),
                )
            },
            EnvelopeBatchProvenance("Crossref SugarCRM Import/1.0", OffsetDateTime.now().toString()),
        )

        logger.info("Asserting organizations into the item graph")
        val ingestionId = itemGraph.ingest(listOf(envelopeBatch))
        while (itemGraph.isIngestionRunning(ingestionId)) {
            logger.info("Waiting for organizations ingestion to complete")
            Thread.sleep(1000)
        }
        logger.info("Asserted organizations into the item graph")
    }
}
