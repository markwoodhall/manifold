package org.crossref.manifold.modules.crossreforganizations

import com.fasterxml.jackson.annotation.JsonProperty

data class AuthRequest(
    val grantType: String = "password",
    val clientId: String = "sugar",
    val clientSecret: String = "",
    val username: String,
    val password: String,
    val platform: String = "custom",
)

data class AuthResponse(
    val accessToken: String,
    val expiresIn: Int,
    val tokenType: String,
    val scope: String,
)

data class PagedSugarOrganizations(
    val nextOffset: Int,
    val records: Array<SugarOrganization>,
)

data class SugarOrganization(
    val id: String,
    val name: String,
    val sponsorId: String?,
    @JsonProperty("crossref_member_id_c")
    val crossrefMemberId: Int?,
    val accountType: String,
    val identifier: String = "https://id.crossref.org/organization/$crossrefMemberId",
    @JsonProperty("active_c")
    val active: Boolean,
) {
    val isMember: Boolean get() = when (accountType) {
        "Member Publisher",
        "REPRESENTED_MEMBER",
        "Sponsoring Publishers",
        -> true
        else -> false
    }

    val isSponsor: Boolean get() = when (accountType) {
        "Sponsoring Entities",
        "Sponsoring Publishers",
        -> true
        else -> false
    }

    val isSponsored: Boolean get() = when (accountType) {
        "Sponsored Publisher",
        "REPRESENTED_MEMBER",
        -> true
        else -> false
    }
}
