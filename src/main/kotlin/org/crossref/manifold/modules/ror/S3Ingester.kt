package org.crossref.manifold.modules.ror

import org.crossref.manifold.modules.ror.Module.Companion.ROR
import org.crossref.messaging.aws.autoconfig.S3AutoConfig.Companion.S3_NOTIFICATION_QUEUE
import org.crossref.messaging.aws.s3.S3EventNotification
import org.crossref.messaging.aws.sqs.SqsListener
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.GetObjectRequest

class S3Ingester(
    private val s3Client: S3Client,
    private val rorIngestion: RORIngestion,
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Process ROR json files retrieved from S3.
     * The method will attempt to fetch the file, transform it and then ingest it as an EnvelopeBatch.
     */
    @SqsListener("$ROR.$S3_NOTIFICATION_QUEUE")
    fun onMessageReceived(s3EventNotification: S3EventNotification) {
        logger.info("Starting ror s3 ingestion...")
        for ((objectKey, bucketName) in s3EventNotification.records) {
            if (!objectKey.endsWith(".json")) {
                logger.warn("Skipping non json file $bucketName/$objectKey")
            } else {
                logger.info("Ingesting ROR file $bucketName/$objectKey")
                val s3Object = s3Client.getObject(
                    GetObjectRequest.builder()
                        .bucket(bucketName)
                        .key(objectKey)
                        .build(),
                )

                val content = s3Object.buffered().reader().readText()
                rorIngestion.ingest(content)
            }
        }
    }
}
