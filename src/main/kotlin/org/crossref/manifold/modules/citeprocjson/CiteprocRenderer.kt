package org.crossref.manifold.modules.citeprocjson

import clojure.lang.Keyword
import clojure.lang.RT
import org.crossref.cayenne
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.rendering.Configuration.CITEPROC_ENABLED
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.ContentType
import org.crossref.manifold.rendering.ContentTypeRenderer
import org.crossref.manifold.util.clojure.toClj
import org.crossref.manifold.modules.consts.RelationshipTypes
import org.crossref.manifold.modules.consts.IdentifierTypes
import org.crossref.manifold.itemgraph.Resolver
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component

@Component
@ConditionalOnProperty(prefix = RENDERING, name = [CITEPROC_ENABLED], matchIfMissing = true)
class CiteprocRenderer() : ContentTypeRenderer {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)
    private val organizationIdentifierRegex: Regex = Regex(IdentifierTypes.ORGANIZATION_PATTERN)

    init {
        cayenne.boot()
    }

    override fun internalContentType(): ContentType = ContentType.CITEPROC_JSON
    override fun collection(): String = "https://id.crossref.org/collections/work"

    /**
     * Internally, manifold has the concept of an organization but typically things
     * that we consider as such are represented as members, before we can call out to
     * cayenne we need to turn organization identifiers into member identifiers
     */
    fun organizationIdToMemberId(item: Item): Item {
        val newItem = item
            .withIdentifiers(
                item.identifiers.map { identifier ->
                    if (identifier.uri.toString().startsWith(IdentifierTypes.ORGANIZATION_URI)) {
                        Identifier.new(identifier.uri.toString().replace(organizationIdentifierRegex, IdentifierTypes.MEMBER_URI))
                    } else {
                        identifier
                    }
                },
            )
            .withRelationships(
                item.rels.map { relationship ->
                    relationship.withItem(organizationIdToMemberId(relationship.obj))
                },
            )
        return newItem
    }

    override fun render(itemTree: Item): String {
        logger.info("Rendering ${itemTree.pk} using ${internalContentType()}")

        // Turn organization ids back into member ids for cayenne
        val patchedItem = organizationIdToMemberId(itemTree)

        val fieldsToMakeValuesKeywords = setOf("type", "subtype", "kind")

        val converted = patchedItem.toClj(fieldsToMakeValuesKeywords)
        val esDoc = cayenne.itemToEsDoc(converted)
        val cleanedEsDoc = RT.dissoc(esDoc, Keyword.intern("indexed"))

        val steward = patchedItem.rels.firstOrNull { it.relTyp == RelationshipTypes.STEWARD }?.obj

        var citeproc = cayenne.esDocToCiteproc(cleanedEsDoc)
            ?: throw Exception("Couldn't find that item.")

        if (steward != null) {
            val property = steward.properties.firstOrNull()
            val name = property?.values?.get("name")?.textValue()
            citeproc = RT.assoc(citeproc, "publisher", name)
        }

        return cayenne.writeJsonString(citeproc)
    }
}
