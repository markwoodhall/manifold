package org.crossref.manifold.modules.unixml.support

import com.fasterxml.jackson.databind.node.*
import org.crossref.cayenne
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.ingestion.removeAmbiguousItems
import org.crossref.manifold.itemgraph.Collections.Companion.COLLECTION_RELATIONSHIP_TYPE
import org.crossref.manifold.itemgraph.Collections.Companion.collectionIdentifierForType
import org.crossref.manifold.itemgraph.Collections.Companion.collectionIdentifierForTypeSubtype
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemtree.*
import org.crossref.manifold.modules.consts.IdentifierTypes
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.modules.consts.RelationshipTypes
import org.crossref.manifold.modules.unixml.support.Metadata.getDoiRegistrations
import org.crossref.manifold.retrieval.view.markBlankNodes
import org.crossref.manifold.util.clojure.fromClj
import java.io.InputStreamReader
import java.time.OffsetDateTime

/**
 * User Agent string for this module.
 */
const val CROSSREF_XML_INGEST = "Crossref UNIXML Importer/1.0"

/**
 * For a UniXML derived Item Tree, create the Envelope Batch that asserts it.
 *
 * If bulk is set, all merge strategies are set to 'naive' for simple ingestion.
 */
fun itemTreeToEnvelopes(
    metadataItemTree: Item,
    trace: String,
    bulk: Boolean = false
): EnvelopeBatch {

    // Remove relationship types for Items that are actually transformed to properties.
    val lessRelTypes = Metadata.patchRemoveRelTypes(metadataItemTree)

    val lessIssn = Metadata.removeKnownAmbiguities(lessRelTypes)

    // Any nodes that weren't assigned blank nodes should have them added.
    val withBlankNodes = markBlankNodes(lessIssn, IdentifierTypes.CR_BLANK_NODE_PREFIX)

    // Now find the set of landing pages found in the <doi_data> tags.
    val registeredDois = getDoiRegistrations(withBlankNodes)

    // Expect to find only one MemberId here.
    val memberIds = CrmItems.getStewardMemberId(withBlankNodes)
    val distinctMemberIds = (memberIds).toSet()

    return if (distinctMemberIds.size != 1) {
        val message =
            "ERROR! Found more than one member id in this file. We expect to find exactly one. Got: $distinctMemberIds"
        logger.error(message)
        throw Exception(message)
    } else {

        val depositingMember = distinctMemberIds.first()

        val crossref = Item().withIdentifier(Identifier.new(Items.CROSSREF_AUTHORITY))



        // "Crossref says that this member is the maintainer of this DOI".
        val maintainerAssertions = registeredDois.map { (landingPageItem, type, subType) ->
            val typeCollection = collectionIdentifierForType(type ?: "UNKNOWN")
            val subTypeCollection = collectionIdentifierForTypeSubtype(type ?: "UNKNOWN", subType ?: "UNKNOWN")

            val tree = CrmItems.memberIdToOrganizationId(
                landingPageItem.withRelationships(
                    listOf(
                        Relationship(RelationshipTypes.STEWARD, depositingMember),
                        Relationship(COLLECTION_RELATIONSHIP_TYPE, Item().withIdentifier(Identifier.new(typeCollection))),
                        Relationship(COLLECTION_RELATIONSHIP_TYPE, Item().withIdentifier(Identifier.new(subTypeCollection))),
                    ),
                ),
            )

            val assertion = ItemTreeAssertion(
                assertedAt = OffsetDateTime.now(),
                assertingParty = crossref,

                // If there was a prior maintainer, it should be replaced.
                // In bulk mode we rely on there being no prior maintainer data to replace.
                mergeStrategy = if (bulk) MergeStrategy.NAIVE else MergeStrategy.CLOSED,
            )

            Envelope(listOf(tree), assertion)
        }

        // Crossref says that it is the registration agency of this DOI.
        val registrationAgencyAssertions = registeredDois.map { (contentItem, _, _) ->
            val tree = CrmItems.memberIdToOrganizationId(
                contentItem.withRelationships(
                    listOf(
                        Relationship(RelationshipTypes.RA_REL_TYPE, crossref)
                    ),
                ),
            )

            val assertion = ItemTreeAssertion(
                assertedAt = OffsetDateTime.now(),
                assertingParty = crossref,

                // Just union all assertions we get, as this is designed for simple bulk ingestion.
                mergeStrategy = MergeStrategy.NAIVE,
            )

            Envelope(listOf(tree), assertion)
        }

        // And finally "depositing member says that here's some bibliographic metadata"
        val metadataItemTreeAssertion = ItemTreeAssertion(
            assertedAt = OffsetDateTime.now(),
            assertingParty = depositingMember,

            /** Just union all assertions we get, as this is designed for simple bulk ingestion.
             * We'd normally use a UNION strategy but this is expected to populate an empty database from a snapshot,
             * Note that the [RESOLUTION_URL_REL_TYPE] is asserted above with the same from_item_pk and asserting party
             * meaning that a Closed merge strategy is unsuitable.
             **/
            mergeStrategy = MergeStrategy.NAIVE,
        )
        val metadataEnvelope = Envelope(listOf(withBlankNodes), metadataItemTreeAssertion)

        val allEnvelopes =
            listOf(metadataEnvelope) + registrationAgencyAssertions + maintainerAssertions

        EnvelopeBatch(allEnvelopes, EnvelopeBatchProvenance(CROSSREF_XML_INGEST, trace))
    }
}

/**
 * For a UNIXML document construct an Envelopes that asserts the metadata itself.
 *
 * If bulk is set, all merge strategies are set to 'naive' for simple ingestion.
 */
fun parseUniXmlToEnvelopes(
    trace: String,
    text: String,
    bulk: Boolean = false,
): Collection<EnvelopeBatch> {
    logger.debug("Reading UniXML into Item Tree assertion...")

    return InputStreamReader(text.byteInputStream()).use { stream ->

        // List of pairs of [primary-identifier, item-tree]
        //        val parsedXmlPairs = cayenne.parseXml(stream, "query_result")
        val parsedXmlPairs = cayenne.parseXmlCentred(stream, "query_result")

        // The Metadata Item Trees are those that represent the pure metadata within the XML file.
        val metadataItemTrees = metadataItemTrees(parsedXmlPairs)

        // At this point we'll need to do some patching to get it into an Envelope, and that can happen for each Item
        // tree individually.
        val all = metadataItemTrees.map {
            itemTreeToEnvelopes(
                it,
                trace,
                bulk
            )
        }

        // This is a bit of an emergency breaker to find bugs in the parser.
        all.mapNotNull { removeAmbiguousItems(it) }
    }
}

private fun metadataItemTrees(parsedXmlPairs: MutableIterable<Any?>) =
    parsedXmlPairs.map { tree ->
        // First item in the pair is the primary identifier for this doc which we don't need.
        val itemTree = tree as Map<*, *>
        Item.fromClj(itemTree)
    }
