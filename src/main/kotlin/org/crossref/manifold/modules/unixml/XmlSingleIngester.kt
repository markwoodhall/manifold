package org.crossref.manifold.modules.unixml

import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.unixml.support.parseUniXmlToEnvelopes
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.*

@Service
class XmlSingleIngester(
    private val itemGraph: ItemGraph,
) {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)


    fun ingest(objectKey: String, content: String): UUID {
        logger.debug("Ingest XML: parsing.")
        val envelopeBatches = parseUniXmlToEnvelopes(
            objectKey,
            content,
        )
        logger.debug("Ingest XML: ${envelopeBatches.count()} batches...")
        val ingestionId = itemGraph.ingest(envelopeBatches)
        logger.debug("Ingest XML: finished.")
        return ingestionId
    }
}
