package org.crossref.manifold.modules.unixml.support

import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Properties
import org.crossref.manifold.itemtree.propertiesFromSimpleMap
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.modules.consts.RelationshipTypes
import org.crossref.manifold.modules.consts.IdentifierTypes

/**
 * These functions patch or extract data that are ultimately derived from CRM-items.
 * This data is outside the metadata XML, so we need to be very mindful of using it.
 */
object CrmItems {

    private val memberIdentifierRegex: Regex = Regex(IdentifierTypes.MEMBER_PATTERN)

    /**
     * Retrieve the Member Item (i.e. that with a http://id.crossref.org/member/ identifier)
     * From anywhere in the tree, may be in the ancestors.
     */
    fun getStewardMemberId(item: Item): List<Item> =
        item.rels.flatMap {
            // Find other Items in the 'steward' role with relation to this one.
            if (it.relTyp == "steward") {
                // Of which, find any identifier that says it's a member.
                it.obj.identifiers
                    .filter { i -> i.uri.toString().startsWith(IdentifierTypes.MEMBER_URI) }

                    // Don't take the whole 'publisher' object, just an empty Item with just that Identifier.
                    .map { Item().withIdentifier((it)) }

            } else {
                emptyList()
            }
        } + item.rels.flatMap { getStewardMemberId(it.obj) }

    /**
     * Internally, manifold has the concept of an organization but typically things
     * that we consider as such are represented as members, here we turn member identifiers
     * into organization identifiers
     */
    fun memberIdToOrganizationId(item: Item): Item {
        val newItem = item
            .withIdentifiers(
                item.identifiers.map { identifier ->
                    if (identifier.uri.toString().startsWith(IdentifierTypes.MEMBER_URI)) {
                        Identifier.new(identifier.uri.toString().replace(memberIdentifierRegex, IdentifierTypes.ORGANIZATION_URI))
                    } else {
                        identifier
                    }
                },
            )
            .withRelationships(
                item.rels.map { relationship ->
                    relationship.withItem(memberIdToOrganizationId(relationship.obj))
                },
            )
        return newItem
    }
}

/**
 * These functions patch or extract data from the XML member-supplied metadata.
 */
object Metadata {
    private val patchRemoveRelTypesToDrop = setOf(
        // these should be folded in as an identifier directly.
        "issn",
        "isbn",

        // these should be properties.
        "approved",
        "published",
        "update-policy",
        "start",

        "published-print",
        "published-online",
        "content-created",
        "content-updated",
        "published-print",
        "published-online",
        "published-other",

        // unsure
        "assertion",
    )

    /**
     * For an Item Tree, retrieve all DOIs being registered. Each is represented by a simple Item with the DOI as its
     * identifier. The type and subtype are also returned for each.
     * @return set of Triples of Item, content type, subtype.
     */
    fun getDoiRegistrations(itemTree: Item): Set<Triple<Item, String?, String?>> {
        // Set of pairs of content item (e.g. article) to landing page item.
        // The content item only needs its identifier, not the rest of its baggage.
        val itemLandingPages = mutableSetOf<Triple<Item, String?, String?>>()

        // This recursive function looks at two levels. The `item` variable points to the current node, and then we look
        // at the items via its immediate relations.
        fun recurse(item: Item) {
            // If there's a resource resolution for this, it's a DOI.
            if (item.rels.any { it.relTyp == RelationshipTypes.RESOURCE_RESOLUTION }) {
                val doiItem = Item().withIdentifier(item.identifiers.first())
                val type = item.properties.firstNotNullOfOrNull {
                    it.values.get("type")
                }?.asText()

                val subtype = item.properties.firstNotNullOfOrNull {
                    it.values.get("subtype")
                }?.asText()

                itemLandingPages.add(Triple(doiItem, type, subtype))
            }

            // And recurse down.
            for (rel in item.rels) {
                recurse(rel.obj)
            }
        }

        recurse(itemTree)

        return itemLandingPages
    }

    /**
     * Cayenne's item tree parser currently represents a few things as Items that really should be properties.
     * Recursively walk the tree, extracting those relationships, patching them into Properties by merging with existing
     * properties.
     *
     * This remodelling will be addressed in future versions of Cayenne, so this code should be removed when all named
     * relationships have been removed.
     */
    fun patchRemoveRelTypes(item: Item): Item {
        // Split the relationships into those that do and don't match the list of types to drop.
        val (toProps, keepRels) = item.rels.partition { patchRemoveRelTypesToDrop.contains(it.relTyp) }

        // Although Item Tree allows for multiple Property Statements (for different provenances), in this module we
        // expect to have at most one Property. Retrieve that, or create a blank one.
        val itemProps: Properties = item.properties.firstOrNull() ?: propertiesFromSimpleMap(emptyMap())

        // Convert relationship type into property name and value into property value.
        val relsAsProps = propertiesFromSimpleMap(toProps.map {
            Pair(it.relTyp, it.obj.properties.firstOrNull()?.values)
        }.toMap())

        // Then merge with existing properties on this Item.
        val combinedProps = itemProps.merge(relsAsProps)

        // Reconstruct the item, patching in the new combined Properties and the Relationships with the unwanted ones
        // removed.
        // Relationships are also the point of recursion in the tree, so recurse in applying this function to sub-trees.
        val newItem = item
            .withProperties(listOf(combinedProps))
            .withRelationships(keepRels.map { relationship ->
                relationship.withItem(
                    patchRemoveRelTypes(relationship.obj)
                )
            })

        return newItem
    }

    /**
     * Remove known ambiguities such as ISSNs and Supplementary IDs for now because they cause a lot of ambiguous
     * Identifiers.
     * This will be fixed in a future Item Tree parser.
     */
    fun removeKnownAmbiguities(item: Item): Item =
        item
            .withIdentifiers(item.identifiers.filterNot {
                it.uri.toString().startsWith("http://id.crossref.org/issn/") or
                        it.uri.toString().startsWith("http://id.crossref.org/supp/")
            })
            .withRelationships(item.rels.map { it.withItem(removeKnownAmbiguities(it.obj)) })
}


