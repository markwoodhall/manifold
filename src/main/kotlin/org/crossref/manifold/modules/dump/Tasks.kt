package org.crossref.manifold.modules.dump

import org.crossref.manifold.itemgraph.stats.RelationshipStatsDao
import org.springframework.core.env.Environment
import org.springframework.core.env.get
import org.springframework.stereotype.Service
import javax.annotation.PreDestroy

/**
 * If the user supplies a "DUMP_GRAPHVIZ" argument, save the whole database to that filename in GraphViz DOT format before exit.
 */
@Service
class DumpSql(
    private val env: Environment,
    private val relationshipStatsDao: RelationshipStatsDao
) {
    @PreDestroy
    fun destroy() {
        val filename = env["DUMP_SQL"]

        if (filename != null) {
            println("Dump SQL: $filename")

            relationshipStatsDao.dumpSql(filename)
        }
    }
}
