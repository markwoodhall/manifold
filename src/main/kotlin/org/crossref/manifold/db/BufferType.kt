package org.crossref.manifold.db

enum class BufferType { ITEM_IDENTIFIER, RELATIONSHIP_ASSERTION, PROPERTY_ASSERTION }