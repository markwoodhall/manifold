package org.crossref.manifold.db

object Constants {
    const val BUFFER = "buffer"
    const val BATCH_WAIT_SECONDS = "batch-wait-seconds"
}
