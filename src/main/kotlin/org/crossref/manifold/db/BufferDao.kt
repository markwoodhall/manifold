package org.crossref.manifold.db

import org.crossref.manifold.util.Constants.BATCH_SIZE
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Repository
import java.util.concurrent.locks.ReentrantLock

@Repository
class BufferDao(
    private val jdbcTemplate: JdbcTemplate,
    private val bufferLockDao: BufferLockDao,
    @Value("\${${Constants.BUFFER}.${BATCH_SIZE}:1000000}") private val batchSize: Int,
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    private val bufferLocks = BufferType.values().associateWith { ReentrantLock() }

    /*
     * Compatible buffer functions must use the following signature.
     *
     * process_buffered_XXXX(IN batch_size INT, OUT count_processed INT, OUT count_remaining INT)
     */
    @Async
    fun processBuffer(bufferType: BufferType) {
        if (bufferLocks[bufferType]!!.tryLock()) {
            try {
                if (bufferLockDao.lockBuffer(bufferType)) {
                    try {
                        val prefixedPF = "process_buffered_${bufferType.name.lowercase()}s"

                        var countProcessed = 0
                        var countRemaining = 0
                        do {
                            logger.debug("running $prefixedPF with batch size $batchSize")
                            jdbcTemplate.query("SELECT * FROM $prefixedPF(?)", {
                                countProcessed = it.getInt("count_processed")
                                countRemaining = it.getInt("count_remaining")
                            }, batchSize)

                            if (countProcessed > 0 || countRemaining > 0) {
                                { msg: String -> logger.info(msg) }
                            } else {
                                { msg: String -> logger.debug(msg) }
                            }(
                                "$prefixedPF reports $countProcessed rows processed, $countRemaining rows remaining. " + if (countRemaining > 0) {
                                    "Rerunning..."
                                } else {
                                    "Exiting..."
                                }
                            )
                        } while (countRemaining > 0)
                    } catch (exception: Exception) {
                        logger.error("Failed to process buffer batch: $exception", exception)
                    } finally {
                        bufferLockDao.unlockBuffer(bufferType)
                    }
                }
            } finally {
                bufferLocks[bufferType]!!.unlock()
            }
        }
    }
}
