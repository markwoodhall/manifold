package org.crossref.manifold.db

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.util.concurrent.TimeUnit

data class Fetched<T>(
    val rows: List<T>,
    val totalRows: Int,
    val fetchSize: Int,
    val nextCursor: String?,
)

@Repository
class CursorDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    @Scheduled(fixedRate = 1, timeUnit = TimeUnit.MINUTES)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun closeUnusedCursors() {
        jdbcTemplate.update(
            "DELETE FROM open_cursor WHERE last_used_at < NOW() - INTERVAL '5 minutes'"
        )
    }
}
