package org.crossref.manifold.db

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

@Repository
class BufferLockDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun lockBuffer(bufferType: BufferType): Boolean =
        jdbcTemplate.update(
            "UPDATE buffer_count SET processor_running = TRUE WHERE name = ? AND NOT processor_running",
            bufferType.name
        ) == 1

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun unlockBuffer(bufferType: BufferType): Boolean =
        jdbcTemplate.update(
            "UPDATE buffer_count SET processor_running = FALSE WHERE name = ? AND processor_running",
            bufferType.name
        ) == 1
}
