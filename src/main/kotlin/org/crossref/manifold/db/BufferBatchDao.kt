package org.crossref.manifold.db

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.crossref.manifold.db.Constants.BATCH_WAIT_SECONDS
import org.crossref.manifold.db.Constants.BUFFER
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Repository
class BufferBatchDao(
    private val jdbcTemplate: JdbcTemplate,
    @Value("\${${BUFFER}.${BATCH_WAIT_SECONDS}:1}") private val batchWaitSeconds: Int,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    private var logger: Logger = LoggerFactory.getLogger(this::class.java)


    private val bufferInserts = BufferType.values().associateWith {
        SimpleJdbcInsert(jdbcTemplate).withTableName("${it.name.lowercase()}_buffer").usingGeneratedKeyColumns("pk")
    }

    fun openBufferBatch(): UUID = UUID.randomUUID()

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun addToBufferBatch(bufferType: BufferType, values: List<MapSqlParameterSource>, batchId: UUID) {
        if (values.isNotEmpty()) {
            values.forEach { it.apply { addValue("buffer_batch_id", batchId) } }
            bufferInserts[bufferType]!!.executeBatch(*values.toTypedArray())
        }
    }

    /**
     * Wait for batch to finish processing within the given buffer type.
     */
    fun waitForBufferBatch(batchId: UUID, buffer: BufferType) {
        while (getBufferBatchSize(batchId, buffer) > 0) {
            runBlocking {
                delay(batchWaitSeconds * 1000L)
            }
        }
    }

    /**
     * Wait for all batches to finish processing with the given buffer type.
     */
    fun waitForBuffer(buffer: BufferType) {
        while (getBufferSize(buffer) > 0) {
            runBlocking {
                delay(batchWaitSeconds * 1000L)
            }
        }
    }

    fun isBufferBatchRunning(batchId: UUID, buffer: BufferType): Boolean {
        val size = getBufferBatchSize(batchId, buffer)
        logger.debug("Buffer size $batchId $buffer is $size")
        return size > 0
    }

    fun isBufferRunning(buffer: BufferType): Boolean {
        val size = getBufferSize(buffer)
        logger.debug("Buffer size $buffer is $size")
        return size > 0
    }

    private fun getBufferBatchSize(batchId: UUID, buffer: BufferType): Int {
        return jdbcTemplate.queryForObject(
            "SELECT COUNT(buffer_batch_id) FROM " + "${buffer.name.lowercase()}_buffer WHERE buffer_batch_id = ? ::uuid",
            Int::class.java,
            batchId
        )
    }

    /**
     * Get the size of the buffer for the given type.
     */
    private fun getBufferSize(buffer: BufferType): Int {
        return npTemplate.query(
            "SELECT COUNT(pk) AS count FROM " + "${buffer.name.lowercase()}_buffer",
            emptyMap<String, Any>()
        ) { rs, _ ->
            rs.getInt(
                "count"
            )
        }.first()
    }
}
