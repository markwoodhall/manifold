package org.crossref.messaging.core

import org.springframework.messaging.core.MessagePostProcessor

interface QueuePoller<T> {
    fun pollQueue(queueIdentifier: T, messagePostProcessor: MessagePostProcessor? = null)

    fun startPolling()

    fun stopPolling()
}
