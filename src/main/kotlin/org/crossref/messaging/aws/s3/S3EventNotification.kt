package org.crossref.messaging.aws.s3

/**
 * Simplified S3 event notification message received via SQS.
 */
data class S3EventNotification(val records: List<Record>) {
    data class Record(
        val objectKey: String, val bucketName: String
    )
}
