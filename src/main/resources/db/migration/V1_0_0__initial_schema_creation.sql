-- A batch of envelopes sent in response to user input.
CREATE TABLE envelope_batch
(
    pk         BIGSERIAL PRIMARY KEY    NOT NULL,
    ext_trace  VARCHAR                  NULL,
    user_agent VARCHAR                  NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE INDEX ON envelope_batch (ext_trace);

CREATE TABLE envelope
(
    pk                BIGSERIAL PRIMARY KEY    NOT NULL,
    envelope_batch_pk BIGINT                   NOT NULL,
    created_at        TIMESTAMP WITH TIME ZONE NOT NULL,
    FOREIGN KEY (envelope_batch_pk) REFERENCES envelope_batch (pk)
);

CREATE INDEX ON envelope (envelope_batch_pk);

CREATE TABLE buffer_count
(
    name              VARCHAR PRIMARY KEY NOT NULL,
    row_count         INT                 NOT NULL,
    processor_running BOOLEAN             NOT NULL
);

INSERT INTO buffer_count (name,
                          row_count,
                          processor_running)
VALUES ('ITEM_IDENTIFIER',
        0,
        FALSE);
INSERT INTO buffer_count (name,
                          row_count,
                          processor_running)
VALUES ('RELATIONSHIP_ASSERTION',
        0,
        FALSE);
INSERT INTO buffer_count (name,
                          row_count,
                          processor_running)
VALUES ('PROPERTY_ASSERTION',
        0,
        FALSE);

-- Buffer of item_identifiers .
-- Contains un-normalized scheme and host which will need to be normalized.
CREATE TABLE item_identifier_buffer
(
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    scheme VARCHAR NOT NULL,
    scheme_null BOOLEAN NOT NULL,
    host VARCHAR NOT NULL,
    host_null BOOLEAN NOT NULL,
    port SMALLINT,
    path VARCHAR NOT NULL,
    path_null BOOLEAN NOT NULL,
    query VARCHAR NOT NULL,
    query_null BOOLEAN NOT NULL,
    fragment VARCHAR NOT NULL,
    fragment_null BOOLEAN NOT NULL,
    persistent BOOLEAN NOT NULL,
    blank BOOLEAN NOT NULL,
    buffer_batch_id UUID NOT NULL
);
CREATE INDEX ON item_identifier_buffer (buffer_batch_id);

-- An Item is any 'thing' that exists, including Research Objects.
CREATE TABLE item
(
    pk         BIGSERIAL PRIMARY KEY    NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    -- does this item play the role of blank node. This doesn't mean 'empty'.
    blank      BOOLEAN NOT NULL
);

CREATE TABLE host (
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    val VARCHAR NOT NULL,
    val_null BOOLEAN NOT NULL
);
CREATE UNIQUE INDEX ON host (val, val_null);

CREATE TABLE scheme (
    pk SMALLSERIAL PRIMARY KEY NOT NULL,
    val VARCHAR NOT NULL,
    val_null BOOLEAN NOT NULL
);

-- An Item Identifier is a URI, which is associated with exactly one Item.
-- Identifiers from arbitrary websites can get big, hence no length constraint on suffix.
-- There is no performance difference with PostgreSQL.
-- NULL is significant in the URI model. Because of SQL's handling of NULL in indexing and comparison, we store a
-- separate flag to say whether the query and fragment are null.
CREATE TABLE item_identifier
(
    pk                 BIGSERIAL PRIMARY KEY NOT NULL,
    scheme_pk          SMALLINT NOT NULL,
    port               SMALLINT NOT NULL,
    host_pk            BIGINT   NOT NULL,
    path               VARCHAR  NOT NULL,
    path_null          BOOLEAN  NOT NULL,
    query              VARCHAR  NOT NULL,
    query_null         BOOLEAN  NOT NULL,
    fragment           VARCHAR  NOT NULL,
    fragment_null      BOOLEAN  NOT NULL,
    item_pk            BIGINT   NOT NULL,
    persistent         BOOLEAN  NOT NULL,
    FOREIGN KEY (item_pk) REFERENCES item (pk),
    FOREIGN KEY (scheme_pk) REFERENCES scheme (pk),
    FOREIGN KEY (host_pk) REFERENCES host (pk)
);

-- Most unique field first for efficient index:
-- The path is the basis for nearly all difference.
-- Beyond that, some members mirror the DOI path on their own domain, so the host would tell the difference.
-- Beyond that, there are many URLs where the path is the same but the query differs.
-- The rest of the fields are either rare or mostly the same.
CREATE UNIQUE INDEX ON item_identifier (path, path_null, host_pk, query, query_null, scheme_pk, port, fragment, fragment_null);
CREATE INDEX ON item_identifier (item_pk);

CREATE FUNCTION process_buffered_item_identifiers(IN batch_size INT, OUT count_processed INT, OUT count_remaining INT)
    LANGUAGE plpgsql AS
$$
BEGIN
    CREATE TEMP TABLE iib_processing ON COMMIT DROP AS
    SELECT *
    FROM item_identifier_buffer
    ORDER BY pk
    LIMIT batch_size;

    IF (SELECT COUNT(*) FROM iib_processing) > 0 THEN

        -- Create any unknown schemes.
        -- Null is recorded in the schemes table.
        WITH
            distinct_schemes AS (SELECT scheme AS val, scheme_null AS val_null FROM iib_processing GROUP BY (scheme, scheme_null)),
            -- keep only those that don't match existing ones
            new_schemes AS (
                SELECT
                    distinct_schemes.val AS val,
                    distinct_schemes.val_null AS val_null
                FROM distinct_schemes LEFT JOIN scheme ON scheme.val = distinct_schemes.val AND scheme.val_null = distinct_schemes.val_null WHERE scheme.pk IS NULL)
        INSERT INTO scheme (val, val_null)
            SELECT val, val_null FROM new_schemes;

        -- Create any unknown hosts.
        -- Null is recorded in the hosts table.
        WITH
            distinct_hosts AS (SELECT host AS val, host_null AS val_null FROM iib_processing GROUP BY (host, host_null)),
            -- keep only those that don't match existing ones
            new_hosts AS (
            SELECT
                distinct_hosts.val AS val,
                 distinct_hosts.val_null AS val_null
            FROM distinct_hosts LEFT JOIN host ON host.val = distinct_hosts.val AND host.val_null = distinct_hosts.val_null WHERE host.pk IS NULL)
        INSERT INTO host (val, val_null)
            SELECT val, val_null FROM new_hosts;

        CREATE INDEX ON iib_processing (pk);
        ANALYZE iib_processing;

        -- Create a table of items/identifiers that did not previously exist in the database.
        -- Assign a new item_pk for each.
        -- No nulls allowed in the foreign keys, they are recorded as entries in scheme and host tables.
        CREATE TEMP TABLE iip_resolved ON COMMIT DROP AS
            -- If there are conflicting values for blank and persistent, take the first.
            WITH distinct_entries AS
                (SELECT DISTINCT ON (path, path_null, host_pk, query, query_null, scheme_pk, port, fragment, fragment_null)
                    path,
                    path_null,
                    host.pk as host_pk,
                    query,
                    query_null,
                    scheme.pk as scheme_pk,
                    port,
                    fragment,
                    fragment_null,
                    blank,
                    persistent
                FROM iib_processing
                JOIN scheme ON scheme.val = iib_processing.scheme AND scheme.val_null = iib_processing.scheme_null
                JOIN host ON host.val = iib_processing.host AND host.val_null = iib_processing.host_null )
            SELECT
                    d.path,
                    d.path_null,
                    d.host_pk,
                    d.query,
                    d.query_null,
                    d.scheme_pk,
                    d.port,
                    d.fragment,
                    d.fragment_null,
                    d.blank,
                    d.persistent,
                    NEXTVAL('item_pk_seq') AS item_pk
                    FROM distinct_entries d
                LEFT JOIN item_identifier i ON
                    d.path = i.path AND
                    d.path_null = i.path_null AND
                    d.host_pk = i.host_pk AND
                    d.query = i.query AND
                    d.query_null = i.query_null AND
                    d.scheme_pk = i.scheme_pk AND
                    d.port = i.port AND
                    d.fragment = i.fragment AND
                    d.fragment_null = i.fragment_null
                WHERE i.pk IS NULL;

        CREATE INDEX ON iip_resolved (path, path_null, host_pk, query, query_null, scheme_pk, port, fragment, fragment_null, persistent);
        CREATE INDEX ON iip_resolved (item_pk);
        ANALYZE iip_resolved;

        -- Insert the rest.
        INSERT INTO item (
            pk,
            blank,
            created_at)
        SELECT item_pk, blank, NOW()
        FROM iip_resolved
        ORDER BY item_pk;

        INSERT INTO item_identifier (
            path,
            path_null,
            host_pk,
            query,
            query_null,
            scheme_pk,
            port,
            fragment,
            fragment_null,
            persistent,
            item_pk)
        SELECT path, path_null, host_pk, query, query_null, scheme_pk, port, fragment, fragment_null, persistent, item_pk
        FROM iip_resolved
        ORDER BY path, path_null, host_pk, query, query_null, scheme_pk, port, fragment, fragment_null, persistent, item_pk;

        -- Delete the processed rows from the buffer!
        DELETE
        FROM item_identifier_buffer iib
            USING iib_processing iip
        WHERE iib.pk = iip.pk;

        SELECT COUNT(pk) FROM iib_processing INTO count_processed;

        SELECT COUNT(pk) FROM item_identifier_buffer INTO count_remaining;
    END IF;
END;
$$;


-- A vocabulary of relationship types.
CREATE TABLE relationship_type
(
    pk    SERIAL PRIMARY KEY NOT NULL,
    value VARCHAR(190)       NOT NULL,
    UNIQUE (value)
);

CREATE TABLE relationship
(
    pk                   BIGSERIAL PRIMARY KEY    NOT NULL,
    first_ingested_at    TIMESTAMP WITH TIME ZONE NOT NULL,
    last_ingested_at     TIMESTAMP WITH TIME ZONE NOT NULL,
    first_linked_at      TIMESTAMP WITH TIME ZONE NULL,
    last_linked_at       TIMESTAMP WITH TIME ZONE NULL,
    from_item_pk         BIGINT                   NOT NULL,
    to_item_pk           BIGINT                   NOT NULL,
    relationship_type_pk INT                      NOT NULL,
    FOREIGN KEY (from_item_pk) REFERENCES item (pk),
    FOREIGN KEY (to_item_pk) REFERENCES item (pk),
    FOREIGN KEY (relationship_type_pk) REFERENCES relationship_type (pk),
    UNIQUE (from_item_pk, relationship_type_pk, to_item_pk)
);

CREATE INDEX ON relationship (from_item_pk);
CREATE INDEX ON relationship (relationship_type_pk);
CREATE INDEX ON relationship (to_item_pk);

CREATE TABLE relationship_assertion_buffer
(
    pk                   BIGSERIAL PRIMARY KEY    NOT NULL,
    asserted_at          TIMESTAMP WITH TIME ZONE NOT NULL,
    ingested_at          TIMESTAMP WITH TIME ZONE NOT NULL,
    state                BOOLEAN                  NOT NULL,
    party_pk             BIGINT                   NOT NULL,
    from_item_pk         BIGINT                   NOT NULL,
    to_item_pk           BIGINT                   NOT NULL,
    relationship_type_pk INT                      NOT NULL,
    envelope_pk          BIGINT                   NOT NULL,
    buffer_batch_id      UUID                     NOT NULL
);
CREATE INDEX ON relationship_assertion_buffer (buffer_batch_id);


CREATE TABLE relationship_assertion_current
(
    pk                          BIGINT PRIMARY KEY       NOT NULL,
    asserted_at                 TIMESTAMP WITH TIME ZONE NOT NULL,
    ingested_at                 TIMESTAMP WITH TIME ZONE NOT NULL,
    state                       BOOLEAN                  NOT NULL,
    party_pk                    BIGINT                   NOT NULL,
    from_item_pk                BIGINT                   NOT NULL,
    from_item_is_blank          BOOLEAN                  NOT NULL,
    to_item_pk                  BIGINT                   NOT NULL,
    to_item_is_blank            BOOLEAN                  NOT NULL,
    relationship_type_pk        INT                      NOT NULL,
    relationship_type_is_object BOOLEAN                  NOT NULL,
    envelope_pk                 BIGINT                   NOT NULL,
    FOREIGN KEY (party_pk) REFERENCES item (pk),
    FOREIGN KEY (from_item_pk) REFERENCES item (pk),
    FOREIGN KEY (to_item_pk) REFERENCES item (pk),
    FOREIGN KEY (envelope_pk) REFERENCES envelope (pk),
    FOREIGN KEY (relationship_type_pk) REFERENCES relationship_type (pk),
    UNIQUE (from_item_pk, relationship_type_pk, to_item_pk, party_pk)
);

-- Needed for quick lookup in set_stale_trees_from_items.
CREATE INDEX ON relationship_assertion_current (relationship_type_pk);

-- These are used when populating the relationship table.
CREATE INDEX ON relationship_assertion_current (from_item_pk, to_item_pk, ingested_at)
    WHERE from_item_is_blank
        AND relationship_type_is_object
        AND NOT to_item_is_blank;

CREATE INDEX ON relationship_assertion_current (to_item_pk, from_item_pk, relationship_type_pk, ingested_at)
    WHERE NOT from_item_is_blank
        AND NOT relationship_type_is_object
        AND to_item_is_blank;

CREATE TABLE relationship_assertion_history
(
    pk                          BIGINT PRIMARY KEY       NOT NULL,
    asserted_at                 TIMESTAMP WITH TIME ZONE NOT NULL,
    ingested_at                 TIMESTAMP WITH TIME ZONE NOT NULL,
    state                       BOOLEAN                  NOT NULL,
    party_pk                    BIGINT                   NOT NULL,
    from_item_pk                BIGINT                   NOT NULL,
    from_item_is_blank          BOOLEAN                  NOT NULL,
    to_item_pk                  BIGINT                   NOT NULL,
    to_item_is_blank            BOOLEAN                  NOT NULL,
    relationship_type_pk        INT                      NOT NULL,
    relationship_type_is_object BOOLEAN                  NOT NULL,
    envelope_pk                 BIGINT                   NOT NULL,
    FOREIGN KEY (party_pk) REFERENCES item (pk),
    FOREIGN KEY (from_item_pk) REFERENCES item (pk),
    FOREIGN KEY (to_item_pk) REFERENCES item (pk),
    FOREIGN KEY (envelope_pk) REFERENCES envelope (pk),
    FOREIGN KEY (relationship_type_pk) REFERENCES relationship_type (pk)
);

CREATE INDEX ON relationship_assertion_history (from_item_pk, relationship_type_pk, to_item_pk, party_pk);

CREATE VIEW relationship_assertion AS
SELECT *, TRUE AS current
FROM relationship_assertion_current
UNION ALL
SELECT *, FALSE AS current
FROM relationship_assertion_history;

CREATE FUNCTION process_buffered_relationship_assertions(IN batch_size INT, OUT count_processed INT, OUT count_remaining INT)
    LANGUAGE plpgsql AS
$$
BEGIN
    CREATE TEMP TABLE rab_processing ON COMMIT DROP AS
    SELECT *
    FROM relationship_assertion_buffer
    ORDER BY pk
    LIMIT batch_size;

    IF (SELECT COUNT(*) FROM rab_processing) > 0 THEN
        CREATE INDEX ON rab_processing (from_item_pk, relationship_type_pk, to_item_pk, party_pk, pk DESC);

        -- For each row in rab_processing, bring together the details needed to add a new relationship assertion.
        -- Also compare the pk of the current row to the maximum (i.e. latest) pk for the same relationship
        -- to deduce whether or not this row is the current assertion.
        CREATE TEMP TABLE rap_select ON COMMIT DROP AS
        SELECT rap.*,
               rap.pk = latest_pk.pk                           AS current,
               (SELECT blank
                FROM item
                WHERE item.pk = rap.from_item_pk) AS from_item_is_blank,
               (SELECT blank
                FROM item
                WHERE item.pk = rap.to_item_pk)   AS to_item_is_blank,
               (SELECT value = 'object'
                FROM relationship_type
                WHERE pk = rap.relationship_type_pk)           AS relationship_type_is_object
        FROM rab_processing rap
                 JOIN LATERAL (
            -- Find the maximum pk for this particular assertion.
            SELECT pk
            FROM rab_processing
            WHERE (from_item_pk, relationship_type_pk, to_item_pk, party_pk) =
                  (rap.from_item_pk, rap.relationship_type_pk, rap.to_item_pk, rap.party_pk)
            ORDER BY pk DESC
            LIMIT 1
            ) latest_pk ON TRUE;

        -- Moves current rows that are about to be superseded from current to history.
        WITH assertions_to_move AS (
            -- Delete and return from the current table
            DELETE FROM relationship_assertion_current rac
                USING rap_select rap
                WHERE (rac.from_item_pk, rac.relationship_type_pk, rac.to_item_pk, rac.party_pk) =
                      (rap.from_item_pk, rap.relationship_type_pk, rap.to_item_pk, rap.party_pk)
                    AND current
                RETURNING rac.*)
             -- And insert the returned deletions into history
        INSERT
        INTO relationship_assertion_history
        SELECT *
        FROM assertions_to_move;

        -- Insert buffered assertions that are already non-current straight into history.
        INSERT INTO relationship_assertion_history (pk,
                                                    asserted_at,
                                                    ingested_at,
                                                    state,
                                                    party_pk,
                                                    from_item_pk,
                                                    from_item_is_blank,
                                                    to_item_pk,
                                                    to_item_is_blank,
                                                    relationship_type_pk,
                                                    relationship_type_is_object,
                                                    envelope_pk)
        SELECT pk,
               asserted_at,
               ingested_at,
               state,
               party_pk,
               from_item_pk,
               from_item_is_blank,
               to_item_pk,
               to_item_is_blank,
               relationship_type_pk,
               relationship_type_is_object,
               envelope_pk
        FROM rap_select
        WHERE NOT current;

        -- Insert new current buffered assertions.
        INSERT INTO relationship_assertion_current (pk,
                                                    asserted_at,
                                                    ingested_at,
                                                    state,
                                                    party_pk,
                                                    from_item_pk,
                                                    from_item_is_blank,
                                                    to_item_pk,
                                                    to_item_is_blank,
                                                    relationship_type_pk,
                                                    relationship_type_is_object,
                                                    envelope_pk)
        SELECT pk,
               asserted_at,
               ingested_at,
               state,
               party_pk,
               from_item_pk,
               from_item_is_blank,
               to_item_pk,
               to_item_is_blank,
               relationship_type_pk,
               relationship_type_is_object,
               envelope_pk
        FROM rap_select
        WHERE current;

        -- Normal relationships
        INSERT INTO relationship (from_item_pk,
                                  relationship_type_pk,
                                  to_item_pk,
                                  first_ingested_at,
                                  last_ingested_at,
                                  first_linked_at,
                                  last_linked_at)
            -- This select only applies the first time a relationship is asserted. Otherwise the existing one gets updated by the conflict.
        SELECT from_item_pk,
               relationship_type_pk,
               to_item_pk,
               MIN(ingested_at),
               MAX(ingested_at),
               NULL,
               NULL
        FROM rap_select
        WHERE NOT from_item_is_blank
          AND NOT relationship_type_is_object
          AND NOT to_item_is_blank
        GROUP BY from_item_pk,
                 relationship_type_pk,
                 to_item_pk
        ORDER BY from_item_pk,
                 relationship_type_pk,
                 to_item_pk
        ON CONFLICT (from_item_pk, relationship_type_pk, to_item_pk)
            DO UPDATE SET last_ingested_at = excluded.last_ingested_at;

        -- Reified relationships.
        WITH new_blank_nodes AS (SELECT rs.from_item_pk,
                                        rs.relationship_type_pk,
                                        ra.to_item_pk,
                                        rs.ingested_at,
                                        ra.ingested_at AS linked_at
                                 FROM rap_select rs
                                          JOIN relationship_assertion ra
                                               ON rs.to_item_pk = ra.from_item_pk
                                 WHERE NOT rs.from_item_is_blank
                                   AND NOT rs.relationship_type_is_object
                                   AND ra.relationship_type_is_object
                                   AND rs.to_item_is_blank),
             new_linked_objects AS (SELECT ra.from_item_pk,
                                           ra.relationship_type_pk,
                                           rs.to_item_pk,
                                           ra.ingested_at,
                                           rs.ingested_at AS linked_at
                                    FROM rap_select rs
                                             JOIN relationship_assertion ra
                                                  ON rs.from_item_pk = ra.to_item_pk
                                    WHERE rs.from_item_is_blank
                                      AND rs.relationship_type_is_object
                                      AND NOT ra.relationship_type_is_object
                                      AND NOT rs.to_item_is_blank),
             new_reified_assertions AS (SELECT *
                                        FROM new_blank_nodes
                                        UNION
                                        SELECT *
                                        FROM new_linked_objects)
        INSERT
        INTO relationship(from_item_pk,
                          relationship_type_pk,
                          to_item_pk,
                          first_ingested_at,
                          last_ingested_at,
                          first_linked_at,
                          last_linked_at)
        SELECT from_item_pk,
               relationship_type_pk,
               to_item_pk,
               MIN(ingested_at) AS first_ingested_at,
               MAX(ingested_at) AS last_ingested_at,
               MIN(linked_at)   AS first_linked_at,
               MAX(linked_at)   AS last_linked_at
        FROM new_reified_assertions
        GROUP BY from_item_pk, relationship_type_pk, to_item_pk
        ORDER BY from_item_pk, relationship_type_pk, to_item_pk
        ON CONFLICT (from_item_pk, relationship_type_pk, to_item_pk)
            DO UPDATE SET last_ingested_at = excluded.last_ingested_at, last_linked_at = excluded.last_linked_at;

        -- When relationship assertions are made against an item, mark that Item as stale
        -- and in need of a potential re-render.
        -- Only mark stale items with 'from_item_pk', not 'to_item_pk', as those are the
        -- changes that might result in needing a re-render.
        INSERT INTO item_render_status_queue (item_pk, updated_at)
        SELECT from_item_pk, NOW() FROM rap_select;

        -- Delete the processed rows from the buffer!
        DELETE
        FROM relationship_assertion_buffer rab
            USING rab_processing rap
        WHERE rab.pk = rap.pk;

        SELECT COUNT(*) FROM rab_processing INTO count_processed;

        SELECT COUNT(pk) FROM relationship_assertion_buffer INTO count_remaining;
    END IF;
END;
$$;

CREATE TABLE property_assertion_buffer
(
    pk              BIGSERIAL PRIMARY KEY    NOT NULL,
    asserted_at     TIMESTAMP WITH TIME ZONE NOT NULL,
    ingested_at     TIMESTAMP WITH TIME ZONE NOT NULL,
    values          JSONB                    NOT NULL,
    party_pk        BIGINT                   NOT NULL,
    item_pk         BIGINT                   NOT NULL,
    envelope_pk     BIGINT                   NOT NULL,
    buffer_batch_id UUID                     NOT NULL
);
CREATE INDEX ON property_assertion_buffer (buffer_batch_id);

CREATE TABLE property_assertion_current
(
    pk          BIGINT PRIMARY KEY       NOT NULL,
    asserted_at TIMESTAMP WITH TIME ZONE NOT NULL,
    ingested_at TIMESTAMP WITH TIME ZONE NOT NULL,
    values      JSONB                    NOT NULL,
    party_pk    BIGINT                   NOT NULL,
    item_pk     BIGINT                   NOT NULL,
    envelope_pk BIGINT                   NOT NULL,
    FOREIGN KEY (party_pk) REFERENCES item (pk),
    FOREIGN KEY (item_pk) REFERENCES item (pk),
    FOREIGN KEY (envelope_pk) REFERENCES envelope (pk),
    UNIQUE (item_pk, party_pk)
);

CREATE TABLE property_assertion_history
(
    pk          BIGINT PRIMARY KEY       NOT NULL,
    asserted_at TIMESTAMP WITH TIME ZONE NOT NULL,
    ingested_at TIMESTAMP WITH TIME ZONE NOT NULL,
    values      JSONB                    NOT NULL,
    party_pk    BIGINT                   NOT NULL,
    item_pk     BIGINT                   NOT NULL,
    envelope_pk BIGINT                   NOT NULL,
    FOREIGN KEY (party_pk) REFERENCES item (pk),
    FOREIGN KEY (item_pk) REFERENCES item (pk),
    FOREIGN KEY (envelope_pk) REFERENCES envelope (pk)
);

CREATE INDEX property_assertion_history_idx ON property_assertion_history (item_pk, party_pk);

CREATE VIEW property_assertion AS
SELECT *, TRUE AS current
FROM property_assertion_current
UNION ALL
SELECT *, FALSE AS current
FROM property_assertion_history;

CREATE FUNCTION process_buffered_property_assertions(IN batch_size INT, OUT count_processed INT, OUT count_remaining INT)
    LANGUAGE plpgsql AS
$$
BEGIN
    CREATE TEMP TABLE pab_processing ON COMMIT DROP AS
    SELECT *
    FROM property_assertion_buffer
    ORDER BY pk
    LIMIT batch_size;

    IF (SELECT COUNT(*) FROM pab_processing) > 0 THEN
        CREATE INDEX ON pab_processing (item_pk, party_pk, pk DESC);

        /*
         * For each row in pab_processing, compare the pk of the row with the maximum (i.e. latest) pk for the same property to deduce whether or not this row is the current assertion.
         */
        CREATE TEMP TABLE pap_select ON COMMIT DROP AS
        SELECT pap.*, pap.pk = latest_pk.pk AS current
        FROM pab_processing pap
                 JOIN LATERAL (
            -- Find the maximum pk for this particular assertion.
            SELECT pk
            FROM pab_processing
            WHERE (item_pk, party_pk) =
                  (pap.item_pk, pap.party_pk)
            ORDER BY pk DESC
            LIMIT 1
            ) latest_pk ON TRUE
        ORDER BY pap.pk;

        -- Moves current rows that are about to be superseded from current to history.
        WITH pa_history AS (
            -- Delete and return from the current table
            DELETE FROM property_assertion_current pa
                USING pap_select pap
                WHERE (pa.item_pk, pa.party_pk) =
                      (pap.item_pk, pap.party_pk)
                    AND current
                RETURNING pa.*)
             -- And insert the returned deletions into history
        INSERT
        INTO property_assertion_history
        SELECT *
        FROM pa_history;

        -- Insert buffered assertions that are already non-current straight into history.
        INSERT INTO property_assertion_history (pk,
                                                asserted_at,
                                                ingested_at,
                                                values,
                                                party_pk,
                                                item_pk,
                                                envelope_pk)
        SELECT pk,
               asserted_at,
               ingested_at,
               values,
               party_pk,
               item_pk,
               envelope_pk
        FROM pap_select pap
        WHERE NOT current;

        -- Insert new current buffered assertions.
        INSERT INTO property_assertion_current (pk,
                                                asserted_at,
                                                ingested_at,
                                                values,
                                                party_pk,
                                                item_pk,
                                                envelope_pk)
        SELECT pk,
               asserted_at,
               ingested_at,
               values,
               party_pk,
               item_pk,
               envelope_pk
        FROM pap_select pap
        WHERE current;

        -- When property assertions are made against an item, mark that Item as stale
        -- and in need of a re-render.
        INSERT INTO item_render_status_queue (item_pk, updated_at)
        SELECT item_pk, NOW() FROM pap_select;

        -- Delete the processed rows from the buffer!
        DELETE
        FROM property_assertion_buffer pab
            USING pab_processing pap
        WHERE pab.pk = pap.pk;

        SELECT COUNT(*) FROM pab_processing INTO count_processed;

        SELECT COUNT(pk) FROM property_assertion_buffer INTO count_remaining;
    END IF;
END;
$$;

-- Queue of signals that an item needs re-rendering.
CREATE TABLE item_render_status_queue (
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    item_pk BIGINT NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
    FOREIGN KEY (item_pk) REFERENCES item (pk)
);

-- A collection of items and their rendered content types
CREATE TABLE rendered_item
(
    pk           SERIAL PRIMARY KEY       NOT NULL,
    item_pk      BIGINT                   NOT NULL,
    content_type VARCHAR                  NOT NULL,
    pointer      VARCHAR                  NOT NULL,
    hash         VARCHAR                  NOT NULL,
    current      BOOLEAN                  NOT NULL,
    updated_at   TIMESTAMP WITH TIME ZONE NOT NULL,
    version      BIGINT                   NOT NULL,
    FOREIGN KEY (item_pk) REFERENCES item (pk)
);

CREATE INDEX ON rendered_item (content_type);
CREATE INDEX ON rendered_item (current);
CREATE INDEX ON rendered_item (updated_at);
CREATE INDEX ON rendered_item (version);

-- Some items, such as Articles and Members, are treated as root nodes of a tree. These are Items that will be rendered
-- into metadata documents.
-- This table stores the pre-calculated tree from each of those nodes. It allows the retrieval of the tree, but also
-- a quick index to find all trees that contain a given item.
CREATE TABLE item_tree_reachability
(
    -- root of the tree
    root_item_pk  BIGINT PRIMARY KEY NOT NULL,
    -- pks of all items found in the tree
    tree_item_pks BIGINT[]
);

-- Allow searching of a root node by an item that may be in the tree.
CREATE INDEX ON item_tree_reachability USING gin (tree_item_pks);

-- Given an item_pk, find the tree that stems from it, and update the reachability index.
-- This can be called for any item but should be called only on designated root nodes.
CREATE PROCEDURE build_reachability(changed_root_item_pk BIGINT, max_depth INT)
    LANGUAGE plpgsql AS
$$
BEGIN
    WITH
        RECURSIVE
        tree(from_item_pk, to_item_pk, depth) AS (SELECT from_item_pk, to_item_pk, (SELECT max_depth)
                                                  FROM relationship_assertion
                                                  WHERE current
                                                    AND from_item_pk = changed_root_item_pk
                                                  UNION
                                                  SELECT child.from_item_pk, child.to_item_pk, depth - 1
                                                  FROM tree,
                                                       relationship_assertion AS child
                                                  WHERE current
                                                    AND tree.to_item_pk = child.from_item_pk
                                                    AND depth > 0),
        item_pks AS (SELECT ARRAY_AGG(DISTINCT (to_item_pk)) AS tree_pks FROM tree)
    INSERT
    INTO item_tree_reachability (root_item_pk,
                                 tree_item_pks)
    VALUES (changed_root_item_pk,
            (SELECT tree_pks FROM item_pks))
    ON CONFLICT (root_item_pk) DO UPDATE SET tree_item_pks = (SELECT tree_pks FROM item_pks);
END;
$$;

-- Given a set of items that have changed, find the trees that they are part of and update them.
-- Each tree will be marked as stale for re-rending.
-- The reachability index will also be updated because the tree structure may have changed.
CREATE PROCEDURE set_stale_trees_from_items(changed_item_pks BIGINT[])
    LANGUAGE plpgsql AS
$$
BEGIN
    WITH
        -- There may be circular references in the data, which manifest as an item_pk being found in its own
        -- reachability tree. This is valid data.
        -- But setting an item as stale when it is rendered would result in it an infinite render loop, being
        -- perpetually stale.
        -- Avoid this by not setting an item stale when it's found in its own reachability index.
        affected_root_items AS (
            SELECT root_item_pk
            FROM item_tree_reachability
            WHERE changed_item_pks && tree_item_pks
            AND NOT root_item_pk = ANY(tree_item_pks))

    INSERT INTO item_render_status_queue (item_pk, updated_at)
    SELECT root_item_pk, NOW()
    FROM affected_root_items;
END;
$$;

CREATE TABLE open_cursor
(
    cursor_name  CHAR(6) PRIMARY KEY      NOT NULL,
    row_count    INT                      NOT NULL,
    created_at   TIMESTAMP WITH TIME ZONE NOT NULL,
    last_used_at TIMESTAMP WITH TIME ZONE NOT NULL,
    last_key     INT                      NOT NULL
);

-- Some empty tables (e.g. scheme) will be immediately used and will never meet the default table heuristics.
-- So run the analyzer to get a good picture of the initial state.
ANALYZE;