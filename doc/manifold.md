# Module manifold

<img src="./images/celebration.png" align="right" width="300px">

Manifold is where research objects meet. It keeps track of research objects, metadata, and the connections between them,
as well as who says what. It accepts data from Crossref members, Crossref Agents, and other members of the scholarly
community, and brings them together in one single research graph.

Manifold will power a number of future APIs, such as the REST API, Content Negotiation, Funder Registry, Event Data and
more besides. The codebase is split into Kernel functionality, which implements the underlying data model, and modules
which provide features. Everything not in the `org.crossref.manifold.modules` namespace is considered Kernel.

**The Kernel namespace should not contain any Crossref-specific functionality**. This means no mention of work types,
DOIs, ORCID iDs etc. This keeps Kernel focussed on cross-cutting concerns only.

## Project Links

- <a href="https://sonarcloud.io/dashboard?id=org.crossref%3Amanifold">SONAR code quality</a>
- <a href="https://gitlab.com/crossref/manifold">Source code</a>

## Kernel

See `org.crossref.manifold.itemgraph` for a primer on the data model.

## Features are Modules

Modules provide Product features. See `org.crossref.manifold.modules` for an overview.