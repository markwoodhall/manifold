# Package org.crossref.manifold.itemgraph

The Item Graph is the whole set of Items that we know about, the ItemIdentifiers attached to them, and the assertions
made about them. It is constructed by integrating ItemTrees, so read the documentation
for `org.crossref.manifold.itemtree` first.

## Principles of the Item Graph data model

### Items and Identifiers

1. **Every 'thing' we talk about is an Item.** That could be Articles, Authors, Members or anything in between.
2. **Every Item can have Item Identifiers.** These are things like DOIs, ORCID ids, Funder IDs, etc. Identifiers are
   vocabularies we share with sections of our community.
3. **Identifiers are handles that let us point to Items**. Different communities may want to talk about the same Item
   using their own language. One community may use ISSNs to talk about an Item. Another may use a Journal DOI to talk
   about the same Item.
4. **Items sometimes have more than one Identifier.** Identifier systems can overlap (e.g. Journal DOI and ISSN) so we
   need to be able to associate more than one Item Identifier with an Item.
5. **We don't always know the Item Identifier for an Item.** For example authors without ORCID iDs, Articles for which
   we know the citation string not the DOI. Even so, these things are still Items.
6. **We associate Items with Item Identifiers at any time**, for example if we discover a new Identifier, or we're able
   to find it for the first time through matching.
7. **We cannot associate an Item Identifier with more than one Item.** An Item Identifier is always unambiguous. That's
   a conflict and the system won't allow it.

### Relationships and Truth

1. **In the Research Community, Items have relationships, such as citations or authorship**.
2. **There is no absolute truth**, and the only way that we know about a relationship is when someone asserts it.
3. **Item Graph represents the concept of a relationship using our own simple internal vocabulary.**
4. **The Research Community uses a range of Relationship vocabularies that overlap** (e.g. Crossref relationship
   vocabulary, Funder Registry vocabulary, Scholix, various ones for Content Negotiation). When we ingest relationships
   into the Manifold API we transform them into our own vocabulary. When we output relationships, we have the option of
   mapping them to exernal vocabularies. This allows it to power services like Content Negotiation, Funder Registry,
   Scholix etc.
5. **Manifold represents relationships between Items**. Every Relationship is asserted by zero or more asserting
   parties. The presence of a relationship in the Manifold API must always be interpreted alongside the parties that
   asserted it.

### Item Trees

1. **An Item Tree is a data structure that describes how someone sees a number of Items the Relationships between
   them.**
2. **An Item Tree uses Item Identifiers to identify the Items it's talking about**, for example it might discuss the
   concept of a "DOI being authored by an ORCID ID".
3. **An Item Tree is neither true or false**, it just describes a set of Item Identifiers their Relationships and
   metadata.
4. **An Item Tree is tree structured**, meaning that each Item in the tree has relationships to a number of other Items,
   and they can have relationships, and so on.
5. **A single Item Tree can represent the same information as one of our UNIXML files or REST API Citeproc JSON
   responses.** By constructing a tree that includes Journals, Articles, Authors, Funders, it can describe a view of an
   Item and its immediate surroundings in the research graph. 6**Item Trees can be fed into item Graph, and come out of
   Item Graph**. As they just a format for describing a set of Items and Relationships. They are a type
   of <a href="https://en.wikipedia.org/wiki/Named_graph">Named Graph</a>.

### Evolving Truth

1. **An Item Tree can be asserted by a Party.** When a Party sends us an Item Tree, they can say "I believe the data in
   this Item Tree is true".
   **The Item Graph grows by incorporating Item Trees**. An Item Tree is a structure that represents someone's view of
   an Item, its Item Identifiers, its properties, and relationships to other Items.
2. **An Item Tree is always subjective**, as it represents the way that a given asserting party sees the world.
3. **To incorporate an Item Tree into the Item Graph we need to match Items in the tree with Items we alredy know
   about.** First the Items are resolved to existing Items in the Graph via their Item Identifiers. If Items don't
   exist, they are created. The Relationships are also identified. After this, an assertion is made for every
   relationship to say "the asserting party believes this to be true".
4. **Every assertion of a Relationship is recorded against the party who asserted it.** No Relationships exist in the
   Research Manifold API without this provenance.
5. Any party who has access can make an assertion.

### Representing Authority

1. **Crossref could permit any Party to make an assertion,** including members, other organisations, or members of the
   public.
2. **Crossref maintains a set of Authority roles for each Item,** such as "who is the Registration Agency of this DOI",
   or "who is the Steward of this Metadata". This allows us to identify the authority of who said what. For example what
   the Publisher said may be interpreted differently to what the accredited Author said.
3. **Crossref can make assertions about Items, but our view is just as subjective as anyone else's**. When we add a
   reference match, or cited-by count, that assertion is made by Crossref alongside assertions made by other Parties.

See the ItemGraphIngester class documentation for the algorithm.

### Identified and Blank Items

Items exist purely to have Property Statements and Link Statements made about them. Where two Item Trees want to talk
about the same Item, they must use an Item Identifier (such as a DOI or ORCID iD) to indicate which Item they are
talking about.

Some Items don't have Item Identifiers. These are called Blank Nodes, a concept borrowed from RDF. Blank Node Items are
still considered Items, and can still be linked from Items. But because we have no more information about the Item other
than the Item's Properties and its parent in the Item Tree.

So, whilst an Item Tree is a tree of Items with Item Identifiers, the Manifold is a graph of Items, identified by their
Item Identifier.

### Item ontology

The Item ontology includes all Research Items, such as Articles, Journals, ORCID iDs, etc. It assigns each Item a PK (
Primary Key), which is a numerical ID for that Item.

We do not record concepts like ownership in the Item Ontology: it is simply a list of things that we know that someone
says exist.

